<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('creator_id');
            $table->enum('coupon_type', ['0', '1'])->default('0')->comment('0=>Discount,1=>Complimentary Reward');
            $table->string('coupon')->nullable();
            $table->integer('amount')->nullable();
            $table->enum('amount_type', ['Flat', 'Percent'])->default('Flat');
            $table->enum('usage', ['Single', 'Multiple'])->default('Single');
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->string('description')->nullable();
            $table->tinyInteger('active_status')->default('0')->comment('0=>Inactive,1=>Active');
            $table->timestamps();
            $table->foreign('creator_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
