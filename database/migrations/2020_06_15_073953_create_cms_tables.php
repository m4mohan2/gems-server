<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();  
            $table->string('slug')->nullable();                       
            $table->string('short_description')->nullable();
            $table->text('description')->notNullable(); 
            $table->string('image')->nullable();
            $table->tinyInteger('status')->default('0')->comment('0=>Inactive,1=>Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms');
    }
}
