<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGemsCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gems_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('category_name')->unique()->notNullable();
            $table->string('category_type')->unique()->notNullable();            
            $table->tinyInteger('status')->default('0')->comment('0=>Inactive,1=>Active');
            $table->tinyInteger('is_approved')->default('0')->comment('0=>No,1=>Yes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gems_categories');
    }
}
