<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUserDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_details', function (Blueprint $table) {
            $table->string('zip')->nullable()->after('address');
            $table->string('city')->nullable()->after('address');
            $table->string('state')->nullable()->after('address');
            $table->string('country')->nullable()->after('address');
            $table->string('phone')->nullable()->after('address');
            $table->text('address2')->nullable()->after('address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('user_details', function (Blueprint $table) {
            $table->dropColumn('address2');
            $table->dropColumn('phone');
            $table->dropColumn('country');
            $table->dropColumn('city');
            $table->dropColumn('state');
            $table->dropColumn('zip');
        });
    }
}
