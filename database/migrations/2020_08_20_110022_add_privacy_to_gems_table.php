<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPrivacyToGemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gems', function (Blueprint $table) {
            $table->tinyInteger('privacy')->after('is_approved')->default('1')->comment('1=>Public,2=>Private,3=>Friend');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gems', function (Blueprint $table) {
            $table->dropColumn('privacy');
        });
    }
}
