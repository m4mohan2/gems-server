<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUserPrivateInfosTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_private_infos', function (Blueprint $table) {
            $table->tinyInteger('account')->default('1');
            $table->tinyInteger('friend_request')->default('1');
            $table->tinyInteger('save_gems')->default('1');
            $table->tinyInteger('like_gems')->default('1');
            $table->tinyInteger('share_gems')->default('1');
            $table->tinyInteger('comment_post')->default('1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_private_infos', function (Blueprint $table) {
            $table->dropColumn('account');
            $table->dropColumn('friend_request');
            $table->dropColumn('save_gems');
            $table->dropColumn('like_gems');
            $table->dropColumn('share_gems');
            $table->dropColumn('comment_post');
        });
    }
}
