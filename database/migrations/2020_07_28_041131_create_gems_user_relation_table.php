<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGemsUserRelationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gems_provider_relations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('gems_id');
            $table->unsignedBigInteger('user_id');
            $table->tinyInteger('action')->default('0')->comment('0=>Pending,1=>Approved, 2=> Rejected');
            $table->timestamps();
            $table->foreign('gems_id')->references('id')->on('gems')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gems_provider_relations');
    }
}
