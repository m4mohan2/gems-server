<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShareByToShareGemsGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('share_gems_groups', function (Blueprint $table) {
            $table->unsignedBigInteger('share_by')->after('group_id');
            $table->foreign('share_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('share_gems_groups', function (Blueprint $table) {
            $table->dropColumn('share_by');
        });
    }
}
