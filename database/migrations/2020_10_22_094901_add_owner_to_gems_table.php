<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOwnerToGemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gems', function (Blueprint $table) {
           $table->unsignedBigInteger('original_created_by')->nullable();
           $table->unsignedBigInteger('previous_owner')->nullable();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gems', function (Blueprint $table) {
            $table->dropColumn('original_created_by');
            $table->dropColumn('previous_owner');
        });
    }
}
