<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('user_type')->default('0')->nullable();
            $table->tinyInteger('is_influencer')->default('0')->comment('1=>Influencer');
            $table->tinyInteger('active_status')->default('0')->comment('0=>Inactive,1=>Active');
            $table->tinyInteger('is_verified')->default('0')->comment('0=>Not Verified,1=>Verified');
            $table->string('email')->unique()->notNullable();
            $table->string('password')->nullable();
            $table->string('token')->nullable()->comment('Bearer token');
            $table->string('active_token')->nullable()->comment('Account activation token');
            $table->string('password_reset_token')->nullable()->comment('Password reset token');
            $table->string('vp')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
