<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('plan_name')->unique()->notNullable();
            $table->string('slug')->unique();
            $table->string('stripe_plan')->nullable();
            $table->string('plan_peroid')->notNullable();
            $table->string('plan_free_peroid')->notNullable();
            $table->text('plan_paid_services')->notNullable();
            $table->text('plan_free_services')->notNullable();
            $table->double('plan_price', 15, 8)->default('0');
            $table->string('plan_currency')->nullable();
            $table->tinyInteger('status')->default('1')->comment('0=>Inactive,1=>Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
