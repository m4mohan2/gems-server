<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUserAddStripeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->timestamp('trial_ends_at')->nullable()->after('password_reset_token');
            $table->string('card_last_four', 4)->nullable()->after('password_reset_token');
            $table->string('card_brand')->nullable()->after('password_reset_token')->after('password_reset_token');
            $table->string('stripe_id')->nullable()->collation('utf8mb4_bin')->after('password_reset_token');
            $table->rememberToken()->after('password_reset_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('trial_ends_at');
            $table->dropColumn('card_last_four');
            $table->dropColumn('card_brand');
            $table->dropColumn('stripe_id');
            $table->dropColumn('remember_token');
        });
    }
}
