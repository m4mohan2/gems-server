<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('subscriptions');

        Schema::create('subscriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('payment_method')->notNullable()->default('stripe');
            $table->string('stripe_subscription_id')->notNullable();
            $table->string('stripe_customer_id')->notNullable();
            $table->string('stripe_plan_id')->notNullable();
            $table->double('plan_amount', 15, 8)->notNullable()->default('0');
            $table->string('plan_amount_currency')->notNullable();
            $table->string('plan_interval')->notNullable();
            $table->tinyInteger('plan_interval_count')->notNullable();
            $table->string('payer_email')->notNullable();
            $table->timestamp('plan_period_start')->nullable();
            $table->timestamp('plan_period_end')->nullable();
            $table->string('status')->notNullable();
            $table->timestamps();
            $table->longText('dataObj')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('subscription_id')->nullable()->after('id');
            $table->foreign('subscription_id')->references('id')->on('subscriptions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('subscription_id');
        });
    }
}
