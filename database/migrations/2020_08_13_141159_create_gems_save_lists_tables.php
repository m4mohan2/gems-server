<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGemsSaveListsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gems_save_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('gems_id');
            $table->unsignedBigInteger('user_id'); 
            $table->unsignedBigInteger('label_id');
            $table->timestamps();
            $table->foreign('gems_id')->references('id')->on('gems')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('label_id')->references('id')->on('gems_labels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gems_save_lists');
    }
}
