<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomepagesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homepages', function (Blueprint $table) {
            $table->id();
            $table->integer('type')->default('1')->comment('1=>Dimond Section,2=>Tab Section,3=>Logo, 4=>Other');
            $table->string('title')->nullable();  
            $table->string('slug')->nullable();                       
            $table->text('short_description')->nullable();
            $table->text('description')->nullable(); 
            $table->string('image')->nullable();
            $table->tinyInteger('status')->default('0')->comment('0=>Inactive,1=>Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homepages');
    }
}
