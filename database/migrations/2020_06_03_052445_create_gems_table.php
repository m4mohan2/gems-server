<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gems', function (Blueprint $table) {
            $table->bigIncrements('id');            
            $table->string('place_id')->unique()->notNullable();
            $table->string('name')->notNullable();
            $table->string('lat')->notNullable();
            $table->string('lng')->notNullable();
            $table->text('address')->notNullable();
            $table->string('phone_no')->nullable();
            $table->text('note')->nullable();
            $table->text('placeObj')->nullable();
            $table->tinyInteger('status')->default('0')->comment('0=>Inactive,1=>Active');
            $table->tinyInteger('is_approved')->default('0')->comment('0=>No,1=>Yes');
            $table->unsignedBigInteger('created_by');
            $table->timestamps();            
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gems');
    }
}
