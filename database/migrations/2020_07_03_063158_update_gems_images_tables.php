<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateGemsImagesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gems_images', function (Blueprint $table) {
            $table->enum('type', ['cover_image', 'main_image'])->after('gems_id')->default('main_image');
            $table->enum('source', ['image_file', 'google_link'])->after('type')->default('image_file');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gems_images', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('source');
        });
    }
}
