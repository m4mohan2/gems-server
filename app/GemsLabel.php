<?php 

namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class GemsLabel extends Model
{ 
    public $timestamps = true;

    public function savedGems()
    {
        return $this->hasMany('App\GemsSaveList', 'label_id', 'id');
    }

}
