<?php 

namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class Group extends Model
{ 
    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo('App\UserDetail', 'creator_id','user_id')->select('user_id','first_name','last_name','profile_pic','gender','address');
    }

    public function shareGems()
    {
        return $this->hasMany('App\ShareGemsGroup', 'group_id', 'id');
    }

}
