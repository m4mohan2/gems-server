<?php 

namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class GemsComment extends Model
{ 
    public $timestamps = true;

    public function images()
    {
        return $this->hasMany('App\GemsCommentImage', 'comment_id', 'id')->select('comment_id','image');
    }

    public function users()
    {
        return $this->belongsTo('App\UserDetail', 'user_id', 'user_id')->select('user_id','first_name','last_name','profile_pic');
    }

    public function sender_friend_status()
    {
    	return $this->belongsTo('App\UserFriend', 'user_id', 'sender');
    }

    public function receiver_friend_status()
    {
    	return $this->belongsTo('App\UserFriend', 'user_id', 'receiver');
    }
}
