<?php 

namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class Category extends Model
{ 
    public $timestamps = true;

    public function subcategory()
    {
        return $this->hasMany('App\CategorySubcategoryRelation')->select('id','category_id','subcategory_id');
    }

    public function assignedSubcategory()
    {
        return $this->hasManyThrough('App\Subcategory','App\CategorySubcategoryRelation', 'category_id', 'id','id','subcategory_id');
    }

}
