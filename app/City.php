<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'state_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'state_code','country_code','latitude','longitude','created_at','updated_at','flag','wikiDataId'
    ];

    public function parrentState()
    {
        return $this->belongsTo('App\State','state_id');
    }
}