<?php 

namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class GemsSubcategoryRelation extends Model
{ 
    public $timestamps = true;

    public function subcategoryDetail()
    {
        return $this->hasOne('App\Subcategory', 'id', 'subcategory_id');

    }

}
