<?php

namespace App\Http\Controllers;

use App\Plan;
use Illuminate\Http\Request;
use Validator;

class PlanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $pageSize     = $request->input('pageSize') ? $request->input('pageSize') : '10';
        $searchString = $request->get('searchString');
        $export       = $request->get('export');

        $select = Plan::orderBy('id', 'DESC');

        //$select->orderBy('id', 'DESC');

        if ($export) {
            $data = $select->get();
        } else {
            $data = $select->paginate($pageSize);
        }

        /*** To display application status in text format***/
        $pcnt = 0;
        foreach ($data as $pval) {
            $data[$pcnt]->plan_paid_services = json_decode($pval->plan_paid_services);
            $data[$pcnt]->plan_free_services = json_decode($pval->plan_free_services);
            $pcnt++;
        }

        return response()->json([
            'data'    => $data,
            'success' => true,
        ], 200);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'plan_name'          => 'required|unique:plans',
                'plan_peroid'        => 'required',
                'plan_free_peroid'   => 'required',
                'plan_free_services' => 'required',
                'plan_paid_services' => 'required',
                'plan_price'         => 'required',
            ],
            [
                'plan_name.unique' => 'This Plan is already created. Please try a different one',
                'slug.unique'      => 'This Plan is already created. Please try a different one',
            ]
        );
        if ($validator->fails()) {
            $appmessage = Config::get('messages.audit.create_plan');
            $this->insertAuditTrail('CREATE_PLAN', 'CREATE_PLAN_FAIL',$appmessage);
            return response()->json([
                'message' => $validator->errors()->all(),
                'success' => false,
            ], 400);
        }

        \DB::beginTransaction();
        try {

            $model                     = new Plan;
            $model->plan_name          = $request->plan_name;
            $model->slug               = preg_replace("![^a-z0-9]+!i", "-", strtolower($model->plan_name));
            $model->plan_peroid        = $request->plan_peroid;
            $model->plan_free_peroid   = $request->plan_free_peroid;
            $model->plan_free_services = $request->plan_free_services;
            $model->plan_paid_services = $request->plan_paid_services;
            $model->plan_price         = $request->plan_price;
            $model->status             = $request->status;
            $model->plan_currency      = $request->plan_currency;
            $model->plan_interval_count      = $request->plan_interval_count;

            $model->save();

            \DB::commit();
            $message       = 'Plan Created Successfully.';
            $success       = true;
            $resCode       = 201;
            $audit_message = 'CREATE_PLAN_SUCCESS';
            $audit_on      = $model->plan_name;

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $resCode       = 400;
            $audit_message = 'CREATE_PLAN_FAIL';
            $audit_on      = '';
        }
        $appmessage = Config::get('messages.audit.create_plan');
        $this->insertAuditTrail('PLAN', $audit_message,$appmessage, '', $audit_on);
        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    public function update(Request $request, $id)
    {
        \DB::beginTransaction();
        try {
            $model = Plan::find($id);
            if (!empty($model)) {

                $model->plan_name = $request->input('plan_name');

                $Plan = Plan::where('plan_name', '=', $model->plan_name)->where('id', '!=', $id)->first();

                if ($Plan === null) {

                    if ($request->plan_name != '') {
                        $model->plan_name = $request->plan_name;
                    }

                    if ($request->plan_peroid != '') {
                        $model->plan_peroid = $request->plan_peroid;
                    }

                    if ($request->plan_free_peroid != '') {
                        $model->plan_free_peroid = $request->plan_free_peroid;
                    }

                    if ($request->plan_free_services != '') {
                        $model->plan_free_services = $request->plan_free_services;
                    }

                    if ($request->plan_paid_services != '') {
                        $model->plan_paid_services = $request->plan_paid_services;
                    }

                    if ($request->plan_price != '') {
                        $model->plan_price = $request->plan_price;
                    }

                    if ($request->plan_currency != '') {
                        $model->plan_currency = $request->plan_currency;
                    }

                    if ($request->plan_interval_count != '') {
                        $model->plan_interval_count = $request->plan_interval_count;
                    }

                    if ($request->status != '') {
                        $model->status = $request->status;
                    }

                    $model->save();

                    \DB::commit();
                    $message       = 'Record Updated successfully!';
                    $success       = true;
                    $resCode       = 201;
                    $audit_message = 'UPDATE_PLAN_SUCCESS';
                    $audit_on      = $model->plan_name;

                } else {
                    $message       = 'Plan Name is already exist.';
                    $success       = false;
                    $resCode       = 400;
                    $audit_message = 'UPDATE_PLAN_FAIL';
                    $audit_on      = $model->plan_name;
                }

            } else {
                $message       = 'Record not exists!';
                $success       = true;
                $resCode       = 400;
                $audit_message = 'UPDATE_PLAN_FAIL';
                $audit_on      = '';
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $resCode       = 400;
            $audit_message = 'UPDATE_PLAN_FAIL';
            $audit_on      = '';
        }
        $appmessage = Config::get('messages.audit.update_plan');
        $this->insertAuditTrail('PLAN', $audit_message,$appmessage, '', $audit_on);
        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }
    /**
     * Delete record.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        \DB::beginTransaction();
        try {
            $model = Plan::find($id);
            if (!empty($model)) {
                $model->delete();

                \DB::commit();
                $message       = 'Removed successfully!';
                $success       = true;
                $resCode       = 200;
                $audit_message = 'DELETE_PLAN_SUCCESS';
                $audit_on      = $model->plan_name;

            } else {
                $message       = 'Record not exists!';
                $success       = true;
                $resCode       = 400;
                $audit_message = 'DELETE_PLAN_FAIL';
                $audit_on      = '';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $alerttype     = false;
            $audit_message = 'DELETE_PLAN_FAIL';
            $audit_on      = '';
        }
        $appmessage = Config::get('messages.audit.delete_plan');
        //Return message
        $this->insertAuditTrail('DELETE_PLAN', $audit_message,$appmessage, '', $audit_on);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }
    /**
     * Status update.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function statusUpdate(Request $request, $id)
    {

        //return response()->json($request);
        \DB::beginTransaction();
        try {
            $model = Plan::find($id);
            if (!empty($model)) {

                if ($model->status != (int) $request->input('status')) {
                    $model->status = $request->input('status');
                }

                $model->save();

                \DB::commit();
                $message       = 'Updated successfully!';
                $success       = true;
                $resCode       = 200;
                $audit_message = 'STATUS_UPDATE_PLAN_SUCCESS';
                $audit_on      = $model->plan_name;

            } else {
                $message       = 'record not exists!';
                $success       = false;
                $audit_message = 'STATUS_UPDATE_PLAN_FAIL';
                $audit_on      = '';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $resCode       = 400;
            $audit_message = 'STATUS_UPDATE_GEMS_FAIL';
            $audit_on      = '';
        }
        $appmessage = Config::get('messages.audit.plan_status_update');
        $this->insertAuditTrail('STATUS_UPDATE_PLAN', $audit_message,$appmessage, '', $audit_on);
        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

}
