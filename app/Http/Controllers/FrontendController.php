<?php

namespace App\Http\Controllers;

use App\Cms;
use App\Gems;
use App\GemsLikeunlike;
use App\News;
use App\GemsView;
use App\GemsSaveList;
use App\GemsComment;
use App\ShareGemsGroup;
use App\GemsProviderRelation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class FrontendController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //

    /**
     * Gems search for website.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function gemsSearch(Request $request)
    {
        $latitude  = $request->get('latitude');
        $longitude = $request->get('longitude');
        $gems_name = $request->get('gems_name');
        $pageSize  = $request->get('pageSize') ? $request->get('pageSize') : '10';

        //$latitude = "23.5376328";
        //$longitude = "87.3016263";

        $models = Gems::with([
            'categoryDetail', 'createdBy', 'createdByUserDetail', 'images', 'providers',
        ])->withCount('total_like');

        if ($latitude != '' && $longitude != '') {
            $models->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians( lat ) ) ) ) AS distance'))
                ->having('distance', '<', 1);
        }
        $models->where('status','=','1');
        if ($gems_name != '') {
            $models->where('name', 'LIKE', '%' . $gems_name . '%');
        }

        //$models->orderBy('distance');
        //->where('gems.created_by', '=', $user_id)
        //->orderBy('gems.id', 'DESC');

        $results = $models->paginate($pageSize);
        //$results['total_like'] = GemsLikeunlike::where('gems_id', $id)->where('action', 1)->count();

        return response()->json($results);
    }

    /**
     * Gems search for website.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function gemsAnalysis($id)
    {
        $results = Gems::withCount('total_like', 'total_comment', 'total_view', 'total_share', 'total_favorite')
            ->where('id', $id)
            ->get();
        return response()->json($results);
    }

    /**
     * Gems analysis data by date for website.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function gemsAnalysisByDate(Request $request, $id)
    {
        $from_date = $request->from_date . " 00:00:00";
        $to_date   = $request->to_date . " 23:59:59";
        $results   = Gems::withCount(['total_like' => function ($q) use ($from_date, $to_date) {
            $q->whereBetween('created_at', [$from_date, $to_date]);
        },
            'total_comment'                            => function ($q) use ($from_date, $to_date) {
                $q->whereBetween('created_at', [$from_date, $to_date]);
            },
            'total_view'                               => function ($q) use ($from_date, $to_date) {
                $q->whereBetween('created_at', [$from_date, $to_date]);
            },
            'total_share'                              => function ($q) use ($from_date, $to_date) {
                $q->whereBetween('created_at', [$from_date, $to_date]);
            },
            'saved_lists'                              => function ($q) use ($from_date, $to_date) {
                $q->whereBetween('created_at', [$from_date, $to_date]);
            }])
            ->where('id', $id)
            ->get();
        return response()->json($results);
    }

    /**
     * Gems analysis data by like.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */

    public function gemsAnalysisByLike(Request $request, $id)
    {
        $from_date = $request->from_date . " 00:00:00";
        $to_date   = $request->to_date . " 23:59:59";
        $results   = GemsLikeunlike::where('gems_id', $id)
            ->where('action', '1')
            ->whereBetween('created_at', [$from_date, $to_date])
            ->select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as total'))
            ->groupBy('date')
            ->get();
        return response()->json($results);
    }

    /**
     * Gems analysis data by view.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */

    public function gemsAnalysisByView(Request $request, $id)
    {
        $from_date = $request->from_date . " 00:00:00";
        $to_date   = $request->to_date . " 23:59:59";
        $results   = GemsView::where('gems_id', $id)
            ->where('type', '1')
            ->whereBetween('created_at', [$from_date, $to_date])
            ->select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as total'))
            ->groupBy('date')
            ->get();
        return response()->json($results);
    }

    /**
     * Gems analysis data by comment.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */

    public function gemsAnalysisByComment(Request $request, $id)
    {
        $from_date = $request->from_date . " 00:00:00";
        $to_date   = $request->to_date . " 23:59:59";
        $results   = GemsComment::where('gems_id', $id)
            ->whereBetween('created_at', [$from_date, $to_date])
            ->select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as total'))
            ->groupBy('date')
            ->get();
        return response()->json($results);
    }

    /**
     * Gems analysis data by share.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */

    public function gemsAnalysisByShare(Request $request, $id)
    {
        $from_date = $request->from_date . " 00:00:00";
        $to_date   = $request->to_date . " 23:59:59";
        $results   = ShareGemsGroup::where('gems_id', $id)
            ->whereBetween('created_at', [$from_date, $to_date])
            ->select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as total'))
            ->groupBy('date')
            ->get();
        return response()->json($results);
    }

    /**
     * Gems analysis data by save.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */

    public function gemsAnalysisBySave(Request $request, $id)
    {        
        $from_date = $request->from_date . " 00:00:00";
        $to_date   = $request->to_date . " 23:59:59";
        $results   = GemsSaveList::where('gems_id', $id)
            ->whereBetween('created_at', [$from_date, $to_date])
            ->select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as total'))
            ->groupBy('date')
            ->get();
        return response()->json($results);
    }

    /**
     * Trending gems.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function trendingGems()
    {
        $results = Gems::withCount('total_like', 'total_comment', 'total_view', 'total_share', 'total_favorite')
            ->with(['categoryDetail', 'images'])
            ->orderBy('total_view_count', 'DESC')
            ->orderBy('total_like_count', 'DESC')
            ->orderBy('total_share_count', 'DESC')
            ->take(10)
            ->get();
        return response()->json($results);
    }

    /**
     * CMS details.
     *
     * @param  $slug
     * @return \Illuminate\Http\Response
     */
    public function getCmsDetails($slug)
    {
        try {
            $models = Cms::where('slug', '=', $slug)->where('status', 1)->first();
            return response()->json([
                'message' => $models,
                'success' => true,
            ], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    /**
     * CMS list.
     *
     * @param
     * @return \Illuminate\Http\Response
     */

    public function getCmsList()
    {
        $models = Cms::where('status', '=', '1')->get();
        return response()->json($models);
    }

    /**
     * CMS slug.
     *
     * @param
     * @return \Illuminate\Http\Response
     */

    public function checkCmsSlug()
    {
        $models = Cms::where('status', '=', '1')->get();
        $slug   = array();
        foreach ($models as $key => $value) {
            $slug[$key] = $value->slug;
        }
        return response()->json($slug);
    }

    public function newsDetails($slug)
    {
        try {
            $models = News::where('slug', '=', $slug)->where('status', 1)->first();
            return response()->json([
                'message' => $models,
                'success' => true,
            ], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    /**
     * User like how many gem.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function providerGemList(Request $request, $user_id)
    {
        $pageSize       = $request->input('pageSize') ? $request->input('pageSize') : '10';

        $data = GemsProviderRelation::with(['gems', 'gemsImages', 'gemsCategoryDetail', 'user_like_status','providerDetails','gemsCreatedBy'])
                ->where('user_id', $user_id)
                ->withCount('gemsLike')
                ->paginate($pageSize);

        return response()->json([
            'gems_list' => $data,
            'success'   => true,
        ], 200);
    }
}
