<?php

namespace App\Http\Controllers;

use App\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class SubscriptionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $pageSize     = $request->input('pageSize') ? $request->input('pageSize') : '10';
        $searchString = $request->get('searchString');
        $export       = $request->get('export');
        $user_id      = $request->get('user_id');

        $select = Subscription::orderBy('id', 'DESC');

        if ($user_id) {
            $select->where('user_id', '=', $user_id);
        }

        //$select->orderBy('id', 'DESC');

        if ($export) {
            $data = $select->get();
        } else {
            $data = $select->paginate($pageSize);
        }

        return response()->json([
            'data'    => $data,
            'success' => true,
        ], 200);
    }

    public function createXX(Request $request)
    {
        $this->validate($request, [
            'service_name'  => 'required|unique:services',
            'service_desc'  => 'required',
            'service_price' => 'required',
        ]);
        \DB::beginTransaction();
        try {

            $model                = new Subscription;
            $model->service_name  = $request->service_name;
            $model->service_desc  = $request->service_desc;
            $model->service_price = $request->service_price;
            $model->status        = $request->status;

            $model->save();

            \DB::commit();
            $message       = 'Service Created Successfully.';
            $success       = true;
            $resCode       = 201;
            $audit_message = 'CREATE_SERVICE_SUCCESS';

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $resCode       = 400;
            $audit_message = 'CREATE_SERVICE_FAIL';
        }

        $this->insertAuditTrail('SERVICE', $audit_message, $model->service_name);
        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    public function updateXX(Request $request, $id)
    {
        \DB::beginTransaction();
        try {
            $model = Subscription::find($id);
            if (!empty($model)) {
                if ($request->service_name != '') {
                    $model->service_name = $request->service_name;
                }

                if ($request->service_desc != '') {
                    $model->service_desc = $request->service_desc;
                }

                if ($request->service_price != '') {
                    $model->service_price = $request->service_price;
                }
                if ($request->status != '') {
                    $model->status = $request->status;
                }

                $model->save();
                \DB::commit();
                $message       = 'Record Updated successfully!';
                $success       = true;
                $resCode       = 201;
                $audit_message = 'UPDATE_SERVICE_SUCCESS';
            } else {
                $message       = 'Record not exists!';
                $success       = true;
                $resCode       = 400;
                $audit_message = 'UPDATE_SERVICE_FAIL';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $resCode       = 400;
            $audit_message = 'UPDATE_SERVICE_FAIL';
        }

        $this->insertAuditTrail('SERVICE', $audit_message);
        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }
    /**
     * Delete record.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteXX($id)
    {
        \DB::beginTransaction();
        try {
            $model = Subscription::find($id);
            if (!empty($model)) {
                $model->delete();

                \DB::commit();
                $message       = 'Removed successfully!';
                $success       = true;
                $audit_message = 'DELETE_SERVICES_SUCCESS';

            } else {
                $message       = 'Record not exists!';
                $success       = true;
                $audit_message = 'DELETE_SERVICES_FAIL';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $alerttype     = false;
            $audit_message = 'DELETE_SERVICES_FAIL';
        }

        //Return message
        $this->insertAuditTrail('DELETE_SERVICES', $audit_message);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 200);
    }
    /**
     * Status update.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function statusUpdateXX(Request $request, $id)
    {

        //return response()->json($request);
        \DB::beginTransaction();
        try {
            $model = Services::find($id);
            if (!empty($model)) {

                if ($model->status != (int) $request->input('status')) {
                    $model->status = $request->input('status');
                }

                $model->save();

                \DB::commit();
                $message       = 'Updated successfully!';
                $success       = true;
                $audit_message = 'STATUS_UPDATE_SERVICES_SUCCESS';

            } else {
                $message       = 'record not exists!';
                $success       = false;
                $audit_message = 'STATUS_UPDATE_SERVICES_FAIL';

            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $audit_message = 'STATUS_UPDATE_GEMS_FAIL';
        }

        $this->insertAuditTrail('STATUS_UPDATE_SERVICES', $audit_message);
        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 200);
    }

    public function creditCardLists(Request $request)
    {
        \DB::beginTransaction();
        try {
            $user_id      = Auth::user()->id;
            $subscription = Subscription::where('user_id', $user_id)->select('stripe_customer_id')->first();
            if ($subscription) {
                $stripe       = new \Stripe\StripeClient(Config::get('constants.stripe.secret'));
                $card_details = $stripe->customers->allSources(
                    $subscription->stripe_customer_id,
                    ['object' => 'card']
                );
                $message       = $card_details;
                $success       = true;
                $audit_message = 'View Card List Success';
            } else {
                $message       = 'No subscription found';
                $success       = false;
                $audit_message = 'View Card List Fail';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $audit_message = 'View Card List Fail';
        }
        $this->insertAuditTrail('VIEW_CARD_LIST', $audit_message);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 200);
    }

    public function createStripeUser(Request $request)
    {
        $number    = $request->input('number');
        $exp_month = $request->input('exp_month');
        $exp_year  = $request->input('exp_year');
        $cvc       = $request->input('cvc');
        $name      = $request->input('name');
        $countryObj = Country::find($request->input('companyCountry'));
        $stateObj   = State::find($request->input('companyState'));
        $cityObj    = City::find($request->input('companyCity'));

        $postal_code = $request->input('companyZip');
        $country     = $countryObj ? $countryObj->iso2 : '';
        $state       = $stateObj ? $stateObj->iso2 : '';
        $city        = $cityObj ? $cityObj->name : '';

        $address       = $request->input('addr1') . $request->input('addr2');
        $customer      = $request->input('company_name');
        $billing_email = $request->input('email');

        \DB::beginTransaction();
        try {
            $user_id      = Auth::user()->id;
            $subscription = Subscription::where('user_id', $user_id)->select('stripe_customer_id')->first();
            if ($subscription->stripe_customer_id == '') {
                $stripe = new \Stripe\StripeClient(Config::get('constants.stripe.secret'));

                $tokenRes = $stripe->tokens->create([
                    'card' => [
                        'number'    => $request->input('card_number'),
                        'exp_month' => $request->input('exp_month'),
                        'exp_year'  => $request->input('exp_year'),
                        'cvc'       => $request->input('cvc'),
                    ],
                ]);

                // Add customer to stripe
                try {
                    $customer = \Stripe\Customer::create([
                        'source'  => $tokenRes['id'],
                        'name'    => $name,
                        'email'   => $billing_email,
                        'address' => [
                            'line1'       => $address,
                            'postal_code' => $postal_code,
                            'city'        => $city,
                            'state'       => $state,
                            'country'     => $country,
                        ],
                    ]);
                } catch (Exception $e) {
                    $api_error = $e->getMessage();
                }

                $message       = $card_details;
                $success       = true;
                $audit_message = 'Subscription success';
            } else {
                $message       = 'Already subscribed';
                $success       = false;
                $audit_message = 'Subscription fail';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $audit_message = 'Save a new card fail';
        }

        $this->insertAuditTrail('CREATE_CARD', $audit_message);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 200);
    }

    public function createCard(Request $request)
    {
        $number    = $request->input('number');
        $exp_month = $request->input('exp_month');
        $exp_year  = $request->input('exp_year');
        $cvc       = $request->input('cvc');
        $name      = $request->input('name');

        \DB::beginTransaction();
        try {
            $user_id      = Auth::user()->id;
            $subscription = Subscription::where('user_id', $user_id)->select('stripe_customer_id')->first();
            if ($subscription) {
                $stripe   = new \Stripe\StripeClient(Config::get('constants.stripe.secret'));
                $tokenRes = $stripe->tokens->create([
                    'card' => [
                        'number'    => $number,
                        'exp_month' => $exp_month,
                        'exp_year'  => $exp_year,
                        'cvc'       => $cvc,
                        'name'      => $name,
                    ],
                ]);
                $card_details = $stripe->customers->createSource(
                    $subscription->stripe_customer_id,
                    ['source' => $tokenRes['id']]
                );
                $message       = $card_details;
                $success       = true;
                $audit_message = 'Save a new card success';
            } else {
                $message       = 'No subscription found';
                $success       = false;
                $audit_message = 'Save a new card fail';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $audit_message = 'Save a new card fail';
        }

        $this->insertAuditTrail('CREATE_CARD', $audit_message);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 200);
    }

    public function deleteCard(Request $request, $card_id)
    {
        //$card_id = $request->input('card_id');
        \DB::beginTransaction();
        try {
            $user_id      = Auth::user()->id;
            $subscription = Subscription::where('user_id', $user_id)->select('stripe_customer_id')->first();
            if ($subscription) {
                $stripe       = new \Stripe\StripeClient(Config::get('constants.stripe.secret'));
                $card_details = $stripe->customers->deleteSource(
                    $subscription->stripe_customer_id,
                    $card_id,
                    []
                );
                $message       = $card_details;
                $success       = true;
                $audit_message = 'Delete card  success';
            } else {
                $message       = 'No subscription found';
                $success       = false;
                $audit_message = 'Delete card  fail';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $audit_message = 'Delete card  fail';
        }
        $this->insertAuditTrail('DELETE_CARD', $audit_message);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 200);
    }

    public function updateCard(Request $request)
    {
        $card_id   = $request->input('card_id');
        $exp_month = $request->input('exp_month');
        $exp_year  = $request->input('exp_year');
        $name      = $request->input('name');
        \DB::beginTransaction();
        try {
            $user_id      = Auth::user()->id;
            $subscription = Subscription::where('user_id', $user_id)->select('stripe_customer_id')->first();
            if ($subscription) {
                $stripe       = new \Stripe\StripeClient(Config::get('constants.stripe.secret'));
                $card_details = $stripe->customers->updateSource(
                    $subscription->stripe_customer_id,
                    $card_id,
                    [
                        'exp_month' => $exp_month,
                        'exp_year'  => $exp_year,
                        'name'      => $name,
                    ]
                );
                $message       = $card_details;
                $success       = true;
                $audit_message = 'Update card  success';
            } else {
                $message       = 'No subscription found';
                $success       = false;
                $audit_message = 'Update card  fail';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $audit_message = 'Update card  fail';
        }
        $this->insertAuditTrail('UPDATE_CARD', $audit_message);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 200);
    }

    public function invoice(Request $request)
    {
        $id = $request->id;
        \DB::beginTransaction();
        try {
            $user_id      = Auth::user()->id;
            $subscription = Subscription::where('id', $id)->select('dataObj')->first();
            $dataObj = $subscription->dataObj;
            $jsonObj = explode('JSON:', $dataObj);
            $invoice_no  = json_decode($jsonObj[1])->latest_invoice;
            
            if ($subscription) {
                $stripe = new \Stripe\StripeClient(Config::get('constants.stripe.secret'));
                $card_details = $stripe->invoices->retrieve(
                    $invoice_no,
                    []
                  );
                $message       = $card_details->invoice_pdf;
                $success       = true;
                $audit_message = 'Invoice view success';
            } else {
                $message       = 'No subscription found';
                $success       = false;
                $audit_message = 'Invoice view  fail';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $audit_message = 'Invoice view  fail';
        }
        $this->insertAuditTrail('INVOICE_VIEW', $audit_message);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 200);
    }

}
