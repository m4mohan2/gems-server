<?php

namespace App\Http\Controllers;

use App\Category;
use App\CategorySubcategoryRelation;
use App\Subcategory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Faq list for admin.
     *
     * @param  Request  $request
     * @return Response
     */

    public function importSubcat()
    {
        $file_path = "subcategory.txt";
        if (file_exists($file_path)) {
            $lines         = explode("\n", file_get_contents($file_path));
            $relation_name = "";
            foreach ($lines as $key => $line) {
                $category_names       = explode('_', $line);
                $category_name        = implode(' ', $category_names);
                $new_category         = new Subcategory;
                $new_category->type   = str_replace("\r", '', $line);
                $new_category->name   = ucfirst(str_replace("\r", '', $category_name));
                $new_category->status = 1;
                $new_category->save();
            }
        }
    }

    /**
     * Category list for apps.
     *
     * @param  Request  $request
     * @return Response
     */

    public function categoryList()
    {

        try {
            $models      = Category::with(['assignedSubcategory'])->where('status', '1')->get();
            $subcategory = Subcategory::where('status', '1')->get();
            $message     = $models;
            $success     = true;
            $resCode     = 200;
        } catch (\Exception $e) {
            $message     = $e->getMessage();
            $subcategory = '';
            $success     = true;
            $resCode     = 400;
        }

        return response()->json([
            'category'         => $message,
            'subcategory_list' => $subcategory,
            'success'          => $success,
        ], $resCode);

    }

    /**
     * Sub Category list for apps.
     *
     * @param  Request  $request
     * @return Response
     */

    public function subcategoryAppList()
    {

        try {
            $models  = Subcategory::where('status', '1')->get();
            $message = $models;
            $success = true;
            $resCode = 200;
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $success = true;
            $resCode = 400;
        }

        return response()->json([
            'details' => $message,
            'success' => $success,
        ], $resCode);

    }

    /**
     * Category list for admin.
     *
     * @param  Request  $request
     * @return Response
     */

    public function categoryListAdmin(Request $request)
    {
        $pageSize = $request->input('pageSize') ? $request->input('pageSize') : '10';
        $models   = Category::with(['subcategory'])->paginate($pageSize);
        return response()->json($models);
    }

    /**
     * Add Category.
     *
     * @param  Request  $request
     * @return Response
     */

    public function addCategory(Request $request)
    {
        $this->validate($request, [
            'category_name' => 'required',
            //'description'   => 'required',
        ]);

        try {

            $models                = new Category;
            $models->category_name = $request->category_name;
            $models->description   = $request->description;
            $models->status        = $request->status;
            if ($models->save()) {
                $subcateegories = $request->subcategory;
                if (count($subcateegories) > 0) {
                    foreach ($subcateegories as $key => $subcat) {
                        $sub                 = new CategorySubcategoryRelation;
                        $sub->category_id    = $models->id;
                        $sub->subcategory_id = $subcat['value'];
                        $sub->save();
                    }
                }
            }

            return response()->json([
                'message' => 'Record added successfully',
                'success' => true,
            ], 200);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 500);
        }
    }

    public function updateCategory(Request $request, $id)
    {
        try {
            $models = Category::find($id);

            if ($request->category_name != '') {
                $models->category_name = $request->category_name;
            }

            if ($request->description != '') {
                $models->description = $request->description;
            }
            if ($request->status != '') {
                $models->status = $request->status;
            }

            if ($models->save()) {
                CategorySubcategoryRelation::where('category_id', $id)->delete();
                $subcateegories = $request->subcategory;
                if (count($subcateegories) > 0) {
                    foreach ($subcateegories as $key => $subcat) {
                        $sub                 = new CategorySubcategoryRelation;
                        $sub->category_id    = $models->id;
                        $sub->subcategory_id = $subcat['value'];
                        $sub->save();
                    }
                }
            }
            return response()->json([
                'message' => 'Record updated successfully',
                'user'    => $models,
                'success' => true,
            ], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'success' => false], 400);
        }
    }

    public function statusUpdate(Request $request, $id)
    {

        //return response()->json($request);
        \DB::beginTransaction();
        try {
            $model = Category::find($id);
            if (!empty($model)) {

                if ($model->status != (int) $request->input('status')) {
                    $model->status = $request->input('status');
                }

                $model->save();

                \DB::commit();
                $message       = 'Updated successfully!';
                $success       = true;
                $audit_message = 'STATUS_UPDATE_CATEGORY_SUCCESS';

            } else {
                $message       = 'record not exists!';
                $success       = false;
                $audit_message = 'STATUS_UPDATE_CATEGORY_FAIL';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $audit_message = 'STATUS_UPDATE_CATEGORY_FAIL';
        }

        $this->insertAuditTrail('STATUS_UPDATE_CATEGORY', $audit_message);
        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 200);
    }

    /**
     * Add Subcategory.
     *
     * @param  Request  $request
     * @return Response
     */

    public function addSubcategory(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            //'description'   => 'required',
        ]);

        try {

            $models         = new Subcategory;
            $models->name   = $request->name;
            $models->type   = preg_replace("![^a-z0-9]+!i", "-", strtolower($models->name));
            $models->status = $request->status;
            $models->save();
            return response()->json([
                'message' => 'Record added successfully',
                'success' => true,
            ], 200);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 500);
        }
    }

    /**
     * Update Subcategory.
     *
     * @param  Request  $request
     * @return Response
     */

    public function updateSubcategory(Request $request, $id)
    {
        try {
            $models = Subcategory::find($id);

            if ($request->name != '') {
                $models->name = $request->name;
            }
            $models->save();

            return response()->json([
                'message' => 'Record updated successfully',
                'success' => true,
            ], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'success' => false], 400);
        }
    }

    /**
     * Subcategory Status Update.
     *
     * @param  Request  $request
     * @return Response
     */

    public function statusSubcategoryUpdate(Request $request, $id)
    {

        //return response()->json($request);
        \DB::beginTransaction();
        try {
            $model = Subcategory::find($id);
            if (!empty($model)) {

                if ($model->status != (int) $request->input('status')) {
                    $model->status = $request->input('status');
                }

                $model->save();

                \DB::commit();
                $message       = 'Updated successfully!';
                $success       = true;
                $audit_message = 'STATUS_UPDATE_SUBCATEGORY_SUCCESS';

            } else {
                $message       = 'record not exists!';
                $success       = false;
                $audit_message = 'STATUS_UPDATE_SUBCATEGORY_FAIL';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $audit_message = 'STATUS_UPDATE_SUBCATEGORY_FAIL';
        }

        $this->insertAuditTrail('STATUS_UPDATE_SUBCATEGORY', $audit_message);
        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 200);
    }

    /**
     * SubCategory Delete record.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteSubcategory($id)
    {
        \DB::beginTransaction();
        try {
            $model = Subcategory::find($id);
            if (!empty($model)) {
                $model->delete();

                \DB::commit();
                $message       = 'Removed successfully!';
                $success       = true;
                $audit_message = 'DELETE_CATEGORY_SUCCESS';

            } else {
                $message       = 'Record not exists!';
                $success       = true;
                $audit_message = 'DELETE_CATEGORY_FAIL';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $alerttype     = false;
            $audit_message = 'DELETE_CATEGORY_FAIL';
        }
    }

    /**
     * Category Delete record.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        \DB::beginTransaction();
        try {
            $model = Category::find($id);
            if (!empty($model)) {
                $model->delete();

                \DB::commit();
                $message       = 'Removed successfully!';
                $success       = true;
                $audit_message = 'DELETE_CATEGORY_SUCCESS';

            } else {
                $message       = 'Record not exists!';
                $success       = true;
                $audit_message = 'DELETE_CATEGORY_FAIL';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $alerttype     = false;
            $audit_message = 'DELETE_CATEGORY_FAIL';
        }

        //Return message
        $this->insertAuditTrail('DELETE_CATEGORY', $audit_message);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 200);
    }

    //

    public function subcategoryList()
    {
        $models  = SubCategory::where('status', '1')->orderBy('name', 'ASC')->get();
        $results = array();
        if (count($models) > 0) {
            foreach ($models as $key => $sub) {
                $results[$key]['label'] = $sub->name;
                $results[$key]['value'] = $sub->id;
            }
        }
        return response()->json($results);
    }

    /**
     * SubCategory list for admin.
     *
     * @param  Request  $request
     * @return Response
     */

    public function subcategoryListAdmin(Request $request)
    {
        $pageSize = $request->input('pageSize') ? $request->input('pageSize') : '10';
        $models   = Subcategory::paginate($pageSize);
        return response()->json($models);
    }
}
