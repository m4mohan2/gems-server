<?php

namespace App\Http\Controllers;

use App\AuditTrail;

class AudittrailController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //

    protected function insertAuditTrail($page, $description, $action_id = null, $action_to = null)
    {
        $model              = new AuditTrail;
        $model->page        = $page;
        $model->description = $description;
        $model->action_id   = $action_id;
        $model->action_to   = $action_to;
        $model->action_by   = Auth::user()->id;
        $model->ip_address  = $this->get_client_ip();
        $model->save();
    }
}
