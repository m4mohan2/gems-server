<?php

namespace App\Http\Controllers;

use App\Gems;
use App\GemsCategoryRelation;
use App\GemsComment;
use App\GemsCommentImage;
use App\GemsFavorite;
use App\GemsFeed;
use App\GemsImage;
use App\GemsLabel;
use App\GemsLikeunlike;
use App\GemsProviderRelation;
use App\GemsSaveList;
use App\GemsSubcategoryRelation;
use App\GemsView;
use App\GroupMember;
use App\NotificationSetting;
use App\ShareGems;
use App\ShareGemsGroup;
use App\User;
use App\UserDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Validator;

class GemsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Gems List for frontend.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $perPage     = 10;
        $searchQuery = $request->get('searchQuery');
        $user_id     = Auth::user()->id;

        $models = Gems::with([
            'categoryDetail', 'createdBy', 'createdByUserDetail', 'images', 'sender_friend_status' => function ($query) use ($user_id) {
                $query->where('user_friends.receiver', $user_id);
                $query->where('user_friends.status', 1);
            }, 'receiver_friend_status' => function ($query) use ($user_id) {
                $query->where('user_friends.sender', $user_id);
                $query->where('user_friends.status', 1);
            }])
        //->where('gems.created_by', '=', $user_id)
            ->orderBy('gems.id', 'DESC');

        if ($searchQuery) {
            $results = $models->paginate($perPage);
        } else {
            $results = $models->get();
        }

        return response()->json($results);
    }

    /**
     * Gems Search by place id.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function gemsByPlaceId(Request $request)
    {
        $gems_name = $request->gems_name;
        $place_id  = $request->place_id;
        $models    = Gems::with([
            'categoryDetail', 'createdBy', 'createdByUserDetail', 'images',
        ]);
        if ($place_id != '') {
            $models->where('place_id', '=', $place_id);
        }
        if ($gems_name != '') {
            $models->where('name', 'LIKE', '%' . $gems_name . '%');
        }
        $results = $models->get();

        return response()->json($results);
    }

    /**
     * Gems List for frontend.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function gemsListByDistance(Request $request)
    {
        $latitude       = $request->get('lat');
        $longitude      = $request->get('lng');
        $logged_user_id = Auth::user()->id;
        $models         = Gems::with([
            'categoryDetail', 'createdBy', 'createdByUserDetail', 'images', 'user_like_status' => function ($q) use ($logged_user_id) {
                $q->where('user_id', $logged_user_id);
                $q->select('gems_id', 'action');
            }, 'sender_friend_status' => function ($query) use ($logged_user_id) {
                $query->where('user_friends.receiver', $logged_user_id);
                $query->where('user_friends.status', 1);
            }, 'receiver_friend_status' => function ($query) use ($logged_user_id) {
                $query->where('user_friends.sender', $logged_user_id);
                $query->where('user_friends.status', 1);
            },
        ])->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians( lat ) ) ) ) AS distance'))
            ->withCount(['total_like'])
            ->having('distance', '<', 1)
            ->orderBy('distance');
        //->where('gems.created_by', '=', $user_id)
        //->orderBy('gems.id', 'DESC');

        $results = $models->get();

        return response()->json($results);
    }

    /**
     * Gems search for website.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function gemsSearch(Request $request)
    {
        $latitude  = $request->get('lat');
        $longitude = $request->get('lng');

        $models = Gems::with([
            'categoryDetail', 'createdBy', 'createdByUserDetail', 'images',
        ])->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians( lat ) ) ) ) AS distance'))
            ->having('distance', '<', 2)
            ->orderBy('distance')
            ->where('status', '=', 1);
        //->orderBy('gems.id', 'DESC');

        $results = $models->get();

        return response()->json($results);
    }

    /**
     * Gems List for admin panel
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function gemList(Request $request)
    {
        $latitude  = $request->get('latitude');
        $longitude = $request->get('longitude');
        $gems_name = $request->get('gems_name');
        $status    = $request->get('status');
        $pageSize  = $request->get('pageSize') ? $request->get('pageSize') : '10';

        //$latitude = "23.5376328";
        //$longitude = "87.3016263";

        $models = Gems::with([
            'categoryDetail', 'createdBy', 'createdByUserDetail', 'images', 'providers',
        ])->withCount('total_like');

        if ($latitude != '' && $longitude != '') {
            $models->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians( lat ) ) ) ) AS distance'))
                ->having('distance', '<', 1);
        }
        if ($status != '') {
            $models->where('status', '=', $status);
        }

        if ($gems_name != '') {
            $models->where('name', 'LIKE', '%' . $gems_name . '%');
        }

        //$models->orderBy('distance');
        //->where('gems.created_by', '=', $user_id)
        $models->orderBy('gems.id', 'DESC');

        $results = $models->paginate($pageSize);
        //$results['total_like'] = GemsLikeunlike::where('gems_id', $id)->where('action', 1)->count();

        return response()->json($results);
    }

    /**
     * All Gems List
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function allGemList(Request $request)
    {
        $name    = $request->get('name');
        $user_id = Auth::user()->id;

        $models = Gems::where('status', '1');
        if (Auth::user()->user_type == 2) {

            $provider    = GemsProviderRelation::where('user_id', $user_id)->select('gems_id')->where('action', '1')->get();
            $providerArr = json_decode(json_encode($provider));
            $gems_ids    = array_column($providerArr, 'gems_id');
            $models->whereIn('id', $gems_ids);
        }
        if ($name != '') {
            $models->where('name', 'like', '%' . $name . '%');
        }
        $models->orderBy('name', 'ASC');
        $results = $models->get();

        return response()->json($results);
    }

    /**
     * created new record.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //dd($request);
        $user_id     = Auth::user()->id;
        $gems_name   = $request->input('name');
        $gems_status = $request->input('status');

        $validator = Validator::make($request->all(),
            [
                'name'     => 'required|unique:gems',
                'place_id' => 'required|unique:gems',
                'lat'      => 'required',
                'lng'      => 'required',
                'address'  => 'required',
            ],
            [
                'name.unique'     => 'This GEM is already created. Please try a different one',
                'place_id.unique' => 'This GEM is already created. Please try a different one',
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()->all(),
                'success' => false,
            ], 400);
        }

        \DB::beginTransaction();
        try {

            $model               = new Gems;
            $model->name         = $request->input('name');
            $model->lat          = $request->input('lat');
            $model->lng          = $request->input('lng');
            $model->address      = $request->input('address');
            $model->country_code = $request->input('country_code');
            $model->phone_no     = $request->input('phone_no');
            $model->note         = $request->input('note');
            $model->privacy      = $request->input('privacy');

            $model->created_by          = Auth::user()->id;
            $model->original_created_by = Auth::user()->id;
            if ($gems_status === '') {
                $model->status = 1;
            } else {
                $model->status = $gems_status;
            }

            if ($request->input('place_id') && $request->input('place_id') != '') {
                $model->place_id = $request->input('place_id');
            } else {
                $model->place_id = bin2hex(openssl_random_pseudo_bytes(25));
            }

            if ($model->save()) {
                $categories         = $request->input('category');
                $subcategories      = $request->input('subcategory');
                $google_photo_links = $request->input('google_photo_link');
                $group_id           = $request->input('group_id');

                $this->addGemFeed($model->id, 'Create');

                if (is_array($google_photo_links)) {
                    for ($l = 0; $l < count($google_photo_links); $l++) {
                        $gemsImage          = new GemsImage;
                        $gemsImage->image   = $google_photo_links[$l];
                        $gemsImage->gems_id = $model->id;
                        $gemsImage->type    = 'cover_image';
                        $gemsImage->source  = 'google_link';
                        $gemsImage->save();
                    }
                }

                // if (is_array($categories)) {
                //     $count_category = count($categories);
                //     for ($i = 0; $i < $count_category; $i++) {
                //         $gems_category = GemsCategory::where('category_type', '=', $categories[$i])->first();
                //         if ($gems_category) {
                //             $category_id = $gems_category->id;
                //         } else {
                //             $category_names              = explode('_', $categories[$i]);
                //             $category_name               = implode(' ', $category_names);
                //             $new_category                = new GemsCategory;
                //             $new_category->category_type = $categories[$i];
                //             $new_category->category_name = ucfirst($category_name);
                //             $new_category->status        = 1;
                //             $new_category->is_approved   = 1;
                //             $new_category->save();
                //             $category_id = $new_category->id;
                //         }

                //         $GemsCategoryRelation              = new GemsCategoryRelation;
                //         $GemsCategoryRelation->gems_id     = $model->id;
                //         $GemsCategoryRelation->category_id = $category_id;
                //         $GemsCategoryRelation->save();
                //     }
                // }

                if (is_array($categories)) {
                    foreach ($categories as $key => $category) {
                        $GemsCategoryRelation              = new GemsCategoryRelation;
                        $GemsCategoryRelation->gems_id     = $model->id;
                        $GemsCategoryRelation->category_id = $category;
                        $GemsCategoryRelation->save();
                    }

                }

                if (is_array($subcategories)) {
                    foreach ($subcategories as $key => $subcategory) {
                        $GemsSubCategoryRelation                 = new GemsSubcategoryRelation;
                        $GemsSubCategoryRelation->gems_id        = $model->id;
                        $GemsSubCategoryRelation->subcategory_id = $subcategory;
                        $GemsSubCategoryRelation->save();
                    }

                }

                if ($group_id != '') {
                    $gems_id     = $model->id;
                    $groupMember = GroupMember::where('group_id', $group_id)->get();

                    $sender_user_info = UserDetail::where('user_id', $user_id)->select('first_name', 'last_name')->first();
                    $title            = "New GEM shared";
                    // $push_message     = $sender_user_info->first_name . " " . $sender_user_info->last_name . ' shared a gem: ' . $gems_name;
                    $push_message = "Hey checkout the new GEM " . $gems_name . " created by " . $sender_user_info->first_name . " " . $sender_user_info->last_name;

                    foreach ($groupMember as $member) {
                        $modelShareGems           = new ShareGems;
                        $modelShareGems->user_id  = $member->user_id;
                        $modelShareGems->gems_id  = $gems_id;
                        $modelShareGems->share_by = $user_id;
                        $modelShareGems->save();
                        if ($member->user_id != $user_id && $gems_status != 0) {
                            $receiver_device_token = User::where('id', $member->user_id)->select('device_token')->first();
                            if ($receiver_device_token->device_token != '') {
                                $this->fcm_send($receiver_device_token->device_token, $push_message, $title);
                            }
                        }
                    }
                    $modelSGGroup           = new ShareGemsGroup;
                    $modelSGGroup->group_id = $group_id;
                    $modelSGGroup->gems_id  = $gems_id;
                    $modelSGGroup->share_by = $user_id;
                    $modelSGGroup->save();
                    $appmessage = Config::get('messages.audit.share_gem');
                    $platform   = $request->input('platform', 'Web');
                    $this->insertAuditTrail('SHARE', 'SHARE_GEMS_GROUP_SUCCESS', $appmessage, $platform, $gems_name);
                }
            }

            $sql = "select user_id
        from (
          select sender as friend, status
            from user_friends
           where receiver = $user_id
          union
          select receiver as friend, status
            from user_friends
           where sender = $user_id
          ) as friends
        inner join user_details
        on user_details.user_id = friends.friend where friends.status = 1";

            $friend_info = \DB::select(\DB::raw($sql));
            $friendArr   = array();
            if (count($friend_info) > 0) {
                $sender_user_info = UserDetail::where('user_id', $user_id)->select('first_name', 'last_name')->first();
                $title            = "New GEM created";
                // $push_message     = $sender_user_info->first_name . " " . $sender_user_info->last_name . ' has created a new gem: ' . $gems_name;
                $push_message = " A new GEM " . $gems_name . " has been created by " . $sender_user_info->first_name . " " . $sender_user_info->last_name;
                foreach ($friend_info as $key => $friend) {
                    $receiver_device_token = User::where('id', $friend->user_id)->select('device_token')->first();
                    if ($receiver_device_token->device_token != '') {
                        $this->fcm_send($receiver_device_token->device_token, $push_message, $title);
                    }
                }
            }

            \DB::commit();
            $message       = 'Gem Created Successfully.';
            $success       = true;
            $resCode       = 201;
            $audit_message = 'CREATE_GEMS_SUCCESS';
            $audit_on      = $model->name;
            $gems_id       = $model->id;
            $appmessage    = Config::get('messages.audit.create_gem');

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $resCode       = 400;
            $audit_message = 'CREATE_GEMS_FAIL';
            $audit_on      = '';
            $gems_id       = '';
            $appmessage    = Config::get('messages.audit.create_gem');
        }
        $platform = $request->input('platform', 'Web');
        $this->insertAuditTrail('GEMS', $audit_message, $appmessage, $platform, $audit_on);
        //Return message
        return response()->json([
            'message' => $message,
            'gems_id' => $gems_id,
            'success' => $success,
        ], 201);

    }

    /**
     * Update record
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_id = Auth::user()->id;
        \DB::beginTransaction();
        try {

            $model     = Gems::find($id);
            $gems_name = $model->name;

            if (!empty($model)) {
                $model->country_code = $request->input('country_code');
                $model->phone_no     = $request->input('phone_no');
                $model->note         = $request->input('note');

                if ($request->input('privacy') != '') {
                    $model->privacy = $request->input('privacy');
                }

                if ($model->save()) {
                    $this->addGemFeed($id, 'Modify');
                    GemsCategoryRelation::where('gems_id', $id)->delete();
                    $categories    = $request->input('category');
                    $subcategories = $request->input('subcategory');
                    $new_group_id  = $request->input('new_group_id');
                    $old_group_id  = $request->input('old_group_id');

                    // if (is_array($categories)) {
                    //     $count_category = count($categories);
                    //     for ($i = 0; $i < $count_category; $i++) {
                    //         $gems_category = GemsCategory::where('category_type', '=', $categories[$i])->first();
                    //         if ($gems_category) {
                    //             $category_id = $gems_category->id;
                    //         } else {
                    //             $category_names              = explode('_', $categories[$i]);
                    //             $category_name               = implode(' ', $category_names);
                    //             $new_category                = new GemsCategory;
                    //             $new_category->category_type = $categories[$i];
                    //             $new_category->category_name = ucfirst($category_name);
                    //             $new_category->status        = 1;
                    //             $new_category->is_approved   = 1;
                    //             $new_category->save();
                    //             $category_id = $new_category->id;
                    //         }

                    //         $GemsCategoryRelation              = new GemsCategoryRelation;
                    //         $GemsCategoryRelation->gems_id     = $model->id;
                    //         $GemsCategoryRelation->category_id = $category_id;
                    //         $GemsCategoryRelation->save();
                    //     }
                    // }

                    if (is_array($categories)) {
                        foreach ($categories as $key => $category) {
                            $GemsCategoryRelation              = new GemsCategoryRelation;
                            $GemsCategoryRelation->gems_id     = $model->id;
                            $GemsCategoryRelation->category_id = $category;
                            $GemsCategoryRelation->save();
                        }

                    }

                    if (is_array($subcategories)) {
                        foreach ($subcategories as $key => $subcategory) {
                            $GemsSubCategoryRelation                 = new GemsSubcategoryRelation;
                            $GemsSubCategoryRelation->gems_id        = $model->id;
                            $GemsSubCategoryRelation->subcategory_id = $subcategory;
                            $GemsSubCategoryRelation->save();
                        }

                    }

                    if ($old_group_id != '') {
                        GroupMember::where('group_id', $old_group_id)->delete();
                        ShareGemsGroup::where('group_id', $old_group_id)->delete();
                    }
                    if ($new_group_id != '') {
                        $gems_id     = $model->id;
                        $groupMember = GroupMember::where('group_id', $new_group_id)->get();
                        foreach ($groupMember as $member) {
                            $modelShareGem           = new ShareGems;
                            $modelShareGem->user_id  = $member->user_id;
                            $modelShareGem->gems_id  = $gems_id;
                            $modelShareGem->share_by = $user_id;
                            $modelShareGem->save();
                        }
                        $modelSGGroup           = new ShareGemsGroup;
                        $modelSGGroup->group_id = $new_group_id;
                        $modelSGGroup->gems_id  = $gems_id;
                        $modelSGGroup->share_by = $user_id;
                        $modelSGGroup->save();
                    }
                }

                $sql = "select user_id
                    from (
                      select sender as friend, status
                        from user_friends
                       where receiver = $user_id
                      union
                      select receiver as friend, status
                        from user_friends
                       where sender = $user_id
                      ) as friends
                    inner join user_details
                    on user_details.user_id = friends.friend where friends.status = 1";

                $friend_info = \DB::select(\DB::raw($sql));
                $friendArr   = array();
                if (count($friend_info) > 0) {
                    $sender_user_info = UserDetail::where('user_id', $user_id)->select('first_name', 'last_name')->first();
                    $title            = "GEM updated";
                    $push_message     = $sender_user_info->first_name . " " . $sender_user_info->last_name . ' has updated the GEM : ' . $gems_name;
                    foreach ($friend_info as $key => $friend) {
                        if ($friend->user_id != $user_id) {
                            $receiver_device_token = User::where('id', $friend->user_id)->select('device_token')->first();
                            if ($receiver_device_token->device_token != '') {
                                $this->fcm_send($receiver_device_token->device_token, $push_message, $title);
                            }
                        }
                    }
                }

                \DB::commit();
                $message       = 'Updated successfully!';
                $success       = true;
                $audit_message = 'UPDATE_GEMS_SUCCESS';
                $audit_on      = $model->name;

            } else {
                $message       = 'Record not exists!';
                $success       = true;
                $audit_message = 'UPDATE_GEMS_FAIL';
                $audit_on      = '';
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $audit_message = 'UPDATE_GEMS_FAIL';
            $audit_on      = '';
        }
        $appmessage = Config::get('messages.audit.update_gem');
        $platform   = $request->input('platform', 'Web');
        //Return message
        $this->insertAuditTrail('GEMS', $audit_message, $appmessage, $platform, $audit_on);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 200);
    }

    public function searchPlaces(Request $request)
    {
        // $this->validate($request, [
        //     'keyword' => 'required',
        //     'radius'  => 'required',
        //     'lat'     => 'required',
        //     'lng'     => 'required',
        // ]);

        $validator = Validator::make($request->all(),
            [
                'keyword' => 'required',
                'radius'  => 'required',
                'lat'     => 'required',
                'lng'     => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()->all(),
                'success' => false,
            ], 400);
        }

        \DB::beginTransaction();
        try {

            $string = $request->input('keyword');
            $radius = $request->input('radius') ? $request->input('radius') : '50000'; //The maximum allowed radius is 50000 meters.
            $lat    = $request->input('lat');
            $lng    = $request->input('lng');

            $KEY = '&key=' . Config::get('constants.google.apikey');

            $initialUrl = 'https://maps.googleapis.com/maps/api/place/textsearch/json?';

            //https://maps.googleapis.com/maps/api/place/textsearch/json?query=hospital near by me&location=23.520445,87.311920&radius=10000&key=AIzaSyD_BBiV2jnDTjEMxjvFncobeQ3RY0wUP-Y

            //$fields = '&fields=formatted_address,name,geometry,business_status,place_id';
            $fields = '&fields=photos,formatted_address,name,rating,opening_hours,geometry,business_status,place_id,plus_code,types,icon';

            $inputtype = '&inputtype=textquery';

            //$locationbias = '&locationbias=circle:' . $radius . '@' . $lat . ',' . $lng;
            $locationbias = '&location=' . $lat . ',' . $lng . '&radius=' . $radius;

            $googleApiUrl = $initialUrl . 'query=' . urlencode($string) . $inputtype . $locationbias . $fields . $KEY;

            $result    = file_get_contents($googleApiUrl);
            $resultArr = json_decode(utf8_encode($result), true);

            //return response()->json($googleApiUrl);
            //return response()->json($resultArr);
            $appmessage = Config::get('messages.audit.gem_search');
            $platform   = $request->input('platform', 'Web');
            $this->insertAuditTrail('GEMS', 'GEM_SEARCH', $appmessage, $platform, $string);
            return response()->json([
                'data'    => $resultArr,
                //'totalCount' => $models->count(),
                'success' => true,
            ], 200);

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
        }

    }

    public function placeDetails(Request $request)
    {
        $this->validate($request, [
            'place_id'    => 'required',
            'category_id' => 'required',
        ]);

        \DB::beginTransaction();
        try {

            $category_id = $request->input('category_id');

            $string = $request->input('place_id');
            $bias   = $request->input('bias');

            $KEY = '&key=' . Config::get('constants.google.apikey');

            $initialUrl = 'https://maps.googleapis.com/maps/api/place/details/json?';

            //'https://maps.googleapis.com/maps/api/place/details/json?place_id=ChIJN1t_tDeuEmsRUsoyG83frY4&fields=name,rating,formatted_phone_number&key=YOUR_API_KEY'

            $fields = '&fields=photos,formatted_address,name,rating,opening_hours,geometry,business_status,place_id,plus_code,types,icon';

            $googleApiUrl = $initialUrl . 'place_id=' . urlencode($string) . $fields . $KEY;

            $result = file_get_contents($googleApiUrl);
            $data   = json_decode(utf8_encode($result), true);

            //return response()->json($googleApiUrl);
            return response()->json($data);

            //$this->createGems($data,$request->input('category_id'));

            return response()->json([
                'data'    => $resultArr,
                'success' => true,
            ], 200);

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
        }

    }

    /**
     * Delete record.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        \DB::beginTransaction();
        try {
            $model = Gems::find($id);
            if (!empty($model)) {
                $model->delete();

                \DB::commit();
                $message       = 'Removed successfully!';
                $success       = true;
                $resCode       = 200;
                $audit_message = 'DELETE_GEMS_SUCCESS';
                $audit_on      = $model->name;

            } else {
                $message       = 'Record not exists!';
                $success       = false;
                $resCode       = 400;
                $audit_message = 'DELETE_GEMS_FAIL';
                $audit_on      = '';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $alerttype     = false;
            $audit_message = 'DELETE_GEMS_FAIL';
            $audit_on      = '';
        }

        //Return message
        $appmessage = Config::get('messages.audit.delete_gem');

        $this->insertAuditTrail('GEMS', $audit_message, $appmessage, '', $audit_on);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    /**
     * Gems details.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */

    public function gemsDetails($id)
    {
        $gemInfo    = Gems::find($id);
        $user_id    = Auth::user()->id;
        $creator_id = $gemInfo->created_by;
        $sql        = "select user_id
        from (
          select sender as friend, status
            from user_friends
           where receiver = $creator_id
          union
          select receiver as friend, status
            from user_friends
           where sender = $creator_id
          ) as friends
        inner join user_details
        on user_details.user_id = friends.friend where friends.status = 1";

        $friend_info = \DB::select(\DB::raw($sql));
        $friendArr   = [];
        if (count($friend_info) > 0) {
            foreach ($friend_info as $key => $friend) {
                $friendArr[] = $friend->user_id;
            }
        }
        //print_r($friendArr); die();
        if ((Auth::user()->user_type == 0) || $gemInfo->privacy == 1 || ($user_id == $creator_id) || ($gemInfo->privacy == 3 && in_array($user_id, $friendArr))) {
            $model = Gems::with([
                'createdBy', 'providerName', 'createdByUserDetail', 'categoryDetail', 'subcategoryDetail', 'images', 'share_group', 'saved_lists' => function ($q) use ($user_id) {
                    $q->where('user_id', $user_id);
                }])->withCount(['total_view', 'total_share', 'total_comment', 'saved_lists'])->find($id);

            //$model['catrgory'] = GemsCategoryRelation::with(['categoryDetail'])->where('gems_id',$id)->get();
            $model['total_like'] = GemsLikeunlike::where('gems_id', $id)->where('action', 1)->count();

            $checkLike = GemsLikeunlike::where('gems_id', $id)->where('user_id', $user_id)->where('action', '1')->first();
            if (empty($checkLike)) {
                $model['logged_user_like'] = 'No';
            } else {
                $model['logged_user_like'] = 'Yes';
            }
            //$model['total_like_users'] = GemsLikeunlike::with(['users'])->where('gems_id', $id)->where('action', 1)->select('id')->limit(4)->get();
            $total_like_user           = DB::table('gems_likeunlikes')->join('user_details', 'user_details.user_id', 'gems_likeunlikes.user_id')->where('gems_id', $id)->where('action', 1)->limit(4)->get();
            $model['total_like_users'] = $total_like_user;

            $model['comments'] = GemsComment::with(['images', 'users', 'sender_friend_status' => function ($query) use ($user_id) {
                $query->where('user_friends.receiver', $user_id);
            }, 'receiver_friend_status' => function ($query) use ($user_id) {
                $query->where('user_friends.sender', $user_id);
            }])->where('gems_id', $id)->limit(4)->get();

            //$model['total_dislike'] = GemsLikeunlike::where('gems_id', $id)->where('action', 2)->count();

            $message       = $model;
            $success       = true;
            $resCode       = 200;
            $audit_message = 'VIEW_GEMS_SUCCESS';
            $audit_on      = $model->name;

        } else {
            $gemsDetails   = Gems::with(['images'])->select('id', 'name', 'address', 'privacy')->find($id);
            $message       = $gemsDetails;
            $success       = true;
            $resCode       = 200;
            $audit_message = 'VIEW_GEMS_SUCCESS';
            $audit_on      = $gemsDetails->name;

        }
        $appmessage = Config::get('messages.audit.view_gem');

        //Return message
        $this->insertAuditTrail('GEMS', $audit_message, $appmessage, '', $audit_on);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    /**
     * User like to a gem.
     *
     * @param  \Illuminate\Http\Request  $request,$gems_id
     * @return \Illuminate\Http\Response
     */

    public function likeUserList(Request $request, $gems_id)
    {
        $pageSize        = $request->input('pageSize') ? $request->input('pageSize') : '20';
        $total_like_user = DB::table('gems_likeunlikes')->join('user_details', 'user_details.user_id', 'gems_likeunlikes.user_id')->where('gems_id', $gems_id)->where('action', 1);
        $total_like_user = $total_like_user->paginate($pageSize);
        return response()->json([
            'uset_list' => $total_like_user,
            'success'   => true,
        ], 200);
    }

    /**
     * User like how many gem.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function likeGemByUserList(Request $request, $user_id)
    {
        $logged_user_id = Auth::user()->id;
        $pageSize       = $request->input('pageSize') ? $request->input('pageSize') : '20';
        $total_like_gem = GemsLikeunlike::with(['gems', 'gemsImages', 'gemsCategoryDetail', 'gemsCreatedBy', 'users', 'user_like_status' => function ($q) use ($logged_user_id) {
            $q->where('user_id', $logged_user_id);
            $q->select('gems_id', 'action');
        }])
            ->withCount('total_like')
            ->where('user_id', $user_id)->where('action', 1);
        $total_like_gem = $total_like_gem->paginate($pageSize);

        return response()->json([
            'gems_list' => $total_like_gem,
            'success'   => true,
        ], 200);
    }

    /**
     * User created how many gem.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function createdGemByUserList(Request $request, $user_id)
    {
        $logged_user_id = Auth::user()->id;
        $name           = $request->name;
        $pageSize       = $request->input('pageSize') ? $request->input('pageSize') : '20';
        $total_gem      = Gems::with(['categoryDetail', 'images', 'likeUnlike', 'user_like_status' => function ($q) use ($logged_user_id) {
            $q->where('user_id', $logged_user_id);
            $q->select('gems_id', 'action');
        }])
            ->withCount('total_like')
            ->orderBy('total_like_count', 'DESC')
            ->where('created_by', $user_id)
            ->where('name', 'like', '%' . $name . '%');
        $total_gem = $total_gem->paginate($pageSize);

        return response()->json([
            'gems_list' => $total_gem,
            'success'   => true,
        ], 200);
    }

    /**
     * User gem like status.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function gemLikeStatus(Request $request, $user_id, $gem_id)
    {
        $like_status = GemsLikeunlike::where('gems_id', $gem_id)->where('user_id', $user_id)->select('action')->first();

        return response()->json([
            'like'    => $like_status,
            'success' => true,
        ], 200);
    }

    /**
     * User favourite how many gem.
     *
     * @param  \Illuminate\Http\Request  $request,$user_id
     * @return \Illuminate\Http\Response
     */

    public function favouriteGemByUserList(Request $request, $user_id)
    {
        $pageSize  = $request->input('pageSize') ? $request->input('pageSize') : '20';
        $total_gem = GemsFavorite::with(['gems', 'gemsImages'])->where('user_id', $user_id);
        $total_gem = $total_gem->paginate($pageSize);

        return response()->json([
            'gems_list' => $total_gem,
            'success'   => true,
        ], 200);
    }

    /**
     * Status update.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function statusUpdate(Request $request, $id)
    {
        //return response()->json($request);
        \DB::beginTransaction();
        try {
            $model = Gems::find($id);
            if (!empty($model)) {

                if ($model->status != (int) $request->input('status')) {
                    $model->status = $request->input('status');
                }

                if ($model->is_approved != (int) $request->input('is_approved')) {
                    $model->is_approved = $request->input('is_approved');
                }

                $model->save();

                \DB::commit();
                $message       = 'Updated successfully!';
                $success       = true;
                $resCode       = 200;
                $audit_message = 'STATUS_UPDATE_GEMS_SUCCESS';
                $audit_on      = $model->name;

                $title        = 'GEMS Approved';
                $push_message = 'Your gems has been approved : ' . $model->name;
                if ($request->input('status') == 1) {
                    $receiver_device_token = User::where('id', $model->created_by)->select('device_token')->first();
                    if ($receiver_device_token->device_token != '') {
                        $this->fcm_send($receiver_device_token->device_token, $push_message, $title);
                    }
                }

            } else {
                $message       = 'record not exists!';
                $success       = true;
                $resCode       = 400;
                $audit_message = 'STATUS_UPDATE_GEMS_FAIL';
                $audit_on      = '';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $audit_message = 'STATUS_UPDATE_GEMS_FAIL';
            $audit_on      = '';
        }

        $appmessage = Config::get('messages.audit.status_update_gem');
        $platform   = $request->input('platform', 'Web');
        $this->insertAuditTrail('GEMS', $audit_message, $appmessage, $platform, $audit_on);
        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    /**
     * Share gems to separate user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function shareGemsToUser(Request $request)
    {
        $gems_id  = $request->gems_id;
        $user_ids = $request->user_id;
        $user_id  = Auth::user()->id;
        \DB::beginTransaction();
        try {
            if (is_array($user_ids)) {
                foreach ($user_ids as $key => $user_id) {
                    $model           = new ShareGems;
                    $model->user_id  = $user_id;
                    $model->gems_id  = $gems_id;
                    $model->share_by = $user_id;
                    $model->save();
                }
            } else {
                $model           = new ShareGems;
                $model->user_id  = $user_ids;
                $model->gems_id  = $gems_id;
                $model->share_by = $user_id;
                $model->save();

            }

            \DB::commit();
            $message       = 'Gems share successfully.';
            $success       = true;
            $resCode       = 200;
            $audit_message = 'SHARE_GEMS_USER_SUCCESS';
            $audit_on      = $this->getGemName($gems_id);
        } catch (\Exception $e) {
            //return error message
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'SHARE_GEMS_USER_FAIL';
            $audit_on      = '';
        }

        $appmessage = Config::get('messages.audit.share_gem');
        $platform   = $request->input('platform', 'Web');
        $this->insertAuditTrail('SHARE', $audit_message, $appmessage, $platform, $audit_on);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    /**
     * Share gems to a group.
     *
     * @param  $group_id, $gems_id
     * @return \Illuminate\Http\Response
     */
    public function shareGemsInGroup(Request $request)
    {
        $gems_id  = $request->gems_id;
        $group_id = $request->group_id;
        $user_id  = Auth::user()->id;
        \DB::beginTransaction();
        try {
            $groupMember = GroupMember::where('group_id', $group_id)->get();
            foreach ($groupMember as $member) {
                $model           = new ShareGems;
                $model->user_id  = $member->user_id;
                $model->gems_id  = $gems_id;
                $model->share_by = $user_id;
                $model->save();
            }
            $model           = new ShareGemsGroup;
            $model->group_id = $group_id;
            $model->gems_id  = $gems_id;
            $model->share_by = $user_id;
            $model->save();
            $this->addGemFeed($gems_id, 'Share');

            \DB::commit();
            $message       = 'Gems share successfully.';
            $success       = true;
            $resCode       = 200;
            $audit_message = 'SHARE_GEMS_GROUP_SUCCESS';
            $audit_on      = $this->getGemName($gems_id);
        } catch (\Exception $e) {
            //return error message
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'SHARE_GEMS_GROUP_FAIL';
            $audit_on      = '';
        }
        $appmessage = Config::get('messages.audit.share_gem');
        $platform   = $request->input('platform', 'Web');
        $this->insertAuditTrail('SHARE', $audit_message, $appmessage, $platform, $audit_on);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    /**
     * Gems share by a user .
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function shareGemByUserList(Request $request, $user_id)
    {
        $logged_user_id = Auth::user()->id;
        $pageSize       = $request->input('pageSize') ? $request->input('pageSize') : '20';
        \DB::beginTransaction();
        try {
            $total_like_gem = ShareGemsGroup::with(['gems', 'gemsImages', 'gemsCategoryDetail', 'gemsCreatedBy', 'user_like_status' => function ($q) use ($logged_user_id) {
                $q->where('user_id', $logged_user_id);
                $q->select('gems_id', 'action');
            }])
                ->withCount('total_like')
                ->where('share_by', $user_id);
            $total_like_gem = $total_like_gem->paginate($pageSize);

            \DB::commit();
            $message       = $total_like_gem;
            $success       = true;
            $resCode       = 200;
            $audit_message = 'SHARE_GEMS_USER_LIST_SUCCESS';

        } catch (\Exception $e) {
            //return error message
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'SHARE_GEMS_USER_LIST_FAIL';
        }
        $appmessage = Config::get('messages.audit.share_gem_list');
        $platform   = $request->input('platform', 'Web');
        $this->insertAuditTrail('SHARE_LIST', $audit_message, $appmessage, $platform);
        return response()->json([
            'gems_list' => $message,
            'success'   => $success,
        ], $resCode);

    }

    /**
     * Shared gem list by a user's friends
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function shareGemsList(Request $request, $user_id)
    {

        //$user_id = Auth::user()->id;

        $models = Gems::with([
            'categoryDetail', 'createdBy', 'createdByUserDetail', 'images', 'sender_friend_status' => function ($query) use ($user_id) {
                $query->where('user_friends.receiver', $user_id);
                $query->where('user_friends.status', 1);
            }, 'receiver_friend_status' => function ($query) use ($user_id) {
                $query->where('user_friends.sender', $user_id);
                $query->where('user_friends.status', 1);
            }])
            ->join('share_gems', 'gems.id', '=', 'share_gems.gems_id')
            ->where('share_gems.user_id', '=', $user_id)
            ->orderBy('gems.id', 'DESC');

        $results = $models->get();

        return response()->json($results);
    }

    /**
     * Shared gem list in group by a user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function shareGemsGroupList(Request $request, $user_id)
    {

        //$user_id = Auth::user()->id;

        $models = Gems::with([
            'categoryDetail', 'createdBy', 'createdByUserDetail', 'images'])
            ->join('share_gems_groups', 'gems.id', '=', 'share_gems_groups.gems_id')
            ->where('share_gems_groups.share_by', '=', $user_id)
            ->orderBy('gems.id', 'DESC');

        $results = $models->get();

        return response()->json([
            'result'  => $results,
            'totla'   => $models->count(),
            'success' => true,
        ], 200);
        //return response()->json($results);
    }

    /**
     * Gems Like Unlike
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function gemsLikeUnlike(Request $request)
    {

        $gems_id = $request->gems_id;
        $action  = $request->action;
        $user_id = Auth::user()->id;
        \DB::beginTransaction();
        try {
            $models = GemsLikeunlike::where('user_id', $user_id)->where('gems_id', $gems_id)->first();
            if (!$models) {
                $models = new GemsLikeunlike;
            }

            $models->gems_id = $gems_id;
            $models->user_id = $user_id;
            $models->action  = $action;
            $models->save();

            if ($action == 1) {
                $gems_info             = Gems::where('id', $gems_id)->first();
                $receiver_device_token = User::where('id', $gems_info->created_by)->select('device_token')->first();
                $sender_user_info      = UserDetail::where('user_id', $user_id)->select('first_name', 'last_name')->first();
                if ($receiver_device_token->device_token != '') {
                    $title        = "Your GEM is getting popular";
                    $push_message = $sender_user_info->first_name . " " . $sender_user_info->last_name . ' liked your gem: ' . $gems_info->name;
                    $this->fcm_send($receiver_device_token->device_token, $push_message, $title);
                }
                $this->addGemFeed($gems_id, 'Like');
                \DB::commit();
                $message       = "Record updated successfully";
                $success       = true;
                $resCode       = 200;
                $audit_message = 'GEMS_LIKE_SUCCESS';
                $audit_on      = $this->getGemName($gems_id);
                $data          = $models;
                $appmessage    = Config::get('messages.audit.like_gem');
            } else if ($action == 2) {
                \DB::commit();
                $message       = "Record updated successfully";
                $success       = true;
                $resCode       = 200;
                $audit_message = 'GEMS_UNLIKE_SUCCESS';
                $audit_on      = $this->getGemName($gems_id);
                $data          = $models;
                $appmessage    = Config::get('messages.audit.unlike_gem');
            } else {
                \DB::commit();
                $message       = "Record updated fail";
                $success       = true;
                $resCode       = 400;
                $audit_message = 'GEMS_LIKE_FAIL';
                $audit_on      = '';
                $data          = $models;
                $appmessage    = Config::get('messages.audit.like_gem');
            }
        } catch (\Exception $e) {
            //return error message
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'GEMS_LIKE_FAIL';
            $audit_on      = '';
            $data          = '';
            $appmessage    = Config::get('messages.audit.like_gem');
        }

        $this->insertAuditTrail('LIKE', $audit_message, $appmessage, '', $audit_on);
        return response()->json([
            'message' => $message,
            'success' => $success,
            'data'    => $data,
        ], $resCode);
    }

    public function gemsFavorite(Request $request)
    {

        $gems_id = $request->gems_id;
        $user_id = Auth::user()->id;
        \DB::beginTransaction();
        try {
            $models = GemsFavorite::where('user_id', $user_id)->where('gems_id', $gems_id)->first();
            if ($models) {
                $models->delete();
            }
            $models          = new GemsFavorite;
            $models->gems_id = $gems_id;
            $models->user_id = $user_id;
            $models->save();

            \DB::commit();
            $message       = "Record updated successfully";
            $success       = true;
            $resCode       = 200;
            $audit_message = 'GEMS_FAVOURITE_SUCCESS';
            $audit_on      = $gems_id;

        } catch (\Exception $e) {
            //return error message
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'GEMS_FAVOURITE_FAIL';
            $audit_on      = '';
        }

        $appmessage = Config::get('messages.audit.favourite_gem');
        $platform   = $request->input('platform', 'Web');
        $this->insertAuditTrail('FAVOURITE', $audit_message, $appmessage, $platform, $audit_on);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);

    }

    public function createGemLabel(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'label_name' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()->all(),
                'success' => false,
            ], 400);
        }

        $user_id    = Auth::user()->id;
        $label_name = $request->label_name;
        \DB::beginTransaction();
        try {
            $GemsLabel             = new GemsLabel;
            $GemsLabel->label_name = $label_name;
            $GemsLabel->user_id    = $user_id;
            $GemsLabel->save();

            \DB::commit();
            $message       = $GemsLabel;
            $success       = true;
            $resCode       = 200;
            $audit_message = 'CREATE_GEMS_LABEL_SUCCESS';
            $audit_on      = $label_name;

        } catch (\Exception $e) {
            //return error message
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'CREATE_GEMS_LABEL_FAIL';
            $audit_on      = '';
        }

        $appmessage = Config::get('messages.audit.create_gem_label');
        $platform   = $request->input('platform', 'Web');
        $this->insertAuditTrail('GEMS_LABEL', $audit_message, $appmessage, $platform, $audit_on);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    public function updateGemLabel(Request $request, $id)
    {

        $validator = Validator::make($request->all(),
            [
                'label_name' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()->all(),
                'success' => false,
            ], 400);
        }

        $user_id    = Auth::user()->id;
        $label_name = $request->label_name;
        \DB::beginTransaction();
        try {
            $GemsLabel             = GemsLabel::find($id);
            $GemsLabel->label_name = $label_name;
            $GemsLabel->save();

            \DB::commit();
            $message       = $GemsLabel;
            $success       = true;
            $resCode       = 200;
            $audit_message = 'UPDATE_GEMS_LABEL_SUCCESS';
            $audit_on      = $label_name;
        } catch (\Exception $e) {
            //return error message
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'UPDATE_GEMS_LABEL_FAIL';
            $audit_on      = '';
        }

        $appmessage = Config::get('messages.audit.update_gem_label');
        $platform   = $request->input('platform', 'Web');
        $this->insertAuditTrail('GEMS_LABEL', $audit_message, $appmessage, $platform, $audit_on);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    public function gemLabelList(Request $request)
    {
        $user_id = Auth::user()->id;
        \DB::beginTransaction();
        try {
            $data = GemsLabel::withCount(['savedGems' => function ($query) use ($user_id) {
                $query->where('user_id', $user_id);
            }])->where('user_id', $user_id)->get();
            \DB::commit();
            $message       = $data;
            $success       = true;
            $resCode       = 200;
            $audit_message = 'VIEW_GEM_LABEL_LIST_SUCCESS';
        } catch (\Exception $e) {
            //return error message
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'VIEW_GEM_LABEL_LIST_FAIL';

        }

        $appmessage = Config::get('messages.audit.view_gem_label');
        $platform   = $request->input('platform', 'Web');
        $this->insertAuditTrail('GEMS_LABEL', $audit_message, $appmessage, $platform);
        return response()->json([
            'data'    => $message,
            'success' => $success,
        ], $resCode);

    }

    public function deleteGemLabel($label_id)
    {
        \DB::beginTransaction();
        try {
            $models = GemsLabel::find($label_id);
            if ($models) {
                $models->delete();
            }
            $data = GemsLabel::where('user_id', $models->user_id)->first();

            \DB::commit();
            $message       = $data;
            $success       = true;
            $resCode       = 200;
            $audit_message = 'GEM_LABEL_DELETE_SUCCESS';
            $audit_on      = $models->label_name;
        } catch (\Exception $e) {
            //return error message
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'GEM_LABEL_DELETE_FAIL';
            $audit_on      = '';
        }

        $appmessage = Config::get('messages.audit.delete_gem_label');
        $this->insertAuditTrail('GEMS_LABEL', $audit_message, $appmessage, '', $audit_on);
        return response()->json([
            'data'    => $message,
            'success' => $success,
        ], $resCode);
    }

    public function addGemsSaveList(Request $request)
    {
        $gems_id   = $request->gems_id;
        $user_id   = Auth::user()->id;
        $label_ids = $request->label_id;
        \DB::beginTransaction();
        try {
            if (count($label_ids) > 0) {
                GemsSaveList::where('gems_id', $gems_id)->where('user_id', $user_id)->delete();
                foreach ($label_ids as $key => $id) {
                    $GemsLabel           = new GemsSaveList;
                    $GemsLabel->gems_id  = $gems_id;
                    $GemsLabel->label_id = $id;
                    $GemsLabel->user_id  = $user_id;
                    $GemsLabel->save();
                }
            }
            $gems_info             = Gems::find($gems_id);
            $receiver_id           = $gems_info->created_by;
            $gems_name             = $gems_info->name;
            $receiver_device_token = User::where('id', $receiver_id)->select('device_token')->first();
            $sender_user_info      = UserDetail::where('user_id', $user_id)->select('first_name', 'last_name')->first();
            if ($receiver_device_token->device_token != '') {
                $title        = "GEM Saved!";
                $push_message = $sender_user_info->first_name . " " . $sender_user_info->last_name . ' has saved your GEM: ' . $gems_name . " in his list";
                $this->fcm_send($receiver_device_token->device_token, $push_message, $title);
            }

            \DB::commit();
            $message       = 'Gem Saved successfully';
            $success       = true;
            $resCode       = 200;
            $audit_message = 'ADD_GEM_SAVE_SUCCESS';
            $audit_on      = '';

            return response()->json([
                'message' => 'Gem Saved successfully',
                'success' => true,
            ], 200);
        } catch (\Exception $e) {
            //return error message
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'ADD_GEM_SAVE_FAIL';
            $audit_on      = '';
        }

        $appmessage = Config::get('messages.audit.gem_save');
        $platform   = $request->input('platform', 'Web');

        $this->insertAuditTrail('GEM_SAVE', $audit_message, $appmessage, $platform, $audit_on);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    public function getGemsSaveList($label_id)
    {
        $user_id = Auth::user()->id;
        \DB::beginTransaction();
        try {
            $data = GemsSaveList::with(['gemsDetails', 'gemsCategoryDetail', 'gemsImages', 'gemsCreatedBy', 'user_like_status' => function ($q) use ($user_id) {
                $q->where('user_id', $user_id);
                $q->select('gems_id', 'action');
            }])
                ->withCount('gemsLike')
                ->where('user_id', $user_id)->where('label_id', $label_id)->get();

            \DB::commit();
            $message       = $data;
            $success       = true;
            $resCode       = 200;
            $audit_message = 'VIEW_GEM_SAVE_SUCCESS';
        } catch (\Exception $e) {
            //return error message
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'VIEW_GEM_SAVE_FAIL';
        }

        $appmessage = Config::get('messages.audit.gem_save_list');

        $this->insertAuditTrail('GEMS_SAVE', $audit_message, $appmessage);
        return response()->json([
            'data'    => $message,
            'success' => $success,
        ], $resCode);
    }

    public function deleteGemsSaveList($id)
    {
        $user_id = Auth::user()->id;
        \DB::beginTransaction();
        try {
            $GemsSave = GemsSaveList::find($id);
            if ($GemsSave) {
                $GemsSave->delete();
            }
            $data = GemsSaveList::where('user_id', $user_id)->where('label_id', $GemsSave->label_id)->get();
            return response()->json([
                'data'    => $data,
                'success' => true,
            ], 200);

            \DB::commit();
            $message       = $data;
            $success       = true;
            $resCode       = 200;
            $audit_message = 'DELETE_GEM_SAVE_SUCCESS';
        } catch (\Exception $e) {
            //return error message
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'DELETE_GEM_SAVE_FAIL';
        }

        $appmessage = Config::get('messages.audit.gem_save_delete');

        $this->insertAuditTrail('GEMS_SAVE', $audit_message, $appmessage);
        return response()->json([
            'data'    => $message,
            'success' => $success,
        ], $resCode);
    }

    public function gemsImage(Request $request)
    {
        $gems_id = $request->gems_id;
        if ($request->hasFile('image')) {

            $file       = $request->file('image');
            $fileCount  = $this->moveImage($file, $gems_id);
            $image      = GemsImage::where('gems_id', $gems_id)->get();
            $appmessage = Config::get('messages.audit.gem_image_add');
            $platform   = 'Web';
            $this->insertAuditTrail('GEMS', 'ADD_GEMS_IMAGE', $appmessage, $platform);
            return response()->json([
                'message' => $image,
                'success' => true,
            ], 200);
        }
    }

    public function gemsImageList($gems_id)
    {

        $image = GemsImage::where('gems_id', $gems_id)->get();

        return response()->json([
            'message' => $image,
            'success' => true,
        ], 200);
    }

    public function deleteGemsImage($id)
    {
        \DB::beginTransaction();
        try {

            $image    = GemsImage::find($id);
            $gems_id  = $image->gems_id;
            $filename = "upload/gems/" . $image['image'];

            if (File::exists($filename)) {
                File::delete($filename);
            }

            $image->delete();
            $images = GemsImage::where('gems_id', $gems_id)->get();

            \DB::commit();
            $message       = $data;
            $success       = true;
            $resCode       = 200;
            $images        = $images;
            $audit_message = 'DELETE_GEM_IMAGE_SUCCESS';

        } catch (\Exception $e) {
            //return error message
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'DELETE_GEM_IMAGE_FAIL';
            $images        = '';
        }

        $appmessage = Config::get('messages.audit.delete_gem_image');
        $this->insertAuditTrail('GEMS', $audit_message, $appmessage);
        return response()->json([
            'message' => $message,
            'images'  => $images,
            'success' => $success,
        ], $resCode);
    }

    public function addGemComments(Request $request)
    {
        $gems_id  = $request->gems_id;
        $title    = $request->title;
        $comments = $request->comment;
        $user_id  = Auth::user()->id;

        $validator = Validator::make($request->all(),
            [
                'gems_id' => 'required',
                //'comment' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()->all(),
                'success' => false,
            ], 400);
        }
        \DB::beginTransaction();
        try {
            $comment          = new GemsComment;
            $comment->gems_id = $gems_id;
            $comment->user_id = $user_id;
            $comment->title   = $title;
            $comment->comment = $comments;
            $comment->save();

            $file      = $request->file('image');
            $fileCount = count((array) $file);
            if ($fileCount > 0) {
                $this->commentImage($file, $comment->id, $gems_id, $user_id);
            }
            $gems_info             = Gems::where('id', $gems_id)->first();
            $receiver_device_token = User::where('id', $gems_info->created_by)->select('device_token')->first();
            $sender_user_info      = UserDetail::where('user_id', $user_id)->select('first_name', 'last_name')->first();
            if ($receiver_device_token->device_token != '') {
                $title        = "New comment posted";
                $push_message = $sender_user_info->first_name . " " . $sender_user_info->last_name . ' just commented on your gem: ' . $gems_info->name;
                $this->fcm_send($receiver_device_token->device_token, $push_message, $title);
            }

            $this->addGemFeed($gems_id, 'Comment');
            \DB::commit();
            $message       = 'Comment has been added successfully';
            $success       = true;
            $resCode       = 200;
            $comment_id    = $comment->id;
            $audit_message = 'GEMS_COMMENT_SUCCESS';
            $audit_on      = $this->getGemName($gems_id);

        } catch (\Exception $e) {
            //return error message
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'GEMS_COMMENT_FAIL';
            $comment_id    = '';
            $audit_on      = '';
        }
        $appmessage = Config::get('messages.audit.gem_comment');
        $platform   = $request->input('platform', 'Web');
        $this->insertAuditTrail('COMMENT', $audit_message, $appmessage, $platform, $audit_on);
        return response()->json([
            'message'    => $message,
            'comment_id' => $comment_id,
            'success'    => $success,
        ], $resCode);
    }

    public function gemCommentList($gems_id, Request $request)
    {
        $user_id  = Auth::user()->id;
        $pageSize = $request->input('pageSize') ? $request->input('pageSize') : '10';
        $comments = GemsComment::with(['images', 'users', 'sender_friend_status' => function ($query) use ($user_id) {
            $query->where('user_friends.receiver', $user_id);
        }, 'receiver_friend_status' => function ($query) use ($user_id) {
            $query->where('user_friends.sender', $user_id);
        }])->where('gems_id', $gems_id);
        $comments = $comments->orderBy('id', 'Desc');
        $comments = $comments->paginate($pageSize);

        return response()->json([
            'comments' => $comments,
            'success'  => true,
        ], 200);
    }

    public function gemsCommentImage(Request $request)
    {
        $gems_id    = $request->gems_id;
        $comment_id = $request->comment_id;
        $user_id    = Auth::user()->id;
        if ($request->hasFile('image')) {

            $file      = $request->file('image');
            $fileCount = $this->commentImage($file, $comment_id, $gems_id, $user_id);
            $image     = GemsCommentImage::where('comment_id', $comment_id)->get();

            $appmessage = Config::get('messages.audit.gem_comment_image');
            $platform   = $request->input('platform', 'Web');
            $this->insertAuditTrail('COMMENT', $audit_message, $appmessage, $platform);
            return response()->json([
                'message' => $image,
                'success' => true,
            ], 200);
        }
    }

    public function commentImage($file, $comment_id, $gems_id, $user_id)
    {

        $fileCount = count((array) $file);

        try {

            $notMoveFileArr = array();

            for ($i = 0; $i < $fileCount; $i++) {
                $originalFileName = $file[$i]->getClientOriginalName();
                $filteredOrgName  = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $originalFileName);
                $extension        = strtolower($file[$i]->getClientOriginalExtension());
                $fileName         = date('YmdHms') . rand(100, 999) . '-' . $filteredOrgName;

                $fileSize = $file[$i]->getSize();

                $fileSize = number_format($fileSize / 1048576, 2);

                $destinationPath = "upload/gems/comment";
                $fullFilePath    = $destinationPath . $fileName;
                if ($fileSize > 10) {
                    return response()->json(['error' => 'Filie size must be less than 10 MB'], 401);
                }
                if ($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png') {

                    if ($file[$i]->move($destinationPath, $fileName)) {

                        /* shell_exec ('gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile='.$destinationPath.'test.pdf '.$fullFilePath.''); */

                        if ($fileSize > 6) {

                            $oldFullFilePath = $fullFilePath;

                            $fileName     = date('YmdHmsu') . '-converted-' . $filteredOrgName;
                            $fullFilePath = $destinationPath . $fileName;

                            if ($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'tiff') {

                                shell_exec('convert -quality 50% ' . $oldFullFilePath . '  ' . $fullFilePath . '');

                            }

                            if (File::exists($oldFullFilePath)) {
                                File::delete($oldFullFilePath);
                            }
                        }

                    } else {
                        $notMoveFileArr[] = $originalFileName;
                    }
                } else {
                    return response()->json(['error' => 'Only jpeg,jpg,png, are allowed'], 401);
                }

                try {
                    //echo $fileName."===".$user_id."==".$gems_id."=".$comment_id."--";
                    $modelImage             = new GemsCommentImage;
                    $modelImage->image      = $fileName;
                    $modelImage->comment_id = $comment_id;
                    $modelImage->gems_id    = $gems_id;
                    $modelImage->user_id    = $user_id;
                    $modelImage->save();

                } catch (\Exception $e) {
                    echo $e->getMessage();
                    return response()->json(['error' => $e->getMessage()], 409);
                }

            }

            return $notMoveFileArr;
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 409);
        }
    }

    public function gemsFeed(Request $request)
    {
        $search  = $request->search;
        $user_id = Auth::user()->id;
        $perPage = 10;

        $sql = "select user_id
        from (
          select sender as friend, status
            from user_friends
           where receiver = $user_id
          union
          select receiver as friend, status
            from user_friends
           where sender = $user_id
          ) as friends
        inner join user_details
        on user_details.user_id = friends.friend where friends.status = 1";

        $friend_info = \DB::select(\DB::raw($sql));
        $friendArr   = array();
        if (count($friend_info) > 0) {
            foreach ($friend_info as $key => $friend) {
                $friendArr[$key] = $friend->user_id;
            }
        }
        DB::select(DB::raw("SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))"));
        $data = GemsFeed::with(['gemsDetails', 'gemsCategoryDetail', 'gemsImages', 'gemsCreatedBy', 'user_like_status' => function ($q) use ($user_id) {
            $q->where('user_id', $user_id);
            $q->select('gems_id', 'action');
        }])
            ->withCount(['gemsLike'])
            ->whereHas('gemsDetails', function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%');
            })
            ->groupBy('gems_id')
            ->whereIn('user_id', $friendArr)
            ->orderBy('gems_feeds.id', 'DESC');
        $data = $data->paginate($perPage);

        return response()->json([
            'message' => $data,
            'success' => true,
        ], 200);
    }

    /**
     * Gems View count
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function gemsView(Request $request)
    {

        $gems_id = $request->gems_id;
        $type    = $request->type;
        $user_id = Auth::user()->id;
        try {
            $models = GemsView::where('user_id', $user_id)->where('gems_id', $gems_id)->where('type', $type)->first();
            if ($models) {
                $models->per_user_count = $models->per_user_count + 1;
            } else {
                $models                 = new GemsView;
                $models->per_user_count = 1;
            }
            $models->gems_id = $gems_id;
            $models->user_id = $user_id;
            $models->type    = $type;
            $models->save();

            $message = "Record updated successfully";
            $success = true;
            $data    = $models;
        } catch (\Exception $e) {
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
            $data    = '';
        }
        return response()->json([
            'message' => $message,
            'success' => $success,
            'data'    => $data,
        ], 200);
    }

    /**
     * Assign gem
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function assignGem(Request $request)
    {
        $gem_id         = $request->gem_id;
        $assign_user_id = $request->assign_user_id;
        $user_id        = Auth::user()->id;
        try {
            $gem                 = Gems::find($gem_id);
            $gem->previous_owner = $user_id;
            $gem->created_by     = $assign_user_id;
            $gem->save();

            $message       = 'Assigned successfully';
            $success       = true;
            $responseCode  = '200';
            $audit_message = 'ASSIGNED_GEM';
            $audit_on      = $this->getGemName($gem_id);

            $appmessage = Config::get('messages.audit.assign_gem');
            //Return message
            $this->insertAuditTrail('GEMS', $audit_message, $appmessage, '', $audit_on);
            $gems_name = $this->getGemName($gem_id);
            $title     = "GEM Assign";

            $push_message = $gems_name . ": Gem has been assign to you";

            $receiver_device_token = User::where('id', $assign_user_id)->select('device_token')->first();
            if ($receiver_device_token->device_token != '') {
                $this->fcm_send($receiver_device_token->device_token, $push_message, $title);
            }

        } catch (\Exception $e) {
            $allErrors = $e->getMessage();

            $message      = $allErrors . ' ' . 'Please try again!';
            $success      = false;
            $responseCode = '400';
        }
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $responseCode);
    }

    /**
     * Assign gem
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function assigngemList(Request $request, $user_id)
    {
        try {

            $message      = Gems::where('previous_owner', $user_id)->get();
            $success      = true;
            $responseCode = '200';
        } catch (\Exception $e) {
            $allErrors = $e->getMessage();

            $message      = $allErrors . ' ' . 'Please try again!';
            $success      = false;
            $responseCode = '400';
        }
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $responseCode);
    }

    public function addNotificationSettings(Request $request)
    {
        $user_id      = Auth::user()->id;
        $notification = $request->notification;
        \DB::beginTransaction();
        try {
            $model               = new NotificationSetting;
            $model->user_id      = $user_id;
            $model->notification = $notification;
            $model->save();

            \DB::commit();
            $message = "Record updated successfully";
            $success = true;
            $resCode = 200;
        } catch (\Exception $e) {
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
            $resCode = 400;
        }
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);

    }

    public function removeNotificationSettings(Request $request, $id)
    {
        $user_id = Auth::user()->id;
        \DB::beginTransaction();
        try {
            $model = NotificationSetting::find($id);
            if (!empty($model)) {
                $model->delete();

                \DB::commit();
                $message = "Record updated successfully";
                $success = true;
                $resCode = 200;
            } else {
                \DB::commit();
                $message = "Record not exists";
                $success = false;
                $resCode = 400;
            }

        } catch (\Exception $e) {
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
            $resCode = 400;
        }
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);

    }

    public function moveImage($file, $gems_id)
    {

        $fileCount = count((array) $file);

        try {

            $notMoveFileArr = array();

            for ($i = 0; $i < $fileCount; $i++) {
                $originalFileName = $file[$i]->getClientOriginalName();
                $filteredOrgName  = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $originalFileName);
                $extension        = strtolower($file[$i]->getClientOriginalExtension());
                $fileName         = date('YmdHms') . rand(100, 999) . '-' . $filteredOrgName;

                $fileSize = $file[$i]->getSize();

                $fileSize = number_format($fileSize / 1048576, 2);

                $destinationPath = "upload/gems/";
                $fullFilePath    = $destinationPath . $fileName;
                if ($fileSize > 10) {
                    return response()->json(['error' => 'Filie size must be less than 10 MB'], 401);
                }
                if ($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png') {

                    if ($file[$i]->move($destinationPath, $fileName)) {

                        /* shell_exec ('gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile='.$destinationPath.'test.pdf '.$fullFilePath.''); */

                        if ($fileSize > 6) {

                            $oldFullFilePath = $fullFilePath;

                            $fileName     = date('YmdHmsu') . '-converted-' . $filteredOrgName;
                            $fullFilePath = $destinationPath . $fileName;

                            if ($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'tiff') {

                                shell_exec('convert -quality 50% ' . $oldFullFilePath . '  ' . $fullFilePath . '');

                            }

                            if (File::exists($oldFullFilePath)) {
                                File::delete($oldFullFilePath);
                            }
                        }

                    } else {
                        $notMoveFileArr[] = $originalFileName;
                    }
                } else {
                    return response()->json(['error' => 'Only jpeg,jpg,png, are allowed'], 401);
                }

                try {
                    $gemsImage          = new GemsImage;
                    $gemsImage->image   = $fileName;
                    $gemsImage->gems_id = $gems_id;
                    $gemsImage->save();
                } catch (\Exception $e) {
                    return response()->json(['error' => $e->getMessage()], 409);
                }

            }

            return $notMoveFileArr;
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 409);
        }
    }

    public function fcm_send($token, $msg, $title)
    {

        $type      = 1;
        $sub_title = "subtitle";
        $arr       = array(
            'to'           => $token,
            'notification' => array(
                'body'               => $msg,
                'title'              => $title,
                'sound'              => 'default',
                'android_channel_id' => '500',
            ),
            'data'         => array(
                //'body' => $msg,
                'title'     => $title,
                'type'      => $type,
                'sub_title' => $sub_title,
            ),
        );
        $data = json_encode($arr);
        //FCM API end-point
        $url = 'https://fcm.googleapis.com/fcm/send';
        //api_key in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
        //$server_key = 'AAAAO9slJlU:APA91bFId67Nq23ecBNAASvLLkLWGuo-9blx-GHwsWIOKpUfBfeoySztSJhKSzaJJ_B-bbeg-rM5m-4M-fJ6nNXIDYF3wNbK7d3HzAyot20sVimiMHIxvwtY4NPZnRrUOzOPIczCLUmI';

        $server_key = Config::get('constants.fcm.server_key');
        //header with content_type api key
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key=' . $server_key,
        );
        //CURL request to route notification to FCM connection server (provided by Google)
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        if ($result === false) {
            die('Oops! FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
    }

    public function getGemName($id)
    {
        $gem = Gems::find($id);
        if (!empty($gem)) {
            return $gem->name;
        } else {
            return '';
        }
    }

    public function addGemFeed($gems_id, $action)
    {
        $gemFeed          = new GemsFeed;
        $gemFeed->gems_id = $gems_id;
        $gemFeed->action  = $action;
        $gemFeed->user_id = Auth::user()->id;
        $gemFeed->save();
        return true;
    }

}
