<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * News list for frontend.
     *
     * @param  Request  $request
     * @return Response
     */

    public function newsList()
    {
        $model = News::where('status', '=', '1')->get();
        return response()->json($model);
    }

    /**
     * News list for admin.
     *
     * @param  Request  $request
     * @return Response
     */

    public function newsListAdmin(Request $request)
    {
        $pageSize = $request->input('pageSize') ? $request->input('pageSize') : '10';
        $model   = News::paginate($pageSize);
        return response()->json($model);
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'title'             => 'required',
            'short_description' => 'required',
            'description'       => 'required',
        ]);

        \DB::beginTransaction();
        try {

            $model                    = new News;
            $model->title             = $request->title;
            $model->slug              = preg_replace("![^a-z0-9]+!i", "-", strtolower($model->title));
            $model->short_description = $request->short_description;
            $model->description       = $request->description;
            $model->status            = $request->status;

            // $model->title = "1111";
            // $model->slug = "111";
            // $model->short_description = "adsd";
            // $model->description       = "hkkk";
            // $model->status = "1";
            $model->save();


            if ($request->hasFile('image')) {

                $file    = $request->file('image');
                $news_id = $model->id;

                $fileCount = $this->moveImage($file, $news_id);
            }

            \DB::commit();
            $message = 'News Created Successfully.';
            $success = true;
            $resCode = 200;
            $audit_message = 'CREATE_NEWS_SUCCESS';
            $audit_on      = $model->title;
            
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
            $resCode = 400;
            $audit_message = 'CREATE_NEWS_FAIL';
            $audit_on      = '';
        }
        $appmessage = Config::get('messages.audit.add_news');
        $this->insertAuditTrail('NEWS', $audit_message,$appmessage,'', $audit_on);
        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    public function update(Request $request, $id)
    {
        try {
            $model = News::find($id);
            if ($request->title != '') {
                $model->title = $request->title;
            }

            if ($request->short_description != '') {
                $model->short_description = $request->short_description;
            }

            if ($request->description != '') {
                $model->description = $request->description;
            }
            if ($request->status != '') {
                $model->status = $request->status;
            }

            if ($request->hasFile('image')) {

                $file    = $request->file('image');
                $news_id = $model->id;

                $fileCount = $this->moveImage($file, $news_id);
            }

            $model->save();

            \DB::commit();
            $message = 'News Updated successfully!';
            $success = true;
            $resCode = 200;
            $audit_message = 'UPDATE_NEWS_SUCCESS';
            $audit_on      = $model->title;

        } catch (\Exception $e) {
            
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
            $resCode = 400;
            $audit_message = 'UPDATE_NEWS_FAIL';
            $audit_on      = '';
        }
        $appmessage = Config::get('messages.audit.update_news');
        $this->insertAuditTrail('NEWS', $audit_message,$appmessage,'', $audit_on);
        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }


    protected function moveImage($file, $news_id)
    {
        $fileCount = count((array) $file);
        try {

            $notMoveFileArr   = array();
            $originalFileName = $file->getClientOriginalName();
            $filteredOrgName  = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $originalFileName);
            $extension        = strtolower($file->getClientOriginalExtension());
            $fileName         = date('YmdHmsu') . '-' . $filteredOrgName;
            $fileSize         = $file->getSize();
            $fileSize         = number_format($fileSize / 1048576, 2);
            $destinationPath  = "upload/news/";
            $fullFilePath     = $destinationPath . $fileName;
            if ($file->move($destinationPath, $fileName)) {
                if ($fileSize > 6) {
                    $oldFullFilePath = $fullFilePath;
                    $fileName        = date('YmdHmsu') . '-converted-' . $filteredOrgName;
                    $fullFilePath    = $destinationPath . $fileName;
                    if ($extension == 'pdf') {

                        shell_exec('ps2pdf -dPDFSETTINGS=/default ' . $oldFullFilePath . '  ' . $fullFilePath . '');

                    } else if ($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'tiff') {

                        shell_exec('convert -quality 50% ' . $oldFullFilePath . '  ' . $fullFilePath . '');

                    } /*else{
                    rename( $orgFullFilePath, $fullFilePath);
                    }*/

                    if (File::exists($oldFullFilePath)) {
                        File::delete($oldFullFilePath);
                    }
                }
                $news        = News::find($news_id);
                $news->image = $fileName;
                $news->save();

                //$this->compress($fullFilePath,$destinationPath."compress/".$fileName,60);
            } else {
                $notMoveFileArr[] = $originalFileName;
            }
            return $notMoveFileArr;
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    public function statusUpdate(Request $request, $id)
    {

        //return response()->json($request);
        \DB::beginTransaction();
        try {
            $model = News::find($id);
            if (!empty($model)) {

                if ($model->status != (int) $request->input('status')) {
                    $model->status = $request->input('status');
                }

                if ($model->is_approved != (int) $request->input('is_approved')) {
                    $model->is_approved = $request->input('is_approved');
                }

                $model->save();

                \DB::commit();
                $message       = 'Status Updated successfully!';
                $success       = true;
                $resCode = 201;
                $audit_message = 'STATUS_UPDATE_NEWS_SUCCESS';
                $audit_on      = $model->title;
            } else {
                $message       = 'Record not exists!';
                $success       = false;
                $resCode = 400;
                $audit_message = 'STATUS_UPDATE_NEWS_FAIL';
                $audit_on      = '';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $resCode = 400;
            $audit_message = 'STATUS_UPDATE_GEMS_FAIL';
            $audit_on      = '';
        }
        $appmessage = Config::get('messages.audit.news_status_update');
        $this->insertAuditTrail('NEWS', $audit_message,$appmessage, '', $audit_on);
        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    public function newsDetails($id)
    {
        try {
            $model = News::find($id);
            return response()->json([
                'message' => $model,
                'success' => true,
            ], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    /**
     * Delete record.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        \DB::beginTransaction();
        try {
            $model = News::find($id);
            if (!empty($model)) {
                $model->delete();

                \DB::commit();
                $message       = 'Removed successfully!';
                $success       = true;
                $audit_message = 'DELETE_NEWS_SUCCESS';

            } else {
                $message       = 'Record not exists!';
                $success       = true;
                $audit_message = 'DELETE_NEWS_FAIL';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $alerttype     = false;
            $audit_message = 'DELETE_NEWS_FAIL';
        }
        $appmessage = Config::get('messages.audit.delete_news');
        //Return message
        $this->insertAuditTrail('NEWS', $audit_message,$appmessage, '', $model->title);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 200);
    }

    //
}
