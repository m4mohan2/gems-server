<?php

namespace App\Http\Controllers;

use App\City;
use App\CompanyDetail;
use App\CompanyDocument;
use App\Country;
use App\GemsLabel;
use App\GemsProviderRelation;
use App\Http\Controllers\Controller;
use App\LoginHistory;
use App\Mail\Forgotpassword;
use App\Mail\Register;
use App\Plan;
use App\SocialAccount;
use App\State;
use App\Subscription;
use App\User;
use App\UserDetail;
use App\UserFriend;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Tymon\JWTAuth\JWTAuth;
use Validator;

class AuthController extends Controller
{
    /**
     * @var \Tymon\JWTAuth\JWTAuth
     */
    protected $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    /**
     * Logout JWT
     * @param Request $request
     * @return array
     * @throws \Tymon\JWTAuth\Exceptions\JWTException
     */
    public function logout(Request $request)
    {
        /*$user = $this->jwt->parseToken()->authenticate();

        return response()->json([
        'request' => $request->header(),
        'user' => $user,
        ], 200);*/

        try {
            $this->insertAuditTrail('LOGOUT', 'LOGOUT_SUCCESS',Auth::user()->email);
            $this->jwt->parseToken()->invalidate();
            return response()->json([
                'message' => 'Successfully logged out',
                'success' => true,
            ], 200);
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json([
                'message' => 'token_expired',
                'success' => false,
            ], 500);
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json([
                'message' => 'token_invalid',
                'success' => false,
            ], 500);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json([
                'message' => 'token_absent',
                'error'   => $e->getMessage(),
                'success' => false,
            ], 500);
        }
    }
    /**
     * Renewal process to make JWT reusable after expiry date.
     * @return JsonResponse
     */
    public function refresh()
    {
        try {
            $this->jwt->setToken($this->jwt->getToken());

            if ($this->jwt->parseToken()->invalidate()) {
                return response()->json([
                    'token' => $this->jwt->refresh(),
                ]);
            }

            return response()->json([], 403, $this->_lang['token_incorrect']);

        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'error'   => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function register(Request $request)
    {
        //validate incoming request
        $this->validate($request, [
            'email'      => 'required|email|unique:users',
            'password'   => 'required|confirmed',
            'first_name' => 'required',
            'last_name'  => 'required',
        ]);

        try {

            $model = new User;

            $model->email         = $request->input('email');
            $plainPassword        = $request->input('password');
            $model->password      = app('hash')->make($plainPassword);
            $model->vp            = $plainPassword;
            $model->active_status = 1;
            $model->user_type     = 1;
            $model->is_verified   = 0;
            $model->active_token  = rand(100, 999) . time();

            if ($model->save()) {
                $userD = new UserDetail;

                $userD->user_id    = $model->id;
                $userD->first_name = $request->input('first_name');
                $userD->last_name  = $request->input('last_name');

                $userD->save();
                $find_influencer = User::where('is_influencer', '1')->where('active_status', '1')->get();
                if ($find_influencer) {
                    foreach ($find_influencer as $key => $influencer) {
                        $friend_info            = new UserFriend;
                        $friend_info->sender    = $influencer->id;
                        $friend_info->receiver  = $model->id;
                        $friend_info->action_by = $model->id;
                        $friend_info->status    = '1';
                        $friend_info->save();
                    }
                }

                $GemsLabel             = new GemsLabel;
                $GemsLabel->label_name = 'Favourite places';
                $GemsLabel->user_id    = $model->id;
                $GemsLabel->save();
            }
            $this->insertAuditTrail('REGISTER', 'REGISTER_SUCCESS', $request->input('email'));
            //return successful response

            $name     = $request->input('first_name');
            $email    = $request->input('email');
            $password = $plainPassword;
            $link     = env('FRONT_APP_URL') . 'verify-account/' . $model->active_token;
            Mail::to($email)->send(new Register($name, $email, $password, $link));
            return response()->json(['success' => true, 'message' => 'Registered Successfully!!'], 201);

        } catch (\Exception $e) {
            //return error message
            return response()->json(['success' => false, 'Error' => $e->getMessage(), 'message' => 'Registration Failed!'], 409);
        }

    }

    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function registerProvider(Request $request)
    {
        //validate incoming request
        $validator = Validator::make($request->all(),
            [
                'email'       => 'required|email|unique:users',
                'first_name'  => 'required',
                'last_name'   => 'required',
                'company_doc' => 'required',
            ],
            [
                'email.unique' => 'This Email is already taken. Please try a different one',
            ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()->all(),
                'success' => false,
            ], 400);
        }

        \DB::beginTransaction();

        // Set API key
        \Stripe\Stripe::setApiKey(Config::get('constants.stripe.secret'));

        try {

            $model = new User;

            $model->email         = $request->input('email');
            $plainPassword        = Str::random(8);
            $model->password      = app('hash')->make($plainPassword);
            $model->vp            = $plainPassword;
            $model->active_status = 1;
            $model->user_type     = 2;
            $model->is_verified   = 0;
            $model->active_token  = rand(100, 999) . time();

            if ($model->save()) {

                $userID = $model->id;

                // Add personal details
                $userD = new UserDetail;

                $userD->user_id    = $userID;
                $userD->first_name = $request->input('first_name');
                $userD->last_name  = $request->input('last_name');
                $userD->phone      = $request->input('phone');
                $userD->address    = $request->input('address');
                $userD->address2   = $request->input('address2');
                $userD->country    = $request->input('personalCountry');
                $userD->state      = $request->input('personalState');
                $userD->city       = $request->input('personalCity');
                $userD->zip        = $request->input('personalZip');

                $userD->save();

                // Add comapny details
                $companyModel = new CompanyDetail;

                $companyModel->user_id = $userID;

                $companyModel->company_name    = $request->input('company_name');
                $companyModel->company_phone   = $request->input('company_phone');
                $companyModel->company_email   = $request->input('email');
                $companyModel->addr1           = $request->input('addr1');
                $companyModel->addr2           = $request->input('addr2');
                $companyModel->country         = $request->input('companyCountry');
                $companyModel->state           = $request->input('companyState');
                $companyModel->city            = $request->input('companyCity');
                $companyModel->zip             = $request->input('companyZip');
                $companyModel->prefer_purchase = $request->input('prefer_purchase') == true ? 1 : 0;

                $companyModel->save();

                // Add provider to gems
                $relationModel = new GemsProviderRelation;

                $relationModel->user_id = $userID;
                $relationModel->gems_id = $request->input('gems_id');

                $relationModel->save();

            }

            if ($request->hasFile('company_doc')) {

                $file       = $request->file('company_doc');
                $user_id    = $userID;
                $company_id = $companyModel->id;

                $fileCount = $this->moveFile($file, $user_id, $company_id);
            }

            //Stripe
            $plan = Plan::where('status', '=', '1')->first();

            if ($request->input('prefer_purchase') == true && $plan) {

                $api_error = '';
                $ordStatus = 'error';

                $countryObj = Country::find($request->input('companyCountry'));
                $stateObj   = State::find($request->input('companyState'));
                $cityObj    = City::find($request->input('companyCity'));

                $postal_code = $request->input('companyZip');
                $country     = $countryObj ? $countryObj->iso2 : '';
                $state       = $stateObj ? $stateObj->iso2 : '';
                $city        = $cityObj ? $cityObj->name : '';

                $address       = $request->input('addr1') . $request->input('addr2');
                $customer      = $request->input('company_name');
                $billing_email = $request->input('email');

                $currency = $plan->plan_currency;
                // Plan info
                $planID            = $plan->id;
                $planName          = $plan->plan_name;
                $planPrice         = $plan->plan_price; // Three-letter ISO currency code, in lowercase. Must be a supported currency.
                $planInterval      = $plan->plan_peroid; // Specifies billing frequency. Either day, week, month or year.
                $planIntervalCount = $plan->plan_interval_count;

                /*return response()->json([
                'data' => $plan,
                'success' => false,
                ], 409);*/

                $stripe = new \Stripe\StripeClient(Config::get('constants.stripe.secret'));

                $tokenRes = $stripe->tokens->create([
                    'card' => [
                        'number'    => $request->input('card_number'),
                        'exp_month' => $request->input('exp_month'),
                        'exp_year'  => $request->input('exp_year'),
                        'cvc'       => $request->input('cvc'),
                    ],
                ]);

                // Add customer to stripe
                try {
                    $customer = \Stripe\Customer::create([
                        'source'  => $tokenRes['id'],
                        'name'    => $customer,
                        'email'   => $billing_email,
                        'address' => [
                            'line1'       => $address,
                            'postal_code' => $postal_code,
                            'city'        => $city,
                            'state'       => $state,
                            'country'     => $country,
                        ],
                    ]);
                } catch (Exception $e) {
                    $api_error = $e->getMessage();
                }

                if (empty($api_error) && $customer) {

                    // Convert price to cents
                    $priceCents = round($planPrice * 100);

                    // Create a plan
                    try {
                        $plan = \Stripe\Plan::create(array(
                            "product"        => [
                                "name" => $planName,
                            ],
                            "amount"         => $priceCents,
                            "currency"       => $currency,
                            "interval"       => $planInterval,
                            "interval_count" => $planIntervalCount,
                        ));
                    } catch (Exception $e) {
                        $api_error = $e->getMessage();
                    }

                    if (empty($api_error) && $plan) {
                        // Creates a new subscription
                        try {
                            $subscription = \Stripe\Subscription::create(array(
                                "customer" => $customer->id,
                                "trial_period_days" => 90,
                                "items"    => array(
                                    array(
                                        "plan" => $plan->id,
                                    ),
                                ),
                            ));
                        } catch (Exception $e) {
                            $api_error = $e->getMessage();
                        }

                        if (empty($api_error) && $subscription) {

                            /*return response()->json([
                            'data' => $subscription,
                            'success' => false,
                            ], 409);*/

                            // Retrieve subscription data
                            $subsData = $subscription;

                            // Check whether the subscription activation is successful
                            //if ($subsData['status'] == 'active') {
                                // Subscription info
                                $subscriptionModel = new Subscription;

                                $subscriptionModel->user_id                = $userID;
                                $subscriptionModel->stripe_subscription_id = $subsData['id'];
                                $subscriptionModel->stripe_customer_id     = $subsData['customer'];
                                $subscriptionModel->stripe_plan_id         = $subsData['plan']['id'];
                                $subscriptionModel->plan_amount            = ($subsData['plan']['amount'] / 100);
                                $subscriptionModel->plan_amount_currency   = $subsData['plan']['currency'];
                                $subscriptionModel->plan_interval          = $subsData['plan']['interval'];
                                $subscriptionModel->plan_interval_count    = $subsData['plan']['interval_count'];
                                $subscriptionModel->payer_email            = $billing_email;
                                $subscriptionModel->plan_period_start      = date("Y-m-d H:i:s", $subsData['current_period_start']);
                                $subscriptionModel->plan_period_end        = date("Y-m-d H:i:s", $subsData['current_period_end']);
                                $subscriptionModel->status                 = $subsData['status'];
                                $subscriptionModel->dataObj                = $subsData;

                                $subscriptionModel->save();

                                // Update subscription id in the users table
                                if ($subscriptionModel->save()) {
                                    $model->subscription_id = $subscriptionModel->id;
                                    $model->stripe_id = $subscriptionModel->stripe_customer_id;
                                    
                                    $model->save();
                                }

                                $ordStatus = 'success';

                            //} else {
                                //$message = "Subscription activation failed!";
                            //}
                        } else {
                            $message = "Subscription creation failed! " . $api_error;
                        }
                    } else {
                        $message = "Plan creation failed! " . $api_error;
                    }
                } else {
                    $message = "Invalid card details! $api_error";
                }
            }
            //Stripe

            \DB::commit();

            $name  = $request->input('first_name');
            $email = $request->input('email');
            //$company_email = $request->input('company_email');
            $password = $plainPassword;
            $link     = env('FRONT_APP_URL') . 'verify-account/' . $model->active_token;

            if($_SERVER['HTTP_HOST'] == '54.147.235.207'){
                Mail::to($email)->send(new Register($name, $email, $password, $link));
            }

            $message       = 'Registration successfull. A mail has been sent to your given mail id.';
            $success       = true;
            $resCode       = 201;
            $audit_message = 'CREATE_PROVIDER_SUCCESS';

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $resCode       = 409;
            $audit_message = 'CREATE_PROVIDER_FAIL';
        }

        $this->insertAuditTrail('PROVIDER', $audit_message, $companyModel->company_name);
        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);

    }

    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function registerProvider11(Request $request)
    {

        return response()->json( $_SERVER['HTTP_HOST'] );
        //return response()->json( Config::get('constants.stripe.secret') );

        \Stripe\Stripe::setApiKey(Config::get('constants.stripe.secret'));
        try {

            $stripe = new \Stripe\StripeClient(Config::get('constants.stripe.secret'));

            $tokenRes = $stripe->tokens->create([
                'card' => [
                    'number'    => '4242424242424242',
                    'exp_month' => 9,
                    'exp_year'  => 2021,
                    'cvc'       => '314',
                ],
            ]);

            //return response()->json( $tokenRes );

            $token = $request->input('stripeToken');

            /*\Stripe\Charge::create ( array (
            "amount" => 2,
            "currency" => "usd",
            //"source" => $request->input ( 'stripeToken' ), // obtained with Stripe.js
            "source" => $tokenRes['id'],
            "description" => "Software development services."
            ) );*/

            $payment_intent = \Stripe\Charge::create([
                'amount'      => 3,
                'currency'    => 'usd',
                'source'      => $tokenRes['id'],
                'description' => 'Software development services',
                'shipping'    => [
                    'name'    => 'Jenny Rosen',
                    'address' => [
                        'line1'       => '510 Townsend St',
                        'postal_code' => '98140',
                        'city'        => 'San Francisco',
                        'state'       => 'CA',
                        'country'     => 'US',
                    ],
                ],
            ]);

            $message = 'Payment done successfully !';
            $success = true;
            $resCode = 201;

        } catch (\Exception $e) {
            $allErrors = $e->getMessage();

            $message = $allErrors;
            $success = false;
            $resCode = 409;
        }

        /* $stripe = new \Stripe\StripeClient('sk_test_BQokikJOvBiI2HlWgH4olfQ2');

        // Create a payment intent to start a purchase flow.
        $payment_intent = $stripe->paymentIntents->create([
        'amount' => 2000,
        'currency' => 'usd',
        'description' => 'My first payment',
        ]);

        // Complete the payment using a test card.
        $payment_intent->confirm([
        'payment_method' => 'pm_card_mastercard',
        ]);
         */

        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function billingWebhook(Request $request)
    {

        //return response()->json( Config::get('constants.stripe.secret') );
        try {

            mail("mohank.das@indusnet.co.in", "webhook test", "test data");

            $message = 'Test successfully !';
            $success = true;
            $resCode = 201;

        } catch (\Exception $e) {
            $allErrors = $e->getMessage();

            $message = $allErrors;
            $success = false;
            $resCode = 409;
        }

        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function assignProvider(Request $request)
    {
        //validate incoming request
        $validator = Validator::make($request->all(),
            [
                'company_doc' => 'required',
            ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()->all(),
                'success' => false,
            ], 400);
        }

        \DB::beginTransaction();
        try {
            $companyModel = CompanyDetail::where('user_id', $request->input('user_id'))->first();

            /*return response()->json([
            'request' => $companyModel,
            ], 200);
             */
            // Add provider to gems
            $relationModel = new GemsProviderRelation;

            $relationModel->user_id = $request->input('user_id');
            $relationModel->gems_id = $request->input('gems_id');

            $relationModel->save();

            if ($request->hasFile('company_doc')) {

                $file       = $request->file('company_doc');
                $user_id    = $request->input('user_id');
                $company_id = $companyModel->id;

                $fileCount = $this->moveFile($file, $user_id, $company_id);
            }
            \DB::commit();

            $message       = 'Your Claim Submitted.';
            $success       = true;
            $resCode       = 201;
            $audit_message = 'ASSIGN_PROVIDER_SUCCESS';

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $resCode       = 409;
            $audit_message = 'ASSIGN_PROVIDER_FAIL';
        }

        $this->insertAuditTrail('PROVIDER', $audit_message, $companyModel->company_name);
        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);

    }

    protected function moveFile($file, $user_id, $company_id)
    {

        $fileCount = count((array) $file);

        try {
            $pd = new CompanyDocument;

            $notMoveFileArr = $pdArr = array();

            for ($i = 0; $i < $fileCount; $i++) {
                $originalFileName = $file[$i]->getClientOriginalName();
                $filteredOrgName  = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $originalFileName);
                $extension        = strtolower($file[$i]->getClientOriginalExtension());
                $fileName         = date('YmdHmsu') . '-' . $filteredOrgName;

                $fileSize = $file[$i]->getSize();

                $fileSize = number_format($fileSize / 1048576, 2);

                $destinationPath = "upload/company_documents/" . $company_id . "/";
                $fullFilePath    = $destinationPath . $fileName;

                if ($file[$i]->move($destinationPath, $fileName)) {

                    if ($fileSize > 6) {

                        $oldFullFilePath = $fullFilePath;

                        $fileName     = date('YmdHmsu') . '-converted-' . $filteredOrgName;
                        $fullFilePath = $destinationPath . $fileName;

                        if ($extension == 'pdf') {

                            shell_exec('ps2pdf -dPDFSETTINGS=/default ' . $oldFullFilePath . '  ' . $fullFilePath . '');

                        } else if ($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'tiff') {

                            shell_exec('convert -quality 50% ' . $oldFullFilePath . '  ' . $fullFilePath . '');

                        } /*else{
                        rename( $orgFullFilePath, $fullFilePath);
                        }*/

                        if (File::exists($oldFullFilePath)) {
                            File::delete($oldFullFilePath);
                        }
                    }

                    $pdArr[] = [
                        'user_id'       => $user_id,
                        'company_id'    => $company_id,
                        'org_file_name' => $originalFileName,
                        'file_name'     => $fileName,
                        'file_path'     => $fullFilePath,
                        'created_at'    => date('Y-m-d H:i:s'),
                        'updated_at'    => date('Y-m-d H:i:s'),
                    ];

                } else {
                    $notMoveFileArr[] = $originalFileName;
                }
            }

            $pd::insert($pdArr);
            return $notMoveFileArr;

        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 409);
        }
    }

    public function getLogList(Request $request)
    {
        $pageSize  = $request->input('pageSize') ? $request->input('pageSize') : '10';
        $startDate = $request->get('startDate');
        $endDate   = $request->get('endDate');
        $userid    = $request->get('userid');

        $query = DB::table('audit_trails')
            ->leftJoin('users', 'audit_trails.action_by', '=', 'users.id')
            ->leftJoin('user_details', 'user_details.user_id', '=', 'users.id')
            ->select(
                'audit_trails.page',
                'audit_trails.description',
                'audit_trails.action_on',
                'audit_trails.action_id',
                'audit_trails.ip_address',
                'audit_trails.created_at',
                'user_details.first_name',
                'user_details.last_name',
                'users.email');
        if ($userid != '') {
            $query->where('audit_trails.action_by', $userid);
        }

        if ($startDate != '' && $endDate != '') {
            $query->whereBetween('audit_trails.created_at', [$startDate, $endDate]);
        }

        $data = $query->orderBy('audit_trails.id', 'ASC')
            ->paginate($pageSize);

        $pcnt = 0;
        foreach ($data as $pval) {

            $desc = ucfirst(strtolower(implode(' ', explode('_', $data[$pcnt]->description))));

            $data[$pcnt]->pageDesc = $desc;
            $data[$pcnt]->page     = ucfirst(strtolower($data[$pcnt]->page));

            $pcnt++;
        }

        return response()->json([
            'data'    => $data,
            'success' => true,
        ], 200);
    }

    /**
     * Verify user account.
     *
     * @param  Request  $request
     * @return Response
     */

    public function verifyAccount($token)
    {
        if ($token == "") {
            $message     = 'Oops! Something went wrong in this url.';
            $success     = false;
            $respondCode = 200;

        }

        $model = User::where('active_token', $token)->first();

        if (empty($model)) {
            $message     = 'Requested url is no longer valid. Please try again.';
            $success     = false;
            $respondCode = 200;

        } else {
            $model->active_token = null;
            $model->is_verified  = '1';
            if ($model->save()) {
                $userDetails                 = UserDetail::where('user_id', $model->id)->first();
                $userDetails->verified_email = $model->email;
                $userDetails->save();
                $message     = 'Hi '.$userDetails->first_name.', Your email has been verified successfully.';
                $success     = true;
                $respondCode = 200;
            }
        }

        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $respondCode);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param  Request  $request
     * @return Response
     */
    public function login(Request $request)
    {
        //$decrypted = $this->decrypt($request->EncryptionData);
        //return response()->json(['message' => $decrypted], 201);

        // $this->validate($request, [
        //     'email'    => 'required|email|max:255',
        //     'password' => 'required',
        // ]);

        $validator = Validator::make($request->all(),
            [
                'password' => 'required',
                'email'    => 'required|email|max:255',
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()->all(),
                'success' => false,
            ], 400);
        }

        try {

            $user = User::where('email', '=', $request->input('email'))
                ->where('user_type', '=', $request->input('user_type'))
                ->first();

            //->where('is_verified', '1')
            //->where('active_status', '1')

            //$userStatus = $user->count();

            //$credentials = $request->only(['email', 'password']);
            //if (! $token = Auth::attempt($credentials)) {

            //return response()->json(['message' => $user], 201);

            if ($user !== null) {
                if ($user->is_verified === 1) {
                    if ($user->active_status === 1) {
                        if (!$token = $this->jwt->attempt($request->only('email', 'password'))) {
                            return response()->json([
                                'message' => 'Email and password are not correct',
                                'success' => false,
                            ], 400);
                        }
                    } else {
                        return response()->json([
                            'message' => 'Account is not active yet! Please contact admin.',
                            'success' => false,
                        ], 400);
                    }
                } else {
                    return response()->json([
                        'message' => 'Account is not verified! Please contact admin.',
                        'success' => false,
                    ], 400);
                }
            } else {
                return response()->json([
                    'message' => 'Account not exist.',
                    'success' => false,
                ], 400);
            }

        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json([
                'message' => 'Token Expired',
                'success' => false,
            ], 500);

        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json([
                'message' => 'Token Invalid',
                'success' => false,
            ], 500);

        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json([
                'message' => 'Token Absent!' . $e->getMessage(),
                'success' => false,
            ], 500);

        }
        $loginHistory             = new loginHistory;
        $loginHistory->user_id    = Auth::user()->id;
        $loginHistory->ip_address = $request->ip();
        $loginHistory->login_time = date('Y-m-d  H:i:s');
        $loginHistory->save();
        $platform = $request->input('platform','Web');
        $this->insertAuditTrail('LOGIN', 'LOGIN_SUCCESS',Config::get('messages.audit.login'),$platform, $request->input('email'));

        $user = User::find(Auth::user()->id);
        if ($user) {
            $user->device_token = $request->input('device_token');
            $user->save();
        }

        // $device = UserDevice::where('user_id', Auth::user()->id)->where('device_id', $request->input('device_id'))->first();
        // if ($device) {

        // } else {
        //     $device            = new UserDevice;
        //     $device->user_id   = Auth::user()->id;
        //     $device->device_id = $request->input('device_id');
        //     $device->save();
        // }
        return $this->respondWithToken($token);
    }

    /**
     * Login or Register via social media.
     *
     * @param  Request  $request
     * @return Response
     */
    public function socialMediaLogin(Request $request)
    {
        $this->validate($request, [
            'email'            => 'required|email|max:255',
            'provider_user_id' => 'required',
        ]);

        try {

            $user = User::where('email', '=', $request->input('email'))->first();
            if ($user) {
                if ($user->active_status == 1) {
                    $social_account = SocialAccount::where('provider_user_id', $request->input('provider_user_id'))->first();
                    if (!$social_account) {
                        $new_social_account                   = new SocialAccount;
                        $new_social_account->provider_user_id = $request->input('provider_user_id');
                        $new_social_account->provider         = $request->input('provider');
                        $new_social_account->user_id          = $user->id;
                        $new_social_account->save();
                    }
                } else {
                    return response()->json([
                        'message' => 'Account is inactive',
                        'success' => false,
                    ], 500);
                }
            } else {
                $user = new User;

                $user->email         = $request->input('email');
                $user->device_token  = $request->input('device_token');
                $user->active_status = 1;
                $user->is_verified   = 1;

                if ($user->save()) {

                    $userD = new UserDetail;

                    $userD->user_id    = $user->id;
                    $userD->first_name = $request->input('first_name');
                    $userD->last_name  = $request->input('last_name');

                    $userD->save();

                    $new_social_account                   = new SocialAccount;
                    $new_social_account->provider_user_id = $request->input('provider_user_id');
                    $new_social_account->provider         = $request->input('provider');
                    $new_social_account->user_id          = $user->id;
                    $new_social_account->save();

                    $GemsLabel             = new GemsLabel;
                    $GemsLabel->label_name = 'Favourite places';
                    $GemsLabel->user_id    = $user->id;
                    $GemsLabel->save();
                }
            }

        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json([
                'message' => 'Token Expired',
                'success' => false,
            ], 500);

        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json([
                'message' => 'Token Invalid',
                'success' => false,
            ], 500);

        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json([
                'message' => 'Token Absent!' . $e->getMessage(),
                'success' => false,
            ], 500);
        }

        $token = $this->jwt->fromUser($user);
        $this->jwt->setToken($token);
        $toUser = $this->jwt->toUser($user);

        $loginHistory             = new loginHistory;
        $loginHistory->user_id    = Auth::user()->id;
        $loginHistory->ip_address = $request->ip();
        $loginHistory->login_time = date('Y-m-d  H:i:s');
        $loginHistory->save();
        $this->insertAuditTrail('LOGIN', 'SOCIAL_LOGIN_SUCCESS', $request->input('email'));
        return $this->respondWithToken($token);
    }

    public function forgotPassword(Request $request)
    {
        $emailId = $request->input('email');

        $user = User::where('email', '=', $emailId)
            ->get();

        $token = Str::random(32);

        if (count($user) < 1) {
            return response()->json(['message' => 'Email does not exists'], 200);
        }

        User::where('email', $emailId)
            ->update(['password_reset_token' => $token]);

        try {
            $name = $user[0]->entity_name;

            $link = env('FRONT_APP_URL') . 'reset-password/' . $token;
            Mail::to($emailId)->send(new Forgotpassword($name, $link));

            $this->insertAuditTrail('LOGIN', 'FORGOT_PASSWORD', $request->input('email'));

            return response()->json(['message' => 'Reset password mail sent successfully!!'], 200);

        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => $e->getMessage()], 409);
        }

    }

    public function forgotPasswordVerify($token)
    {

        if ($token == "") {
            return response()->json([
                'message' => 'Oops! Something went wrong in this url.',
                'success' => false,
            ], 201);

        }
        //$id = base64_decode($id);
        $model = User::where('password_reset_token', $token)
            ->first();
        if (empty($model)) {
            return response()->json([
                'message' => 'Requested URL is no longer valid.',
                'success' => false,
            ], 201);
        } else {
            /* $model->password_reset_token = NULL;
            $model->save(); */

            return response()->json([
                'message' => 'Reset your password',
                'token'   => $token,
                'success' => true,
            ], 200);
        }
    }

    /**
     * Login or Register via social media.
     *
     * @param  Request  $request
     * @return Response
     */
    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'token'    => 'required',
                'password' => 'required|confirmed',
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()->all(),
                'success' => false,
            ], 201);
        }
        try {

            $model = User::where('password_reset_token', $request->token)->first();
            if (empty($model)) {
                return response()->json([
                    'message' => 'Token has expired',
                    'success' => false,
                ], 201);
            } else {
                $plainPassword               = $request->input('password');
                $model->password             = app('hash')->make($plainPassword);
                $model->vp                   = $plainPassword;
                $model->password_reset_token = null;
                $model->save();

                $this->insertAuditTrail('LOGIN', 'RESET_PASSWORD', $model->email);

                return response()->json([
                    'message' => 'Password Reset Successfully!!',
                    'success' => true,
                ], 200);
            }

        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

}
