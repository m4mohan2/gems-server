<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;


class CityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');    
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $data = City::all();

        return response()->json([
            'data' => $data ,
            'totalCount' => $data->count(), 
            'success'=> true,
        ], 200);
    }

    public function getCities(Request $request) {

        //$data = City::all();
        $data = City::limit(1000)->get();
        $search = $request->get('id');

        if ($search!='') {
            $data = City::where('state_id','=',$search)->get();         
        }

        return response()->json([
            'data' => $data ,
            'totalCount' => $data->count(), 
            'success'=> true,
        ], 200);   
    }

    
}
