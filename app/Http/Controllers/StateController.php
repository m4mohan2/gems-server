<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\State;


class StateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth'); 
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {
        //$states = State::all();
        $states = State::with([
                    'cities'
            ])->get();

        return response()->json([
            'data' => $states ,
            'totalCount' => $states->count(), 
            'success'=> true,
        ], 200);
    }

    public function getStates(Request $request) {

        $data = State::limit(100)->get();

        $search = $request->get('id');
        if ($search!='') {
            $data = State::where('country_id','=',$search)->get();         
        }

        return response()->json([
            'data' => $data ,
            'totalCount' => $data->count(), 
            'success'=> true,
        ], 200);   
    }
}
