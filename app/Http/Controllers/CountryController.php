<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\State;


class CountryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth'); 
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {
        $data = Country::with([
                    'states'
            ])->get();

        return response()->json([
            'data' => $data ,
            'totalCount' => $data->count(), 
            'success'=> true,
        ], 200);
    }
   
}
