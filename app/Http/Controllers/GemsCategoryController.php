<?php

namespace App\Http\Controllers;

use App\GemsCategory;
use Illuminate\Http\Request;

class GemsCategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the records.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = 100;
        $searchQuery = $request->get('searchQuery');

        $models = GemsCategory::where('status','1')->orderBy('id', 'DESC');


        if ($searchQuery) {
            $results = $models->paginate($perPage);
        } else {
            $results = $models->get();
        }

        /*return response()->json([
            'data'       => $models,
            'totalCount' => $models->count(),
            'success'    => true,
        ], 200);*/

        return response()->json($results);
    }

    /**
     * Display category listing for admin
     *
     * @return void
     */

    public function categoryList(Request $request)
    {
        $perPage = 100;
        $searchQuery = $request->get('searchQuery');

        $models = GemsCategory::orderBy('id', 'DESC');


        if ($searchQuery) {
            $results = $models->paginate($perPage);
        } else {
            $results = $models->get();
        }

        /*return response()->json([
            'data'       => $models,
            'totalCount' => $models->count(),
            'success'    => true,
        ], 200);*/

        return response()->json($results);
    }

    /**
     * created new record.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //dd($request)
        $this->validate($request, [
            'category_name' => 'required|unique:gems_categories',
        ]);
        \DB::beginTransaction();
        try {

            $model                = new GemsCategory;
            $model->category_name = $request->input('category_name');
            $model->status = 1;

            $model->save();

            \DB::commit();
            $message = 'Inserted successfully!';
            $success = true;
            $this->insertAuditTrail('CREATE_GEMS_CATEGORY', 'CREATE_GEMS_CATEGORY_SUCCESS');
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
            $this->insertAuditTrail('CREATE_GEMS_CATEGORY', 'CREATE_GEMS_CATEGORY_FAIL');
        }

        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 201);

    }

    /**
     * Update record
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category_name' => 'required',
        ]);

        \DB::beginTransaction();
        try {

            $model = GemsCategory::find($id);

            if (!empty($model)) {
                $model->category_name = $request->input('category_name');

                $Category = GemsCategory::where('category_name', '=', $model->category_name)->where('id', '!=', $id)->first();

                if ($Category === null) {
                    $model->save();

                    \DB::commit();
                    $message       = 'Updated successfully!';
                    $success       = true;
                    $audit_message = 'UPDATE_GEMS_CATEGORY_SUCCESS';
                } else {
                    $message       = 'Category Name is already exist.';
                    $success       = false;
                    $audit_message = 'UPDATE_GEMS_CATEGORY_FAIL';
                }
            } else {
                $message       = 'Record not exists!';
                $success       = true;
                $audit_message = 'UPDATE_GEMS_CATEGORY_FAIL';
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $audit_message = 'UPDATE_GEMS_CATEGORY_FAIL';
        }

        //Return message
        $this->insertAuditTrail('UPDATE_GEMS_CATEGORY', $audit_message);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 200);
    }
    /**
     * Delete record.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        \DB::beginTransaction();
        try {
            $model = GemsCategory::find($id);
            if (!empty($model)) {
                $model->delete();

                \DB::commit();
                $message       = 'Removed successfully!';
                $success       = true;
                $audit_message = 'DELETE_GEMS_CATEGORY_SUCCESS';

            } else {
                $message       = 'Record not exists!';
                $success       = true;
                $audit_message = 'DELETE_GEMS_CATEGORY_FAIL';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $alerttype     = false;
            $audit_message = 'DELETE_GEMS_CATEGORY_FAIL';
        }

        //Return message
        $this->insertAuditTrail('DELETE_GEMS_CATEGORY', $audit_message);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 200);
    }
    /**
     * Status update.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function statusUpdate(Request $request, $id)
    {
        //return response()->json($request->input('status'));
        \DB::beginTransaction();
        try {
            $model = GemsCategory::find($id);
            if (!empty($model)) {

                if ($model->status != (int) $request->input('status')) {
                    $model->status = $request->input('status');
                }

                // if ($model->is_approved != (int) $request->input('is_approved')) {
                //     $model->is_approved = $request->input('is_approved');
                // }

                $model->save();

                \DB::commit();
                $message       = 'Updated successfully!';
                $success       = true;
                $audit_message = 'STATUS_UPDATE_GEMS_CATEGORY_FAIL';

            } else {
                $message       = 'record not exists!';
                $success       = true;
                $audit_message = 'STATUS_UPDATE_GEMS_CATEGORY_FAIL';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $audit_message = 'STATUS_UPDATE_GEMS_CATEGORY_FAIL';
        }

        $this->insertAuditTrail('STATUS_UPDATE_GEMS_CATEGORY', $audit_message);
        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 200);
    }
}
