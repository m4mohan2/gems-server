<?php

namespace App\Http\Controllers;

use App\Sitesetting;
use Illuminate\Http\Request;

class SitesettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function sitesettingList()
    {
        $models = Sitesetting::get();
        return response()->json($models);
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'setting_name'  => 'required',
            'setting_value' => 'required',
        ]);

        try {

            $models                = new Sitesetting;
            $models->setting_name  = $request->setting_name;
            $models->setting_label = preg_replace("![^a-z0-9]+!i", "-", strtolower($models->setting_name));
            $models->setting_value = $request->setting_value;
            $models->save();

            return response()->json([
                'message' => 'Record added successfully',
                'success' => true,
            ], 200);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $models = Sitesetting::find($id);
             if ($request->setting_name != '') {
                $models->setting_name = $request->setting_name;
            }

            if ($request->setting_value != '') {
                $models->setting_value = $request->setting_value;
            }
            $models->save();

            return response()->json([
                'message' => 'Record updated successfully',
                'details'    => $models,
                'success' => true,
            ], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'success' => false], 400);
        }
    }
}
