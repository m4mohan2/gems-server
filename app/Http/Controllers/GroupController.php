<?php

namespace App\Http\Controllers;

use App\Group;
use App\GroupMember;
use App\ShareGemsGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class GroupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //

    /**
     * Group List.
     *
     * @param  Request  $request
     * @return Response
     */

    public function groupList()
    {
        $user_id = Auth::user()->id;
        $models  = Group::with([
            'user',
        ])
            ->withCount(['shareGems' => function ($query) use ($user_id) {
                $query->where('share_by', $user_id);
            }])
            ->where('status', '=', '1')
            ->where('creator_id', $user_id)->get();
            $appmessage = Config::get('messages.audit.view_group_list');
        $this->insertAuditTrail('GROUP', 'VIEW_GROUP_LIST',$appmessage,'',Auth::user()->email);
        return response()->json($models);
    }

    /**
     * Gems list by group.
     *
     * @param  Request  $request
     * @return Response
     */

    public function getGemsByGroupList(Request $request, $group_id)
    {
        $logged_user_id = Auth::user()->id;
        $pageSize       = $request->input('pageSize') ? $request->input('pageSize') : '20';
        $total_like_gem = ShareGemsGroup::with(['gems', 'gemsImages', 'gemsCategoryDetail', 'gemsCreatedBy', 'user_like_status' => function ($q) use ($logged_user_id) {
            $q->where('user_id', $logged_user_id);
            $q->select('gems_id', 'action');
        }])
            ->withCount('total_like')
            ->where('group_id', $group_id);
        $total_like_gem = $total_like_gem->paginate($pageSize);

        return response()->json([
            'gems_list' => $total_like_gem,
            'success'   => true,
        ], 200);
    }

    /**
     * Create a group.
     *
     * @param  Request  $request
     * @return Response
     */

    public function createGroup(Request $request)
    {
        $member_ids = $request->member_ids;
        $group_name = $request->group_name;
        $this->validate($request, [
            'group_name' => 'required|unique:groups',
        ]);

        \DB::beginTransaction();
        try {
            $models             = new Group;
            $models->group_name = $group_name;
            $models->creator_id = Auth::user()->id;
            $models->status     = 1;
            if ($models->save()) {
                $group_member           = new GroupMember;
                $group_member->group_id = $models->id;
                $group_member->user_id  = Auth::user()->id;
                $group_member->is_admin = '1';
                $group_member->save();
                if (count($member_ids) > 0) {
                    foreach ($member_ids as $key => $member) {
                        $group_member           = new GroupMember;
                        $group_member->group_id = $models->id;
                        $group_member->user_id  = $member;
                        $group_member->is_admin = '0';
                        $group_member->save();
                    }
                }
                \DB::commit();
                $message       = 'Group has been ceated successfully';
                $success       = true;
                $resCode       = 200;
                $audit_message = 'GROUP_CREATE_SUCCESS';
                $audit_on      = $group_name;
                $action_id     = $models->id;

            }
        } catch (\Exception $e) {
            //return error message
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'GROUP_CREATE_FAIL';
            $audit_on      = '';
            $action_id     = null;

        }

        $appmessage = Config::get('messages.audit.create_group');
        $this->insertAuditTrail('GROUP', $audit_message,$appmessage,'', $audit_on, $action_id);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    /**
     * Update Group member.
     *
     * @param  Request  $request
     * @return Response
     */

    public function updateGroupMember(Request $request)
    {
        $group_id       = $request->group_id;
        $member_ids     = $request->member_ids;
        $logged_user_id = Auth::user()->id;
        try {
            if (count($member_ids) > 0) {
                GroupMember::where('group_id', '=', $group_id)->delete();
                foreach ($member_ids as $key => $member) {
                    $group_member           = new GroupMember;
                    $group_member->group_id = $group_id;
                    $group_member->user_id  = $member;
                    $group_member->is_admin = '0';
                    $group_member->save();
                }

                \DB::commit();
                $message       = 'Member has been updated successfully';
                $success       = true;
                $resCode       = 200;
                $audit_message = 'UPDATE_GROUP_MEMBER_SUCCESS';
                $audit_on      = $this->groupName($group_id);
                $action_id     = $group_id;
            } else {
                \DB::commit();
                $message       = 'Please select at least one member';
                $success       = false;
                $resCode       = 400;
                $audit_message = 'UPDATE_GROUP_MEMBER_FAIL';
                $audit_on      = $this->groupName($group_id);
                $action_id     = $group_id;
            }
        } catch (\Exception $e) {
            //return error message
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'UPDATE_GROUP_MEMBER_FAIL';
            $audit_on      = '';
            $action_id     = null;

        }
        $appmessage = Config::get('messages.audit.update_group_member');
        $this->insertAuditTrail('GROUP', $audit_message, $appmessage,'', $audit_on, $action_id);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    /**
     * Join to a group.
     *
     * @param  Request  $request
     * @return Response
     */

    public function joinGroup(Request $request)
    {
        $group_id = $request->group_id;
        $user_id  = $request->user_id;
        \DB::beginTransaction();
        try {
            $count = GroupMember::where('group_id', '=', $group_id)
                ->where('user_id', '=', $user_id)
                ->count();
            if ($count == 0) {
                $group_member           = new GroupMember;
                $group_member->group_id = $group_id;
                $group_member->user_id  = $user_id;
                $group_member->is_admin = '0';
                $group_member->save();
                \DB::commit();
                $message       = 'User have been join group successfully';
                $success       = true;
                $resCode       = 200;
                $audit_message = 'JOIN_GROUP_SUCCESS';
                $audit_on      = $this->groupName($group_id);
                $action_id     = $group_id;

            } else {
                \DB::commit();
                $message       = 'User already joined this group';
                $success       = false;
                $resCode       = 400;
                $audit_message = 'ALREADY_JOIN_GROUP';
                $audit_on      = '';
                $action_id     = null;
            }

        } catch (\Exception $e) {
            //return error message
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'JOIN_GROUP_FAIL';
            $audit_on      = '';
            $action_id     = null;
        }
        $appmessage = Config::get('messages.audit.join_group');
        $this->insertAuditTrail('GROUP', $audit_message,$appmessage,'', $audit_on, $action_id);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    /**
     * Remove a user from a group.
     *
     * @param  Request  $request
     * @return Response
     */

    public function removeFromGroup(Request $request)
    {
        $group_id = $request->group_id;
        $user_id  = $request->user_id;

        \DB::beginTransaction();
        try {
            $model = GroupMember::where('group_id', '=', $group_id)
                ->where('user_id', '=', $user_id)->first();
            if (!empty($model)) {
                $model->delete();
                \DB::commit();
                $message       = 'User have been removed from the group successfully';
                $success       = true;
                $resCode       = 200;
                $audit_message = 'REMOVE_FROM_GROUP_SUCCESS';
                $audit_on      = $this->groupName($group_id);
                $action_id     = $group_id;
                $action_to     = $user_id;
            } else {
                \DB::commit();
                $message       = 'Record not exists!';
                $success       = false;
                $resCode       = 400;
                $audit_message = 'REMOVE_FROM_GROUP_FAIL';
                $audit_on      = '';
                $action_id     = null;
                $action_to     = null;
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'REMOVE_FROM_GROUP_FAIL';
            $audit_on      = '';
            $action_id     = null;
            $action_to     = null;

        }
        $appmessage = Config::get('messages.audit.remove_from_group');
        $this->insertAuditTrail('GROUP', $audit_message,$appmessage,'', $audit_on, $action_id, $action_to);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    /**
     * User can left group by himself
     *
     * @param  Request  $request
     * @return Response
     */

    public function leftGroup(Request $request)
    {
        $group_id = $request->group_id;
        \DB::beginTransaction();
        try {
            $models = GroupMember::where('group_id', '=', $group_id)
                ->where('user_id', '=', Auth::user()->id)
                ->first();
            if ($models->is_admin == 1) {
                $admin_count = GroupMember::where('group_id', '=', $group_id)
                    ->where('is_admin', '=', '1')->count();

                if ($admin_count > 1) {
                    $models->delete();

                    \DB::commit();
                    $message       = 'You have left the group successfully';
                    $success       = true;
                    $resCode       = 200;
                    $audit_message = 'LEFT_GROUP_SUCCESS';
                    $audit_on      = $this->groupName($group_id);
                    $action_id     = $group_id;
                } else {

                    \DB::commit();
                    $message       = 'Please give atleast one member to admin rights';
                    $success       = false;
                    $resCode       = 400;
                    $audit_message = 'LEFT_GROUP_FAIL';
                    $audit_on      = $this->groupName($group_id);
                    $action_id     = $group_id;
                }
            } else {
                $models->delete();
                \DB::commit();
                $message       = 'You have left the group successfully';
                $success       = true;
                $resCode       = 200;
                $audit_message = 'LEFT_GROUP_SUCCESS';
                $audit_on      = $this->groupName($group_id);
                $action_id     = $group_id;
            }
        } catch (\Exception $e) {
            //return error message $e->getMessage()
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'LEFT_GROUP_FAIL';
            $audit_on      = $this->groupName($group_id);
            $action_id     = $group_id;
        }

        $appmessage = Config::get('messages.audit.left_group');
        $this->insertAuditTrail('GROUP', $audit_message,$appmessage,'', $audit_on, $action_id);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);

    }
/**
 * Group member list
 *
 * @param  Request  $request
 * @return Response
 */

    public function memberList(Request $request)
    {
        $group_id = $request->group_id;
        \DB::beginTransaction();
        try {
            $member = GroupMember::with(['user'])->where('group_id', '=', $group_id)->get();

            \DB::commit();
            $message       = $member;
            $success       = true;
            $resCode       = 200;
            $audit_message = 'VIEW_MEMBER_LIST_SUCCESS';
            $audit_on      = $this->groupName($group_id);
            $action_id     = $group_id;
        } catch (\Exception $e) {
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'VIEW_MEMBER_LIST_FAIL';
            $audit_on      = '';
            $action_id     = null;
        }
        $appmessage = Config::get('messages.audit.view_member_list');
        $this->insertAuditTrail('GROUP', $audit_message,$appmessage,'', $audit_on, $action_id);
        return response()->json([
            'list'    => $message,
            'success' => $success,
        ], $resCode);

    }

    /**
     * Make a normal user to admin
     *
     * @param  Request  $request
     * @return Response
     */

    public function makeGroupAdmin(Request $request)
    {
        $group_id       = $request->group_id;
        $user_id        = $request->user_id;
        $logged_user_id = Auth::user()->id;

        \DB::beginTransaction();
        try {
            $checkAdmin = GroupMember::where('group_id', '=', $group_id)
                ->where('user_id', '=', $logged_user_id)
                ->first();
            if (!empty($checkAdmin)) {
                if ($checkAdmin->is_admin == 1) {
                    $models = GroupMember::where('group_id', '=', $group_id)
                        ->where('user_id', '=', $user_id)
                        ->first();
                    if ($models) {
                        $models->is_admin = '1';
                        $models->save();
                        \DB::commit();
                        $message       = 'Admin has been assign successfully';
                        $success       = true;
                        $resCode       = 200;
                        $audit_message = 'MAKE_GROUP_ADMIN_SUCCESS';
                        $audit_on      = $this->groupName($group_id);
                        $action_id     = $group_id;
                    }
                } else {
                    \DB::commit();
                    $message       = 'You are not an admin.';
                    $success       = false;
                    $resCode       = 400;
                    $audit_message = 'MAKE_GROUP_ADMIN_SUCCESS_FAIL';
                    $audit_on      = $this->groupName($group_id);
                    $action_id     = $group_id;
                }
            } else {
                $message       = 'You are not a member.';
                $success       = false;
                $resCode       = 400;
                $audit_message = 'MAKE_GROUP_ADMIN_SUCCESS_FAIL';
                $audit_on      = $this->groupName($group_id);
                $action_id     = $group_id;
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'MAKE_GROUP_ADMIN_SUCCESS_FAIL';
            $audit_on      = $this->groupName($group_id);
            $action_id     = $group_id;
        }
        $appmessage = Config::get('messages.audit.make_group_admin');
        $this->insertAuditTrail('GROUP', $audit_message,$appmessage,'', $audit_on, $action_id);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    /**
     * Delete a group
     *
     * @param  Request  $request, $group_id
     * @return Response
     */

    public function deleteGroup(Request $request)
    {

        $group_id = $request->group_id;
        \DB::beginTransaction();
        try {
            $model = Group::where('id', '=', $group_id)->first();

            if (!empty($model)) {
                $group_name = $this->groupName($group_id);
                $model->delete();
                \DB::commit();
                $message       = 'Group has been deleted successfully';
                $success       = true;
                $resCode       = 200;
                $audit_message = 'DELETE_GROUP_SUCCESS';
                $audit_on      = $group_name;

            } else {
                \DB::commit();
                $message       = 'Record not found';
                $success       = false;
                $resCode       = 400;
                $audit_message = 'DELETE_GROUP_FAIL';
                $audit_on      = '';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'DELETE_GROUP_FAIL';
            $audit_on      = '';
        }
        $appmessage = Config::get('messages.audit.delete_group');
        $this->insertAuditTrail('GROUP', $audit_message,$appmessage,'', $audit_on);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);

    }

    private function groupName($id)
    {
        $group = Group::find($id);
        return $group->group_name;
    }
}
