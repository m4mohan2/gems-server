<?php

namespace App\Http\Controllers;

use App\Services;
use Illuminate\Http\Request;

class ServicesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $pageSize     = $request->input('pageSize') ? $request->input('pageSize') : '10';
        $searchString = $request->get('searchString');
        $export       = $request->get('export');
        $status       = $request->get('status');

        $select = Services::orderBy('id', 'DESC');

        if($status){
            $select->where('status', '=', '1');
        }

        //$select->orderBy('id', 'DESC');

        if ($export) {
            $data = $select->get();
        } else {
            $data = $select->paginate($pageSize);
        }

        return response()->json([
            'data'    => $data,
            'success' => true,
        ], 200);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'service_name'  => 'required|unique:services',
            'service_desc'  => 'required',
            'service_price' => 'required',
        ]);
        \DB::beginTransaction();
        try {

            $model                = new Services;
            $model->service_name  = $request->service_name;
            $model->service_desc  = $request->service_desc;
            $model->service_price = $request->service_price;
            $model->status        = $request->status;

            $model->save();

            \DB::commit();
            $message = 'Service Created Successfully.';
            $success = true;
            $resCode = 201;
            $audit_message = 'CREATE_SERVICE_SUCCESS';
            
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
            $resCode = 400;
            $audit_message = 'CREATE_SERVICE_FAIL';
        }
        $appmessage = Config::get('messages.audit.create_service');
        $this->insertAuditTrail('SERVICE',$audit_message,$appmessage, '', $model->service_name);
        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    public function update(Request $request, $id)
    {
        \DB::beginTransaction();
        try {
            $model = Services::find($id);
            if (!empty($model)) {
                if ($request->service_name != '') {
                    $model->service_name = $request->service_name;
                }

                if ($request->service_desc != '') {
                    $model->service_desc = $request->service_desc;
                }

                if ($request->service_price != '') {
                    $model->service_price = $request->service_price;
                }
                if ($request->status != '') {
                    $model->status = $request->status;
                }

                $model->save();
                \DB::commit();
                $message = 'Record Updated successfully!';
                $success = true;
                $resCode = 201;
                $audit_message = 'UPDATE_SERVICE_SUCCESS';
            } else {
                $message = 'Record not exists!';
                $success = true;
                $resCode = 400;
                $audit_message = 'UPDATE_SERVICE_FAIL';
            }
            
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
            $resCode = 400;
            $audit_message = 'UPDATE_SERVICE_FAIL';
        }
        $appmessage = Config::get('messages.audit.update_service');
        $this->insertAuditTrail('SERVICE', $audit_message,$appmessage);
        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }
    /**
     * Delete record.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        \DB::beginTransaction();
        try {
            $model = Services::find($id);
            if (!empty($model)) {
                $model->delete();

                \DB::commit();
                $message       = 'Removed successfully!';
                $success       = true;
                $audit_message = 'DELETE_SERVICES_SUCCESS';

            } else {
                $message       = 'Record not exists!';
                $success       = true;
                $audit_message = 'DELETE_SERVICES_FAIL';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $alerttype     = false;
            $audit_message = 'DELETE_SERVICES_FAIL';
        }
        $appmessage = Config::get('messages.audit.delete_service');
        //Return message
        $this->insertAuditTrail('DELETE_SERVICES', $audit_message,$appmessage);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 200);
    }
    /**
     * Status update.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function statusUpdate(Request $request, $id)
    {

        //return response()->json($request);
        \DB::beginTransaction();
        try {
            $model = Services::find($id);
            if (!empty($model)) {

                if ($model->status != (int) $request->input('status')) {
                    $model->status = $request->input('status');
                }

                $model->save();

                \DB::commit();
                $message       = 'Updated successfully!';
                $success       = true;
                $audit_message = 'STATUS_UPDATE_SERVICES_SUCCESS';

            } else {
                $message       = 'record not exists!';
                $success       = false;
                $audit_message = 'STATUS_UPDATE_SERVICES_FAIL';

            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $audit_message = 'STATUS_UPDATE_GEMS_FAIL';
        }
        $appmessage = Config::get('messages.audit.service_status_update');
        $this->insertAuditTrail('STATUS_UPDATE_SERVICES', $audit_message);
        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 200);
    }

}
