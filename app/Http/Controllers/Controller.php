<?php
namespace App\Http\Controllers;

//import auth facades
use App\AuditTrail;
use App\User;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    protected function respondWithToken($token)
    {
        $details = User::with([
            'userDetail',
            'privateData',
            'role',
        ])->exclude(['token', 'active_token', 'password_reset_token'])->where('id', '=', Auth::user()->id)->first();

        return response()->json([
            'token'   => $token,
            'user'    => $details,
            'message' => "Login Successful",
            'success' => true,
//'token_type' => 'bearer',
            //'expires_in' => Auth::factory()->getTTL() * 60
        ], 200);
    }

    protected function insertAuditTrail($page, $description, $message = null, $platform = 'Web', $action_on = null, $action_id = null, $action_to = null)
    {
        if (Auth::user()) {
            $model              = new AuditTrail;
            $model->page        = $page;
            $model->description = $description;
            $model->message     = $message;
            $model->platform    = $platform;
            $model->action_on   = $action_on;
            $model->action_to   = $action_to;
            $model->action_id   = $action_id;
            $model->access_info = $_SERVER['HTTP_USER_AGENT'];
            $model->action_by   = Auth::user()->id;
            $model->ip_address  = $this->get_client_ip();
            $model->save();
        }
    }

    protected function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP')) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        } else if (getenv('HTTP_X_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        } else if (getenv('HTTP_X_FORWARDED')) {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        } else if (getenv('HTTP_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        } else if (getenv('HTTP_FORWARDED')) {
            $ipaddress = getenv('HTTP_FORWARDED');
        } else if (getenv('REMOTE_ADDR')) {
            $ipaddress = getenv('REMOTE_ADDR');
        } else {
            $ipaddress = 'UNKNOWN';
        }

        return $ipaddress;
    }
}
