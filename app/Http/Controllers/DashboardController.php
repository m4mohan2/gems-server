<?php

namespace App\Http\Controllers;

use App\Country;
use App\Gems;
use App\GemsProviderRelation;
use App\User;
use App\UserDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //

    public function engagementSummery(Request $request, $id)
    {
        $from_date = $request->from_date . " 00:00:00";
        $to_date   = $request->to_date . " 23:59:59";
        DB::select(DB::raw("SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))"));
        $results = Gems::withCount([
            'total_like'    => function ($q) use ($from_date, $to_date) {
                $q->whereBetween('created_at', [$from_date, $to_date]);
            },
            'total_comment' => function ($q) use ($from_date, $to_date) {
                $q->whereBetween('created_at', [$from_date, $to_date]);
            },
            'total_view'    => function ($q) use ($from_date, $to_date) {
                $q->whereBetween('created_at', [$from_date, $to_date]);
            },
            'total_share'   => function ($q) use ($from_date, $to_date) {
                $q->whereBetween('created_at', [$from_date, $to_date]);
            },
            'saved_lists'   => function ($q) use ($from_date, $to_date) {
                $q->whereBetween('created_at', [$from_date, $to_date]);
            }])
            ->with([
                'total_like'  => function ($q) use ($from_date, $to_date) {
                    $q->whereBetween('created_at', [$from_date, $to_date]);
                    $q->select('gems_id', DB::raw('DATE(created_at) as date'), DB::raw('count(id)  as total'));
                    $q->groupBy('date');
                },
                'total_view'  => function ($q) use ($from_date, $to_date) {
                    $q->whereBetween('created_at', [$from_date, $to_date]);
                    $q->select('gems_id', DB::raw('DATE(created_at) as date'), DB::raw('count(id)  as total'));
                    $q->groupBy('date');
                },
                'total_share' => function ($q) use ($from_date, $to_date) {
                    $q->whereBetween('created_at', [$from_date, $to_date]);
                    $q->select('gems_id', DB::raw('DATE(created_at) as date'), DB::raw('count(id)  as total'));
                    $q->groupBy('date');
                },
                'saved_lists' => function ($q) use ($from_date, $to_date) {
                    $q->whereBetween('created_at', [$from_date, $to_date]);
                    $q->select('gems_id', DB::raw('DATE(created_at) as date'), DB::raw('count(id)  as total'));
                    $q->groupBy('date');
                },

            ])
            ->where('id', $id)
            ->get();
        $dateArr           = [];
        $Variable1         = strtotime($from_date);
        $Variable2         = strtotime($to_date);
        $diff              = strtotime($from_date) - strtotime($to_date);
        $days              = abs(round($diff / 86400));
        $likeListsArr      = json_decode(json_encode($results[0]->total_like));
        $likeDateArr       = array_column($likeListsArr, 'date');
        $likeTotalArr      = array_column($likeListsArr, 'total');
        $viewListsArr      = json_decode(json_encode($results[0]->total_view));
        $viewDateArr       = array_column($viewListsArr, 'date');
        $viewTotalArr      = array_column($viewListsArr, 'total');
        $shareListsArr     = json_decode(json_encode($results[0]->total_share));
        $shareDateArr      = array_column($shareListsArr, 'date');
        $shareTotalArr     = array_column($shareListsArr, 'total');
        $saveListsArr      = json_decode(json_encode($results[0]->saved_lists));
        $saveListsDateArr  = array_column($saveListsArr, 'date');
        $saveListsTotalArr = array_column($saveListsArr, 'total');
        for ($i = 0; $i <= $days; $i++) {
            $date                = date("Y-m-d", strtotime("+$i day", strtotime($from_date)));
            $dateArr[$i]['date'] = $date;
            if (false !== $key = array_search($date, $likeDateArr)) {
                $dateArr[$i]['like'] = $likeTotalArr[$key];
            } else {
                $dateArr[$i]['like'] = 0;
            }
            if (false !== $key = array_search($date, $viewDateArr)) {
                $dateArr[$i]['view'] = $viewTotalArr[$key];
            } else {
                $dateArr[$i]['view'] = 0;
            }
            if (false !== $key = array_search($date, $shareDateArr)) {
                $dateArr[$i]['share'] = $shareTotalArr[$key];
            } else {
                $dateArr[$i]['share'] = 0;
            }
            if (false !== $key = array_search($date, $saveListsDateArr)) {
                $dateArr[$i]['saved'] = $saveListsTotalArr[$key];
            } else {
                $dateArr[$i]['saved'] = 0;
            }
        }

        return response()->json([
            'message' => $dateArr,
            'success' => true,
        ], 200);

    }

    public function popularGems(Request $request)
    {
        $from_date = $request->from_date . " 00:00:00";
        $to_date   = $request->to_date . " 23:59:59";
        $user_id   = Auth::user()->id;
        try {
            $results = Gems::withCount([
                'total_like'    => function ($q) use ($from_date, $to_date) {
                    $q->whereBetween('created_at', [$from_date, $to_date]);
                },
                'total_comment' => function ($q) use ($from_date, $to_date) {
                    $q->whereBetween('created_at', [$from_date, $to_date]);
                },
                'total_view'    => function ($q) use ($from_date, $to_date) {
                    $q->whereBetween('created_at', [$from_date, $to_date]);
                },
                'total_share'   => function ($q) use ($from_date, $to_date) {
                    $q->whereBetween('created_at', [$from_date, $to_date]);
                },
                'saved_lists'   => function ($q) use ($from_date, $to_date) {
                    $q->whereBetween('created_at', [$from_date, $to_date]);
                }])->where('privacy', '!=', '2');
            // if (Auth::user()->user_type != 0) {
            //     $results->where('created_by', '=', $user_id);
            // }

            if (Auth::user()->user_type == 2) {

                $provider    = GemsProviderRelation::where('user_id', $user_id)->select('gems_id')->where('action', '1')->get();
                $providerArr = json_decode(json_encode($provider));
                $gems_ids    = array_column($providerArr, 'gems_id');
                $results->whereIn('id', $gems_ids);
            }
            $results = $results->orderBy(DB::raw("total_like_count + total_comment_count + total_share_count"), 'DESC')
                ->take(5)
                ->get();

            if (count($results) > 0) {
                for ($i = 0; $i < count($results); $i++) {
                    $current_total  = $results[$i]->total_like_count + $results[$i]->total_comment_count + $results[$i]->total_view_count + $results[$i]->saved_lists_count;
                    $previous_total = $this->previousMonthGemsStatus($results[$i]->id, $from_date, $to_date);
                    $previous_total = $previous_total->original;
                    if ($previous_total > 0) {
                        $growth = round(100 * ($current_total - $previous_total) / $previous_total);
                    } else {
                        $growth = $current_total;
                    }
                    $results[$i]['growth'] = $growth;

                }
            }
        } catch (\Exception $e) {
            $allErrors = $e->getMessage();
            return response()->json([
                'message' => $allErrors,
                'success' => false,
            ], 400);
        }

        return response()->json([
            'message' => $results,
            'success' => true,
        ], 200);
    }

    public function previousMonthGemsStatus($id, $from_date, $to_date)
    {
        $from_date = $from_date . " 00:00:00";
        $to_date   = $to_date . " 23:59:59";

        $from_date = date('Y-m-d H:i:s', strtotime($from_date . ' -1 month'));
        $to_date   = date('Y-m-d H:i:s', strtotime($to_date . ' -1 month'));
        $user_id   = Auth::user()->id;
        $results   = Gems::withCount([
            'total_like'    => function ($q) use ($from_date, $to_date) {
                $q->whereBetween('created_at', [$from_date, $to_date]);
            },
            'total_comment' => function ($q) use ($from_date, $to_date) {
                $q->whereBetween('created_at', [$from_date, $to_date]);
            },
            'total_view'    => function ($q) use ($from_date, $to_date) {
                $q->whereBetween('created_at', [$from_date, $to_date]);
            },
            'total_share'   => function ($q) use ($from_date, $to_date) {
                $q->whereBetween('created_at', [$from_date, $to_date]);
            },
            'saved_lists'   => function ($q) use ($from_date, $to_date) {
                $q->whereBetween('created_at', [$from_date, $to_date]);
            }])
            ->where('id', '=', $id)
            ->where('privacy', '!=', '2');
        // if (Auth::user()->user_type != 0) {
        //     $results->where('created_by', '=', $user_id);
        // }
        $results = $results->first();

        $total = $results->total_like_count + $results->total_comment_count + $results->total_view_count + $results->saved_lists_count;
        // return response()->json([
        //     'total' => $total,
        //     'success' => true,
        // ], 200);
        return response()->json($total);
    }

    public function audienceCountryWise(Request $request, $id)
    {
        $from_date = $request->from_date . " 00:00:00";
        $to_date   = $request->to_date . " 23:59:59";
        $results   = Country::withCount([
            'gemsLike'    => function ($q) use ($from_date, $to_date, $id) {
                $q->where('gems_id', $id);
                $q->whereBetween('gems_likeunlikes.created_at', [$from_date, $to_date]);
            },
            'gemsComment' => function ($q) use ($from_date, $to_date, $id) {
                $q->where('gems_id', $id);
                $q->whereBetween('gems_comments.created_at', [$from_date, $to_date]);
            },
            'gemsShare'   => function ($q) use ($from_date, $to_date, $id) {
                $q->where('gems_id', $id);
                $q->whereBetween('share_gems_groups.created_at', [$from_date, $to_date]);
            }])
            ->orderBy(DB::raw("gems_like_count + gems_comment_count + gems_share_count"), 'DESC')
            ->orderBy('name', 'ASC')
            ->get();

        return response()->json([
            'message' => $results,
            'success' => true,
        ], 200);
    }

    public function engagementGenderWise(Request $request, $id)
    {
        $from_date = $request->from_date . " 00:00:00";
        $to_date   = $request->to_date . " 23:59:59";
        DB::select(DB::raw("SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))"));
        $results = DB::table('gems_likeunlikes')
            ->leftJoin('user_details', 'gems_likeunlikes.user_id', '=', 'user_details.user_id')
            ->where('gems_likeunlikes.gems_id', '=', $id)
            ->where('gems_likeunlikes.action', '=', '1')
            ->whereBetween('gems_likeunlikes.created_at', [$from_date, $to_date])
            ->groupBy('user_details.gender')
            ->select('user_details.gender', DB::raw("count(*) as total"), DB::raw("round(count(*) *100 / (select count(*) from gems_likeunlikes where gems_id = $id and action = 1)) as percent"))
            ->get();

        return response()->json([
            'message' => $results,
            'success' => true,
        ], 200);
    }

    public function adminAudienceCountryWise(Request $request)
    {
        $from_date = $request->from_date . " 00:00:00";
        $to_date   = $request->to_date . " 23:59:59";
        $results   = Country::withCount([
            'gemsLike'    => function ($q) use ($from_date, $to_date) {
                $q->whereBetween('gems_likeunlikes.created_at', [$from_date, $to_date]);
            },
            'gemsComment' => function ($q) use ($from_date, $to_date) {
                $q->whereBetween('gems_comments.created_at', [$from_date, $to_date]);
            },
            'gemsShare'   => function ($q) use ($from_date, $to_date) {
                $q->whereBetween('share_gems_groups.created_at', [$from_date, $to_date]);
            }])
            ->orderBy(DB::raw("gems_like_count + gems_comment_count + gems_share_count"), 'DESC')
            ->orderBy('name', 'ASC')
            ->get();

        return response()->json([
            'message' => $results,
            'success' => true,
        ], 200);
    }

    public function adminEngagementGenderWise(Request $request)
    {
        $from_date = $request->from_date . " 00:00:00";
        $to_date   = $request->to_date . " 23:59:59";
        DB::select(DB::raw("SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))"));
        $results = DB::table('gems_likeunlikes')
            ->leftJoin('user_details', 'gems_likeunlikes.user_id', '=', 'user_details.user_id')
            ->where('gems_likeunlikes.action', '=', '1')
            ->whereBetween('gems_likeunlikes.created_at', [$from_date, $to_date])
            ->groupBy('user_details.gender')
            ->select('user_details.gender', DB::raw("count(*) as total"), DB::raw("round(count(*) *100 / (select count(*) from gems_likeunlikes where action = 1 and created_at between '$from_date' and '$to_date')) as percent"))
            ->get();

        return response()->json([
            'message' => $results,
            'success' => true,
        ], 200);
    }

    public function activeUser(Request $request)
    {
        $from_date = $request->from_date . " 00:00:00";
        $to_date   = $request->to_date . " 23:59:59";
        try {
            $results = UserDetail::withCount([
                'total_like'    => function ($q) use ($from_date, $to_date) {
                    $q->whereBetween('created_at', [$from_date, $to_date]);
                },
                'total_comment' => function ($q) use ($from_date, $to_date) {
                    $q->whereBetween('created_at', [$from_date, $to_date]);
                },
                'total_view'    => function ($q) use ($from_date, $to_date) {
                    $q->whereBetween('created_at', [$from_date, $to_date]);
                },
                'total_share'   => function ($q) use ($from_date, $to_date) {
                    $q->whereBetween('created_at', [$from_date, $to_date]);
                },
                'saved_lists'   => function ($q) use ($from_date, $to_date) {
                    $q->whereBetween('created_at', [$from_date, $to_date]);
                }]);

            $results = $results->orderBy(DB::raw("total_like_count + total_comment_count + total_share_count"), 'DESC')
                ->take(6)
                ->get();

        } catch (\Exception $e) {
            $allErrors = $e->getMessage();
            return response()->json([
                'message' => $allErrors,
                'success' => false,
            ], 400);
        }

        return response()->json([
            'message' => $results,
            'success' => true,
        ], 200);
    }

    public function totalRecords(Request $request)
    {
        $from_date = $request->from_date . " 00:00:00";
        $to_date   = $request->to_date . " 23:59:59";

        $user     = User::where('user_type', '=', '1')->where('active_status', '=', '1')->whereBetween('created_at', [$from_date, $to_date])->count();
        $provider = User::where('user_type', '=', '2')->where('active_status', '=', '1')->whereBetween('created_at', [$from_date, $to_date])->count();

        $claim_gem      = GemsProviderRelation::whereBetween('created_at', [$from_date, $to_date])->count();
        $claim_pending  = GemsProviderRelation::where('action', '=', '0')->whereBetween('created_at', [$from_date, $to_date])->count();
        $claim_approved = GemsProviderRelation::where('action', '=', '1')->whereBetween('created_at', [$from_date, $to_date])->count();
        $claim_rejected = GemsProviderRelation::where('action', '=', '2')->whereBetween('created_at', [$from_date, $to_date])->count();

        $results['user_count']     = $user;
        $results['provider_count'] = $provider;
        $results['sales_count']    = '0';
        $results['sales_currency'] = 'Euro';
        $results['total_claim']    = $claim_gem;
        $results['claim_pending']  = $claim_pending;
        $results['claim_approved'] = $claim_approved;
        $results['claim_rejected'] = $claim_rejected;

        return response()->json([
            'message' => $results,
            'success' => true,
        ], 200);
    }
}
