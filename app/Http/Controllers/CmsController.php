<?php

namespace App\Http\Controllers;

use App\Cms;
use Illuminate\Http\Request;

class CmsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function cmsList()
    {
        $models = Cms::get();
        return response()->json($models);
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'title'             => 'required',
            'short_description' => 'required',
            'description'       => 'required',
        ]);

        try {

            $models                    = new Cms;
            $models->title             = $request->title;
            $models->slug              = preg_replace("![^a-z0-9]+!i", "-", strtolower($models->title));
            $models->short_description = $request->short_description;
            $models->description       = $request->description;
            $models->status            = $request->status;

            $models->save();

            if ($request->hasFile('image')) {

                $file   = $request->file('image');
                $cms_id = $models->id;

                $fileCount = $this->moveImage($file, $cms_id);
            }
            return response()->json([
                'message' => 'Record added successfully',
                'success' => true,
            ], 200);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $models = Cms::find($id);
            if ($request->title != '') {
                $models->title = $request->title;
            }

            if ($request->short_description != '') {
                $models->short_description = $request->short_description;
            }

            if ($request->description != '') {
                $models->description = $request->description;
            }
            if ($request->status != '') {
                $models->status = $request->status;
            }

            if ($request->hasFile('image')) {

                $file   = $request->file('image');
                $cms_id = $models->id;

                $fileCount = $this->moveImage($file, $cms_id);
            }

            $models->save();
            return response()->json([
                'message' => 'Record updated successfully',
                'user'    => $models,
                'success' => true,
            ], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'success' => false], 400);
        }
    }

    protected function moveImage($file, $cms_id)
    {
        $fileCount = count((array) $file);
        try {

            $notMoveFileArr   = array();
            $originalFileName = $file->getClientOriginalName();
            $filteredOrgName  = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $originalFileName);
            $extension        = strtolower($file->getClientOriginalExtension());
            $fileName         = date('YmdHmsu') . '-' . $filteredOrgName;
            $fileSize         = $file->getSize();
            $fileSize         = number_format($fileSize / 1048576, 2);
            $destinationPath  = "upload/cms/";
            $fullFilePath     = $destinationPath . $fileName;
            if ($file->move($destinationPath, $fileName)) {
                if ($fileSize > 6) {
                    $oldFullFilePath = $fullFilePath;
                    $fileName        = date('YmdHmsu') . '-converted-' . $filteredOrgName;
                    $fullFilePath    = $destinationPath . $fileName;
                    if ($extension == 'pdf') {

                        shell_exec('ps2pdf -dPDFSETTINGS=/default ' . $oldFullFilePath . '  ' . $fullFilePath . '');

                    } else if ($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'tiff') {

                        shell_exec('convert -quality 50% ' . $oldFullFilePath . '  ' . $fullFilePath . '');

                    } /*else{
                    rename( $orgFullFilePath, $fullFilePath);
                    }*/

                    if (File::exists($oldFullFilePath)) {
                        File::delete($oldFullFilePath);
                    }
                }
                $cms        = Cms::find($cms_id);
                $cms->image = $fileName;
                $cms->save();

                //$this->compress($fullFilePath,$destinationPath."compress/".$fileName,60);
            } else {
                $notMoveFileArr[] = $originalFileName;
            }
            return $notMoveFileArr;
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    public function statusUpdate(Request $request, $id)
    {

        //return response()->json($request);
        \DB::beginTransaction();
        try {
            $model = Cms::find($id);
            if (!empty($model)) {

                if ($model->status != (int) $request->input('status')) {
                    $model->status = $request->input('status');
                }

                if ($model->is_approved != (int) $request->input('is_approved')) {
                    $model->is_approved = $request->input('is_approved');
                }

                $model->save();

                \DB::commit();
                $message       = 'Updated successfully!';
                $success       = true;
                $audit_message = 'STATUS_UPDATE_CMS_SUCCESS';

            } else {
                $message       = 'record not exists!';
                $success       = false;
                $audit_message = 'STATUS_UPDATE_CMS_FAIL';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $audit_message = 'STATUS_UPDATE_GEMS_FAIL';
        }

        $this->insertAuditTrail('STATUS_UPDATE_CMS', $audit_message);
        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 200);
    }

    public function cmsDetails($id)
    {
        try {
        $models = Cms::find($id);
        return response()->json([
                'message' => $models,
                'success' => true,
            ], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    //
}
