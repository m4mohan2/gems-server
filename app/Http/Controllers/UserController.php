<?php

namespace App\Http\Controllers;

//import auth facades
use App\CompanyDetail;
use App\Gems;
use App\GemsLikeunlike;
use App\GemsProviderRelation;
use App\GemsSaveList;
use App\ShareGemsGroup;
use App\User;
use App\UserDetail;
use App\UserFriend;
use App\UserPrivateInfo;
//use App\UserDevice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $pageSize     = $request->input('pageSize') ? $request->input('pageSize') : '10';
        $searchString = $request->get('searchString');
        $user_type    = $request->get('user_type');
        $export       = $request->get('export');

        /*$query = User::with([
        'userDetail',
        ])
        ->where('user_type', '=', $user_type)
        ->exclude(['token', 'active_token', 'password_reset_token']);*/

        $query = DB::table('users')
            ->where('user_type', '=', $user_type)
            ->leftJoin('user_details', 'user_details.user_id', '=', 'users.id')
            ->leftJoin('countries', 'user_details.country', '=', 'countries.id')
            ->leftJoin('states', 'states.id', '=', 'user_details.state')
            ->leftJoin('cities', 'cities.id', '=', 'user_details.city');

        if ($searchString != '') {

            $query->where(function ($q) use ($searchString) {
                $q->where('users.email', 'like', '%' . $searchString . '%')
                    ->orWhere('user_details.first_name', 'like', '%' . $searchString . '%')
                    ->orWhere('user_details.last_name', 'like', '%' . $searchString . '%');

            });
        }

        $query->select(
            'users.id',
            'users.user_type',
            'users.email',
            'users.is_influencer',
            'users.active_status',
            'users.is_verified',
            'user_details.first_name',
            'user_details.last_name',
            'user_details.gender',
            'user_details.dob',
            'user_details.profile_pic',
            'user_details.address',
            'user_details.phone',
            'user_details.biography',
            'user_details.zip',
            'user_details.country',
            'user_details.state',
            'user_details.city',
            DB::raw('countries.name country_name'),
            DB::raw('states.name state_name'),
            DB::raw('cities.name city_name')
        );

        $query->orderBy('users.id', 'DESC');

        if ($export) {
            $data = $query->get();
        } else {
            $data = $query->paginate($pageSize);
        }

        return response()->json([
            'data'    => $data,
            'resCode' => 200,
            'success' => true,
        ], 200);
    }

    /**
     * User lists search by email.
     *
     * @return void
     */
    public function userLists(Request $request)
    {
        $email  = $request->get('email');
        $select = User::with([
            'userDetail',
        ])
            ->where('email', 'like', '%' . $email . '%')
            ->where('user_type', '=', '1')
            ->exclude(['token', 'active_token', 'password_reset_token'])
            ->orderBy('email', 'ASC');
        $data = $select->get();

        return response()->json([
            'data'    => $data,
            'resCode' => 200,
            'success' => true,
        ], 200);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function auditUserList(Request $request)
    {
        $email  = $request->get('email');
        $select = User::with([
            'userDetail',
        ])
            ->where('email', 'like', '%' . $email . '%')
        //->where('user_type', '=', '1')
            ->exclude(['token', 'active_token', 'password_reset_token']);
        $data = $select->get();

        return response()->json([
            'data'    => $data,
            'resCode' => 200,
            'success' => true,
        ], 200);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function providers(Request $request)
    {
        $pageSize     = $request->input('pageSize') ? $request->input('pageSize') : '10';
        $searchString = $request->get('searchString');
        $export       = $request->get('export');

        $select = User::with([
            'userDetail',
            'companyDetail',
            'companyDocuments',
            'userCountry',
            'userState',
            'userCity',
            'companyCountry',
            'companyState',
            'companyCity',
            'gemDetail',
            'gemStatus',
        ])
            ->where('user_type', '=', '2')
            ->exclude(['token', 'active_token', 'password_reset_token']);

        $select->orderBy('id', 'DESC');

        if ($export) {
            $data = $select->get();
        } else {
            $data = $select->paginate($pageSize);
        }

        return response()->json([
            'data'    => $data,
            'success' => true,
        ], 200);
    }

    /**
     * update user credentials.
     *
     * @param  Request  $request
     * @return Response
     */

    public function updateUser(Request $request)
    {
        $user_id = $request->input('user_id');
        if ($user_id == 0) {
            $user_id = Auth::user()->id;
        }

        // $this->validate($request, [
        //   'first_name' => 'required',
        // ]);
        try {
            $user             = UserDetail::where('user_id', '=', $user_id)->first();
            $user->first_name = $request->input('first_name');
            $user->last_name  = $request->input('last_name');
            $user->gender     = $request->input('gender');
            $user->dob        = $request->input('dob');
            $user->biography  = $request->input('biography');
            $user->address    = $request->input('address');
            $user->country    = $request->input('country');
            $user->state      = $request->input('state');
            $user->city       = $request->input('city');
            $user->phone      = $request->input('phone');
            $user->zip        = $request->input('zip');
            if ($user->save()) {
                //$device_ids = UserDevice::select('device_id')->whereIn('user_id',[2,6])->get()->toArray();
                //dd($device_ids);

                //$push_message = 'User Update Successfully:-'.$user->first_name;
                //$reg_ids = array('fuvtCTEBPk4:APA91bHOlf630iHrMMDNImu-Pq9kpsfTW4KC_uSiAW99rz01ort_9m8dJy7WHDZfTPAe53MYQfGG7hfqgirwomntsgtR0mRWGgu98nJhDpdIxDs5fyVD3IRvZWXUtXGlhacPr9xTv1oz');
                // $registration_ids = '98754125478965412';
                //$this->fcm($push_message,$reg_ids);
                // $this->fcm_android($reg_ids,$registration_ids);

                if ($request->hasFile('profile_pic')) {

                    $file    = $request->file('profile_pic');
                    $user_id = $user->id;

                    $fileCount = $this->moveLogo($file, $user_id);
                }
                $user->profile_pic = $user->profile_pic;
                return response()->json([
                    'message' => 'User Profile updated successfully',
                    'user'    => $user,
                    'success' => true,
                ], 200);
                $appmessage = Config::get('messages.audit.update_profile');
                $platform   = $request->input('platform', 'Web');
                $this->insertAuditTrail('USER', 'PROFILE_UPDATE_SUCCESS', $appmessage, $platform, $model->email);
            } else {
                $this->insertAuditTrail('USER', 'PROFILE_UPDATE_FAIL');
                return response()->json([
                    'message' => 'User Profile update Failed!',
                    'success' => false,
                ], 201);
            }
        } catch (\Exception $e) {
            $this->insertAuditTrail('USER', 'PROFILE_UPDATE_FAILE');
            return response()->json(['message' => $e->getMessage(), 'success' => false], 400);
        }
    }

    /**
     * update provider credentials.
     *
     * @param  Request  $request
     * @return Response
     */

    public function updateProvider(Request $request, $id)
    {
        /*return response()->json([
        'data' => $request->hasFile('profile_pic'),
        'success' => false,
        ], 409);*/

        \DB::beginTransaction();
        try {

            $model = User::find($id);
            if (!empty($model)) {

                $userD = UserDetail::where('user_id', '=', $id)->first();

                if (!empty($userD)) {

                    $userD->first_name = $request->input('first_name');
                    $userD->last_name  = $request->input('last_name');
                    $userD->phone      = $request->input('phone');
                    $userD->address    = $request->input('address');
                    $userD->address2   = $request->input('address2');
                    $userD->country    = $request->input('personalCountry');
                    $userD->state      = $request->input('personalState');
                    $userD->city       = $request->input('personalCity');
                    $userD->zip        = $request->input('personalZip');

                    $userD->save();

                    if ($request->hasFile('profile_pic')) {

                        $file = $request->file('profile_pic');

                        $fileCount = $this->moveLogo($file, $id);
                    }
                }

                $companyModel = CompanyDetail::where('user_id', '=', $id)->first();

                if (!empty($companyModel)) {

                    $companyModel->company_name  = $request->input('company_name');
                    $companyModel->company_phone = $request->input('company_phone');
                    $companyModel->addr1         = $request->input('addr1');
                    $companyModel->addr2         = $request->input('addr2');
                    $companyModel->country       = $request->input('companyCountry');
                    $companyModel->state         = $request->input('companyState');
                    $companyModel->city          = $request->input('companyCity');
                    $companyModel->zip           = $request->input('companyZip');

                    $companyModel->save();
                }

                $relationModel = GemsProviderRelation::where('user_id', '=', $id)
                    ->where('gems_id', $request->input('hiddenGemId'))
                    ->first();
                //return response()->json(['message' => $relationModel], 201);

                if (!empty($relationModel)) {

                    if ($relationModel->action != (int) $request->input('claim')) {
                        $relationModel->action = $request->input('claim');
                    }

                    $relationModel->save();
                }

                \DB::commit();
                $user          = $userD;
                $message       = 'Provider Profile updated successfully.';
                $success       = true;
                $resCode       = 201;
                $audit_message = 'PROVIDER_UPDATE_SUCCESS';
                $audit_on      = $model->email;

            } else {
                $user          = '';
                $message       = 'Record not exists!';
                $success       = true;
                $resCode       = 400;
                $audit_message = 'PROVIDER_UPDATE_FAIL';
                $audit_on      = '';
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $user          = '';
            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $resCode       = 400;
            $audit_message = 'PROVIDER_UPDATE_FAIL';
            $audit_on      = '';
        }
        $appmessage = Config::get('messages.audit.update_provider');
        $platform   = $request->input('platform', 'Web');
        $this->insertAuditTrail('USER', $audit_message, $appmessage, $platform, $audit_on);
        //Return message
        return response()->json([
            'message' => $message,
            'user'    => $user,
            'success' => $success,
        ], $resCode);
    }

    public function providerInfo(Request $request)
    {
        $user_id = $request->input('user_id');
        if ($user_id == '') {
            $user_id = Auth::user()->id;
        }
        try {
            $user = User::with([
                'userDetail',
                'companyDetail',
                'privateData',
                'userCountry',
                'userState',
                'userCity',
                'companyCountry',
                'companyState',
                'companyCity',
            ])->where('id', '=', $user_id)->first();

            //$user['total_created_gems']   = Gems::where('created_by', $user_id)->count();
            //$user['total_favourite_gems'] = GemsSaveList::where('user_id', $user_id)->count();
            //$user['total_like_gems']      = GemsLikeunlike::where('user_id', $user_id)->where('action', '1')->count();
            //$user['total_share_gems']     = ShareGemsGroup::where('share_by', $user_id)->count();
            /*$sql                          = "select user_id, friends.status, user_details.first_name,user_details.last_name,user_details.gender,user_details.dob,user_details.profile_pic
            from (
            select sender as friend, status
            from user_friends
            where receiver = $user_id
            union
            select receiver as friend, status
            from user_friends
            where sender = $user_id
            ) as friends
            inner join user_details
            on user_details.user_id = friends.friend where friends.status = 1";

            $friend_info = \DB::select(\DB::raw($sql));

            $user['total_friends']                  = count($friend_info);
            $user['pending_sent_friend_request']    = UserFriend::where('sender', '=', $user_id)->where('status', '0')->count();
            $user['pending_receive_friend_request'] = UserFriend::where('receiver', '=', $user_id)->where('status', '0')->count();
            $user['total_sent_friend_request']      = UserFriend::where('sender', '=', $user_id)->count();
            $user['total_receive_friend_request']   = UserFriend::where('receiver', '=', $user_id)->count();*/

            if ($user) {
                //$this->insertAuditTrail('VIEW_PROFILE', 'PROFILE_VIEW_SUCCESS', $user_id);
                return response()->json(['user' => $user, 'message' => 'Provider details', 'success' => true], 200);
            } else {
                //$this->insertAuditTrail('VIEW_PROFILE', 'INVALID_USER', $user_id);
                return response()->json(['message' => 'User not found', 'success' => false], 404);
            }
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }

    }

    /**
     * Claim list.
     *
     * @param  Request  $request
     * @return Response
     */
    public function claimList(Request $request)
    {
        $action = $request->get('action');
        \DB::beginTransaction();
        try {
            $pageSize = $request->input('pageSize') ? $request->input('pageSize') : '10';
            $models   = GemsProviderRelation::with(['gems', 'provider', 'companyDetail', 'companyDocuments']);

            if ($action != '') {
                $models->where('action', '=', $action);
            }
            $result = $models->paginate($pageSize);

            $message = $result;
            $success = true;
            $resCode = 200;

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
            $resCode = 400;
        }

        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    public function changeClaimStatus(Request $request, $id)
    {
        try {
            $model         = GemsProviderRelation::find($id);
            $model->action = $request->input('action');
            $model->save();

            $message = 'Successfully updated';
            $success = true;
            $resCode = 200;
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
            $resCode = 400;
        }

        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    /**
     * update user credentials by user id.
     *
     * @param  Request  $request
     * @return Response
     */

    public function updateSelectedUser(Request $request, $user_id)
    {

        // $this->validate($request, [
        //   'first_name' => 'required',
        // ]);

        // return response()->json([
        //             'message' => $request->first_name,
        //             'user'    => $request->last_name."--++--",
        //             'success' => true,
        //         ], 200);

        try {
            $user             = UserDetail::where('user_id', '=', $user_id)->first();
            $user->first_name = $request->input('first_name');
            $user->last_name  = $request->input('last_name');
            $user->gender     = $request->input('gender');
            $user->address    = $request->input('address');
            $user->biography  = $request->input('biography');
            $user->dob        = $request->input('dob');
            $user->phone      = $request->input('phone');
            $user->country    = $request->input('country');
            $user->state      = $request->input('state');
            $user->city       = $request->input('city');
            $user->zip        = $request->input('zip');
            if ($user->save()) {
                if ($request->hasFile('profile_pic')) {
                    // return response()->json([
                    //     'message' => "entry",
                    //     'success' => true,
                    // ], 200);

                    $file    = $request->file('profile_pic');
                    $user_id = $user->id;

                    $fileCount = $this->moveLogo($file, $user_id);
                }
                $user->profile_pic = $user->profile_pic;
                return response()->json([
                    'message' => 'User Profile updated successfully',
                    'user'    => $user,
                    'success' => true,
                ], 200);
                $appmessage = Config::get('messages.audit.update_profile');
                $platform   = $request->input('platform', 'Web');
                $this->insertAuditTrail('UPDATE_PROFILE', 'PROFILE_UPDATE_SUCCESS');
            } else {
                $this->insertAuditTrail('UPDATE_PROFILE', 'PROFILE_UPDATE_FAILE', $appmessage, $platform);
                return response()->json([
                    'message' => 'User Profile update Failed!',
                    'success' => false,
                ], 201);
            }
        } catch (\Exception $e) {
            $this->insertAuditTrail('UPDATE_PROFILE', 'PROFILE_UPDATE_FAILE');
            return response()->json(['message' => $e->getMessage(), 'success' => false], 400);
        }
    }

    /**
     * Status update.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function statusUpdate(Request $request, $id)
    {
        \DB::beginTransaction();
        try {
            $model = User::find($id);
            if (!empty($model)) {

                if ($model->active_status != (int) $request->input('active_status')) {
                    $model->active_status = $request->input('active_status');
                }

                if ($model->save()) {

                    \DB::commit();
                    $message       = 'Updated successfully!';
                    $success       = true;
                    $audit_message = 'STATUS_UPDATE_USER_SUCCESS';
                    $audit_on      = $model->email;
                } else {
                    $message       = 'Error Please try again!';
                    $success       = false;
                    $audit_message = 'STATUS_UPDATE_USER_FAIL';
                    $audit_on      = '';
                }

            } else {
                $message       = 'record not exists!';
                $success       = true;
                $audit_message = 'STATUS_UPDATE_USER_FAIL';
                $audit_on      = '';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $audit_message = 'STATUS_UPDATE_USER_FAIL';
            $audit_on      = '';
        }
        $appmessage = Config::get('messages.audit.status_update_user');
        $platform   = $request->input('platform', 'Web');
        $this->insertAuditTrail('USER', $audit_message, $appmessage, $platform, $audit_on);
        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 200);
    }

    /**
     * Status update.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function influenceUpdate(Request $request, $id)
    {

        \DB::beginTransaction();
        try {
            $model = User::find($id);
            if (!empty($model)) {

                if ($model->is_influencer != (int) $request->input('is_influencer')) {
                    $model->is_influencer = $request->input('is_influencer');
                }

                if ($model->save()) {

                    \DB::commit();
                    $message       = 'Updated successfully!';
                    $success       = true;
                    $audit_message = 'INFLUENCER_UPDATE_USER_FAIL';
                } else {
                    $message = 'Error Please try again!';
                    $success = false;
                }

            } else {
                $message       = 'record not exists!';
                $success       = true;
                $audit_message = 'INFLUENCER_UPDATE_USER_FAIL';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $audit_message = 'INFLUENCER_UPDATE_USER_FAIL';
        }

        $appmessage = Config::get('messages.audit.influencer_update');
        $platform   = $request->input('platform', 'Web');
        $this->insertAuditTrail('USER', $audit_message, $appmessage, $platform);
        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 200);
    }

    /**
     * update user password.
     *
     * @param  Request  $request
     * @return Response
     */

    public function updatePassword(Request $request)
    {
        $user_id = Auth::user()->id;
        $this->validate($request, [
            'old_password'     => 'required',
            'new_password'     => 'required|same:confirm_password',
            'confirm_password' => 'required',
        ]);
        try {
            $user = User::where('id', '=', $user_id)->first();

            $new_password     = $request->input('new_password');
            $old_password     = $request->input('old_password');
            $new_has_password = app('hash')->make($new_password);
            $old_has_password = app('hash')->make($old_password);

            if (Hash::check($old_password, $user->password)) {
                $user->password = $new_has_password;
                $user->save();

                $appmessage = Config::get('messages.audit.update_password');
                $platform   = $request->input('platform', 'Web');

                $this->insertAuditTrail('USER', 'PASSWORD_UPDATE_SUCCESS', $appmessage, $platform);
                return response()->json(['message' => 'Password changed successfully', 'success' => true], 200);
            } else {
                $this->insertAuditTrail('USER', 'PASSWORD_UPDATE_FAIL');
                return response()->json(['old_password' => 'Old Password does not match', 'success' => false], 400);
            }

        } catch (\Exception $e) {
            $this->insertAuditTrail('USER', 'PASSWORD_UPDATE_FAIL');
            return response()->json(['message' => $e->getMessage(), 'success' => false], 400);
        }
    }

    /**
     * cjeck auth is enabled.
     *
     * @param  Request  $request
     * @return Response
     */

    public function authActiveStatus(Request $request)
    {
        $user = Auth::user();

        return response()->json(['valid' => $user->active_status === 1 ? true : false, 'success' => true], 200);
    }

    /**
     * User Details.
     *
     * @param  Request  $request
     * @return Response
     */
    public function userInfo(Request $request)
    {
        $user_id   = $request->input('user_id');
        $user_info = UserDetail::where('user_id', $user_id)->first();

        return response()->json([
            'data'    => $user_info,
            'resCode' => 200,
            'success' => true,
        ], 200);
    }

    /**
     * User Details.
     *
     * @param  Request  $request
     * @return Response
     */

    public function profileDetails(Request $request)
    {
        $user_id = $request->input('user_id');
        if ($user_id == '') {
            $user_id = Auth::user()->id;
        }
        try {
            // $user = \DB::table('users')
            //         ->leftJoin('user_details', 'users.id', '=', 'user_details.user_id')
            //         ->where('user_details.user_id','=',$user_id)
            //         ->where('users.is_verified','=',1);
            // $user = $user->select('user_id','users.email','user_details.first_name','user_details.last_name','user_details.gender','user_details.dob','user_details.address','user_details.profile_pic');
            // $user = $user->first();

            $user = User::with([
                'userDetail',
                'companyDetail',
                'privateData',
                'userCountry',
                'userState',
                'userCity'
            ])->where('id', '=', $user_id)->first();
            $user['total_created_gems']   = Gems::where('created_by', $user_id)->count();
            $user['total_favourite_gems'] = GemsSaveList::where('user_id', $user_id)->count();
            $user['total_like_gems']      = GemsLikeunlike::where('user_id', $user_id)->where('action', '1')->count();
            $user['total_share_gems']     = ShareGemsGroup::where('share_by', $user_id)->count();
            $sql                          = "select user_id, friends.status, user_details.first_name,user_details.last_name,user_details.gender,user_details.dob,user_details.profile_pic
        from (
          select sender as friend, status
            from user_friends
           where receiver = $user_id
          union
          select receiver as friend, status
            from user_friends
           where sender = $user_id
          ) as friends
        inner join user_details
        on user_details.user_id = friends.friend where friends.status = 1";

            $friend_info = \DB::select(\DB::raw($sql));

            $user['total_friends']                  = count($friend_info);
            $user['pending_sent_friend_request']    = UserFriend::where('sender', '=', $user_id)->where('status', '0')->count();
            $user['pending_receive_friend_request'] = UserFriend::where('receiver', '=', $user_id)->where('status', '0')->count();
            $user['total_sent_friend_request']      = UserFriend::where('sender', '=', $user_id)->count();
            $user['total_receive_friend_request']   = UserFriend::where('receiver', '=', $user_id)->count();

            $user['active_coupon'] = \DB::table('coupons')
                ->join('coupon_users', 'coupons.id', '=', 'coupon_users.coupon_id')
                ->where('coupon_users.user_id', $user_id)
                ->whereRaw('? between start_date and end_date', [date('Y-m-d H:i:s')])
                ->where('coupons.active_status', '=', '1')
                ->count();
            $user['assign_gem'] = Gems::where('previous_owner',$user_id)->count();

            $friendArr = [];
            if (count($friend_info) > 0) {
                foreach ($friend_info as $key => $friend) {
                    $friendArr[] = $friend->user_id;
                }
            }

            $user['activity_count'] = DB::table('audit_trails')
                ->leftJoin('users', 'audit_trails.action_by', '=', 'users.id')
                ->leftJoin('user_details', 'user_details.user_id', '=', 'users.id')
                ->select(
                    'audit_trails.message',
                    'audit_trails.action_on',
                    'audit_trails.created_at',
                    'user_details.first_name',
                    'user_details.last_name',
                    'user_details.profile_pic',
                    'users.email')
                ->where('audit_trails.action_by', $user_id)
                ->count();

            if ($user) {
                $appmessage = Config::get('messages.audit.profile_view');
                $platform   = $request->input('platform', 'Web');
                $this->insertAuditTrail('USER', 'PROFILE_VIEW_SUCCESS', $appmessage, $platform, Auth::user()->email);
                return response()->json(['user' => $user, 'message' => 'User details', 'success' => true], 200);
            } else {
                $this->insertAuditTrail('USER', 'INVALID_USER', '', '', Auth::user()->email);
                return response()->json(['message' => 'User not found', 'success' => false], 404);
            }
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }

    }

    /**
     * Send friend request.
     *
     * @param  Request  $request
     * @return Response
     */

    public function sendFriendRequest(Request $request)
    {
        $user_id     = Auth::user()->id;
        $receiver_id = $request->friend_id;
        if ($user_id == $receiver_id) {
            return response()->json(['message' => 'You cannot sent request your self', 'success' => false], 201);
        }
        try {
            $friend_info = UserFriend::where('sender', '=', $user_id)->where('receiver', '=', $receiver_id)->first();
            //dd($friend_info);
            if (!$friend_info) {
                $friend_info            = new UserFriend;
                $friend_info->sender    = $user_id;
                $friend_info->receiver  = $receiver_id;
                $friend_info->action_by = $user_id;
            }
            $friend_info->status = '0';
            $friend_info->save();
            $receiver_device_token = User::where('id', $receiver_id)->select('device_token', 'email')->first();
            $sender_user_info      = UserDetail::where('user_id', $user_id)->select('first_name', 'last_name')->first();
            if ($receiver_device_token->device_token != '') {
                $title        = "Someone wants to be your friend";
                $push_message = $sender_user_info->first_name . " " . $sender_user_info->last_name . ' has sent you a friend request';
                $this->fcm_send($receiver_device_token->device_token, $push_message, $title);
            }
            $appmessage = Config::get('messages.audit.send_friend_request');
            $platform   = $request->input('platform', 'Web');
            $this->insertAuditTrail('FRIEND', 'SEND_FRIEND_REQUEST_SUCCESS', $appmessage, $platform, $receiver_device_token->email);
            return response()->json(['message' => 'Friend request send successfully', 'success' => true], 200);
        } catch (\Exception $e) {

            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    /**
     * pending friend request.
     *
     * @param  Request  $request
     * @return Response
     */
    public function pendingFriendRequest(Request $request)
    {
        $user_id = $request->input('user_id');
        if ($user_id == '') {
            $user_id = Auth::user()->id;
        }
        try {
            $friend_info = \DB::table('user_friends')
                ->leftJoin('user_details', 'user_friends.sender', '=', 'user_details.user_id')
                ->leftJoin('countries', 'countries.id', '=', 'user_details.country')
                ->leftJoin('states', 'states.id', '=', 'user_details.state')
                ->leftJoin('cities', 'cities.id', '=', 'user_details.city')
                ->select('user_id', 'user_details.first_name', 'user_details.last_name', 'user_details.gender', 'user_details.dob', 'user_details.profile_pic','countries.name as country', 'states.name as state','cities.name as city')
                ->where('user_friends.receiver', '=', $user_id)->where('user_friends.status', '=', '0')->get();
            $appmessage = Config::get('messages.audit.pending_friend_view');
            $platform   = $request->input('platform', 'Web');
            $this->insertAuditTrail('FRIEND', 'VIEW_PENDING_FRIEND_REQUEST', $appmessage, $platform);
            return response()->json(['message' => $friend_info, 'success' => true], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    /**
     * pending friend request with pagination.
     *
     * @param  Request  $request
     * @return Response
     */
    public function pendingFriendRequestPagination(Request $request)
    {
        $logged_user_id = Auth::user()->id;
        $pageSize       = $request->input('pageSize') ? $request->input('pageSize') : '20';
        $user_id        = $request->input('user_id');
        if ($user_id == '') {
            $user_id = Auth::user()->id;
        }
        try {
            $friend_info = \DB::table('user_friends')
                ->leftJoin('user_details', 'user_friends.sender', '=', 'user_details.user_id')
                ->leftJoin('countries', 'countries.id', '=', 'user_details.country')
                ->leftJoin('states', 'states.id', '=', 'user_details.state')
                ->leftJoin('cities', 'cities.id', '=', 'user_details.city')
                ->select('user_id', 'user_details.first_name', 'user_details.last_name', 'user_details.gender', 'user_details.dob', 'user_details.profile_pic','countries.name as country', 'states.name as state','cities.name as city')
                ->where('user_friends.receiver', '=', $user_id)->where('user_friends.status', '=', '0')->paginate($pageSize);
            if (count($friend_info) > 0) {
                foreach ($friend_info as $key => $value) {
                    $friend_user                      = $value->user_id;
                    $mutualSql                        = "select count(*) as cnt from ( select friend from ( select $friend_user src, sender friend from user_friends where receiver = $friend_user union all select $friend_user, receiver from user_friends where sender = $friend_user union all select $logged_user_id, sender from user_friends where receiver = $logged_user_id union all select $logged_user_id, receiver from user_friends where sender = $logged_user_id ) t group by friend having count(distinct src) > 1 ) AS T";
                    $mutual_friend_count              = \DB::select(\DB::raw($mutualSql));
                    $friend_info[$key]->mutual_friend = $mutual_friend_count[0]->cnt;
                }
            }
            $appmessage = Config::get('messages.audit.pending_friend_view');
            $this->insertAuditTrail('FRIEND', 'VIEW_PENDING_FRIEND_REQUEST', $appmessage);
            return response()->json(['message' => $friend_info, 'success' => true], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    /**
     * Sent friend request.
     *
     * @param  Request  $request
     * @return Response
     */
    public function sentFriendRequestPagination(Request $request)
    {
        $logged_user_id = Auth::user()->id;
        $pageSize       = $request->input('pageSize') ? $request->input('pageSize') : '20';
        $user_id        = $request->input('user_id');
        if ($user_id == '') {
            $user_id = Auth::user()->id;
        }
        try {
            $friend_info = \DB::table('user_friends')
                ->leftJoin('user_details', 'user_friends.receiver', '=', 'user_details.user_id')
                ->leftJoin('countries', 'countries.id', '=', 'user_details.country')
                ->leftJoin('states', 'states.id', '=', 'user_details.state')
                ->leftJoin('cities', 'cities.id', '=', 'user_details.city')
                ->leftJoin('countries', 'countries.id', '=', 'user_details.country')
                ->leftJoin('states', 'states.id', '=', 'user_details.state')
                ->leftJoin('cities', 'cities.id', '=', 'user_details.city')
                ->select('user_id', 'user_details.first_name', 'user_details.last_name', 'user_details.gender', 'user_details.dob', 'user_details.profile_pic','countries.name as country', 'states.name as state','cities.name as city')
                ->where('user_friends.sender', '=', $user_id)->where('user_friends.status', '=', '0')->paginate($pageSize);

            if (count($friend_info) > 0) {
                foreach ($friend_info as $key => $value) {
                    $friend_user                      = $value->user_id;
                    $mutualSql                        = "select count(*) as cnt from ( select friend from ( select $friend_user src, sender friend from user_friends where receiver = $friend_user union all select $friend_user, receiver from user_friends where sender = $friend_user union all select $logged_user_id, sender from user_friends where receiver = $logged_user_id union all select $logged_user_id, receiver from user_friends where sender = $logged_user_id ) t group by friend having count(distinct src) > 1 ) AS T";
                    $mutual_friend_count              = \DB::select(\DB::raw($mutualSql));
                    $friend_info[$key]->mutual_friend = $mutual_friend_count[0]->cnt;
                }
            }
            $appmessage = Config::get('messages.audit.sent_friend_view');
            $this->insertAuditTrail('FRIEND', 'VIEW_SENT_FRIEND_REQUEST');
            return response()->json(['message' => $friend_info, 'success' => true], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    /**
     * Sent friend request with pagination.
     *
     * @param  Request  $request
     * @return Response
     */
    public function sentFriendRequest(Request $request)
    {
        $user_id = $request->input('user_id');
        if ($user_id == '') {
            $user_id = Auth::user()->id;
        }
        try {
            $friend_info = \DB::table('user_friends')
                ->leftJoin('user_details', 'user_friends.receiver', '=', 'user_details.user_id')
                ->leftJoin('countries', 'countries.id', '=', 'user_details.country')
                ->leftJoin('states', 'states.id', '=', 'user_details.state')
                ->leftJoin('cities', 'cities.id', '=', 'user_details.city')
                ->select('user_id', 'user_details.first_name', 'user_details.last_name', 'user_details.gender', 'user_details.dob', 'user_details.profile_pic', 'user_details.gender', 'user_details.dob', 'user_details.profile_pic','countries.name as country', 'states.name as state','cities.name as city')
                ->where('user_friends.sender', '=', $user_id)->where('user_friends.status', '=', '0')->get();
            $appmessage = Config::get('messages.audit.sent_friend_view');
            $this->insertAuditTrail('FRIEND', 'VIEW_SENT_FRIEND_REQUEST', $appmessage);
            return response()->json(['message' => $friend_info, 'success' => true], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    /**
     * Accepted,Rejected,Blocked friend request.
     *
     * @param  Request  $request
     * @return Response
     */

    public function actionFriendRequest(Request $request)
    {
        $user_id   = Auth::user()->id;
        $friend_id = $request->friend_id;
        $action    = $request->action;
        try {
            if ($action == 4) {
                $friend_info = UserFriend::where('receiver', '=', $friend_id)->where('sender', '=', $user_id)->first();
            } else {
                $friend_info = UserFriend::where('receiver', '=', $user_id)->where('sender', '=', $friend_id)->first();

                if (empty($friend_info) && $action == 3) {
                    $friend_info = UserFriend::where('receiver', '=', $friend_id)->where('sender', '=', $user_id)->first();
                }
            }

            $friend_info->status    = $action;
            $friend_info->action_by = $user_id;
            $friend_info->save();
            $action_type = '';
            if ($action == 1) {
                $action_type           = 'accepted';
                $audit_message         = 'ACCEPT_FRIEND_REQUEST';
                $appmessage            = Config::get('messages.audit.accept_friend_request');
                $receiver_device_token = User::where('id', $friend_id)->select('device_token')->first();
                $sender_user_info      = UserDetail::where('user_id', $user_id)->select('first_name', 'last_name')->first();
                if ($receiver_device_token->device_token != '') {
                    $title        = "Friend request accepted";
                    $push_message = $sender_user_info->first_name . " " . $sender_user_info->last_name . ' has accepted your a friend request';
                    $this->fcm_send($receiver_device_token->device_token, $push_message, $title);
                }
            }
            if ($action == 2) {
                $action_type   = 'rejected';
                $audit_message = 'REJECT_FRIEND_REQUEST';
                $appmessage    = Config::get('messages.audit.reject_friend_request');
            }
            if ($action == 3) {
                $action_type   = 'blocked';
                $audit_message = 'BLOCK_FRIEND_REQUEST';
                $appmessage    = Config::get('messages.audit.block_friend_request');
            }
            if ($action == 4) {
                $action_type   = 'cancelled';
                $audit_message = 'CANCEL_FRIEND_REQUEST';
                $appmessage    = Config::get('messages.audit.cancel_friend_request');
            }
            $message = "Friend request " . $action_type . " successfully";

            $this->insertAuditTrail('FRIEND', $audit_message, $appmessage);
            return response()->json(['message' => $message, 'success' => true], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    /**
     * Cancel friend request.
     *
     * @param  Request  $request
     * @return Response
     */
    public function cancelSentFriendRequest(Request $request)
    {
        $user_id   = Auth::user()->id;
        $friend_id = $request->friend_id;
        try {
            $friend_info            = UserFriend::where('sender', '=', $user_id)->where('receiver', '=', $friend_id)->first();
            $friend_info->status    = 4;
            $friend_info->action_by = $user_id;
            $friend_info->save();
            $message = "Pending friend request has been cancelled successfully";

            $appmessage = Config::get('messages.audit.cancel_sent_friend_request');
            $this->insertAuditTrail('ACTION_FRIEND_REQUEST', 'CANCEL_SENT_FRIEND_REQUEST', $appmessage);
            return response()->json(['message' => $message, 'success' => true], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove friend.
     *
     * @param  Request  $request
     * @return Response
     */
    public function removeFriend(Request $request)
    {
        $user_id   = Auth::user()->id;
        $friend_id = $request->friend_id;
        try {
            $friend_info = UserFriend::where('sender', '=', $user_id)->where('receiver', '=', $friend_id)->first();
            if ($friend_info) {
                $friend_info->delete();
                $message      = "Friend removed successfully";
                $success      = true;
                $responseCode = 200;
            }

            $friend_info_sender = UserFriend::where('sender', '=', $friend_id)->where('receiver', '=', $user_id)->first();
            if ($friend_info_sender) {
                $friend_info_sender->delete();
                $message      = "Friend removed successfully";
                $success      = true;
                $responseCode = 200;
            }

        } catch (\Exception $e) {
            $message      = "Friend remove fail";
            $success      = true;
            $responseCode = 400;
        }

        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $responseCode);
    }

    /**
     * Friend list.
     *
     * @param  Request  $request
     * @return Response
     */

    public function friendList(Request $request)
    {
        $logged_user_id = Auth::user()->id;
        $user_id        = $request->input('user_id');
        $page           = $request->input('page') ?: 1;
        $pagesize       = $request->input('pagesize') ?: 20;
        $limit          = $pagesize;
        $offset         = $limit * ($page - 1);
        if ($user_id == '') {
            $user_id = Auth::user()->id;
        }
        try {
            // $friend_info = \DB::table('user_friends')
            //                 ->leftJoin('user_details', 'user_friends.sender', '=', 'user_details.user_id')
            //                 ->select('user_id','user_details.first_name','user_details.last_name','user_details.gender','user_details.dob','user_details.profile_pic')
            //                 //->where('user_friends.receiver','=',$user_id)
            //                 ->where(function ($query) use($user_id) {
            //                   $query->where('user_friends.receiver', '=', $user_id)
            //                         ->where('user_friends.status', '=', 1);
            //                 })->orWhere(function ($query) use($user_id) {
            //                   $query->where('user_friends.sender', '=', $user_id)
            //                         ->where('user_friends.status', '=', 1);
            //                 })->get();

            $sql = "select user_id, friends.status, user_details.first_name,user_details.last_name,user_details.gender,user_details.dob,user_details.profile_pic, users.is_influencer,countries.name as country,states.name as state,cities.name as city
        from (
          select sender as friend, status
            from user_friends
           where receiver = $user_id
          union
          select receiver as friend, status
            from user_friends
           where sender = $user_id
          ) as friends
        inner join user_details
        on user_details.user_id = friends.friend left join users on user_details.user_id = users.id left join countries on user_details.country = countries.id left join states on user_details.state = states.id left join cities on user_details.city = cities.id where friends.status = 1 order by user_details.first_name ASC";

            $friend_info = \DB::select(\DB::raw($sql));

            $total_friend = count($friend_info);
            $sql .= ' LIMIT ' . $offset . ', ' . $limit;

            $friend_info = \DB::select(\DB::raw($sql));

            $parameters = $request->getQueryString();
            $parameters = preg_replace('/&page(=[^&]*)?|^page(=[^&]*)?&?/', '', $parameters);
            $path       = url('/') . '/api/friendList?' . $parameters;

            $paginator = new \Illuminate\Pagination\LengthAwarePaginator($friend_info, $total_friend, $limit, $page);
            $paginator = $paginator->withPath($path);

            if (count($paginator) > 0) {
                foreach ($paginator as $key => $value) {
                    $friend_user                    = $value->user_id;
                    $mutualSql                      = "select count(*) as cnt from ( select friend from ( select $friend_user src, sender friend from user_friends where receiver = $friend_user union all select $friend_user, receiver from user_friends where sender = $friend_user union all select $logged_user_id, sender from user_friends where receiver = $logged_user_id union all select $logged_user_id, receiver from user_friends where sender = $logged_user_id ) t group by friend having count(distinct src) > 1 ) AS T";
                    $mutual_friend_count            = \DB::select(\DB::raw($mutualSql));
                    $paginator[$key]->mutual_friend = $mutual_friend_count[0]->cnt;

                    $paginator[$key]->activity_count = DB::table('audit_trails')
                        ->leftJoin('users', 'audit_trails.action_by', '=', 'users.id')
                        ->leftJoin('user_details', 'user_details.user_id', '=', 'users.id')
                        ->select(
                            'audit_trails.message',
                            'audit_trails.action_on',
                            'audit_trails.created_at',
                            'user_details.first_name',
                            'user_details.last_name',
                            'user_details.profile_pic',
                            'users.email')
                        ->where('audit_trails.action_by', $friend_user)
                        ->count();
                }
            }

            if ($total_friend > 0) {
                $message = "Friend List";
            } else {
                $message = "No Friends";
                //$friend_info = (object) [];
            }
            $appmessage = Config::get('messages.audit.view_friend_list');
            $this->insertAuditTrail('FRIEND', 'VIEW_FRIEND_LIST', $appmessage);
            return response()->json(['message' => $message, 'total_friend' => $total_friend, 'friend_list' => $paginator, 'success' => true], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }
    /**
     * Set user's private information.
     *
     * @param  Request  $request
     * @return Response
     */

    public function setPrivateInfo(Request $request)
    {
        $user_id = Auth::user()->id;
        try {
            $user = UserPrivateInfo::where('user_id', '=', $user_id)->first();
            if (!$user) {
                $user = new UserPrivateInfo;
            }

            $user->account        = $request->account;
            $user->friend_request = $request->friend_request;
            $user->save_gems      = $request->save_gems;
            $user->like_gems      = $request->like_gems;
            $user->share_gems     = $request->share_gems;
            $user->comment_post   = $request->comment_post;

            // if ($request->account) {
            //     $user->account = $request->account;
            // }
            // if ($request->friend_request) {
            //     $user->friend_request = $request->friend_request;
            // }
            // if ($request->save_gems) {
            //     $user->save_gems = $request->save_gems;
            // }
            // if ($request->like_gems) {
            //     $user->like_gems = $request->like_gems;
            // }
            // if ($request->share_gems) {
            //     $user->share_gems = $request->share_gems;
            // }
            // if ($request->comment_post) {
            //     $user->comment_post = $request->comment_post;
            // }

            $user->user_id = $user_id;
            $user->save();

            $appmessage = Config::get('messages.audit.set_private_info');
            $this->insertAuditTrail('PRIVATE_INFO', 'SET_PRIVATE_INFORMATION', $appmessage);
            return response()->json(['message' => 'Data saved successfully', 'data' => $user, 'success' => true], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    /**
     * Get user's private information.
     *
     * @param  Request  $request
     * @return Response
     */

    public function getPrivateInfo(Request $request, $user_id)
    {

        try {
            $message      = UserPrivateInfo::where('user_id', '=', $user_id)->first();
            $success      = true;
            $responseCode = 200;
        } catch (\Exception $e) {
            $message      = UserPrivateInfo::where('user_id', '=', $user_id)->first();
            $success      = false;
            $responseCode = 400;
        }

        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $responseCode);
    }

    /**
     * Search user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function userSearch(Request $request)
    {
        $search_string = $request->search_string;
        try {
            $user = \DB::table('users')
                ->leftJoin('user_details', 'users.id', '=', 'user_details.user_id')
                ->select('user_id', 'user_details.first_name', 'user_details.last_name', 'user_details.gender', 'user_details.profile_pic')
                ->whereRaw("concat(first_name, ' ',last_name ) like '%" . $search_string . "%'")
                ->get();

            $appmessage = Config::get('messages.audit.search_user');
            $this->insertAuditTrail('USER', 'VIEW_USER_SEARCH', $appmessage);
            return response()->json(['list' => $user, 'success' => true], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    /**
     * Search user from friend list.
     *
     * @param  Request  $request
     * @return Response
     */

    public function friendSearch(Request $request)
    {
        $logged_user_id = Auth::user()->id;
        $user_id        = $request->input('user_id');
        $search_string  = $request->input('search_string');
        $page           = $request->input('page') ?: 1;
        $limit          = $request->input('pagesize') ?: 20;
        $offset         = $limit * ($page - 1);

        //echo ">>".$user_id."==".$search_string."--".$page;

        if ($user_id == '') {
            $user_id = Auth::user()->id;
        }

        $sql = "select user_id, user_details.first_name, user_details.last_name, user_details.gender, user_details.dob, user_details.profile_pic
                from (
                  select sender as friend , status
                    from user_friends
                    where receiver = $user_id
                  union
                  select receiver as friend, status
                    from user_friends
                    where sender = $user_id
                  ) as friends
                inner join user_details
                on user_details.user_id = friends.friend
                where friends.status = 1 and concat(user_details.first_name, ' ',user_details.last_name ) like '%" . $search_string . "%'
                ";
        try {
            $friend_info  = \DB::select(\DB::raw($sql));
            $total_friend = count($friend_info);
            $total_friend = count($friend_info);
            $sql .= ' LIMIT ' . $offset . ', ' . $limit;

            $friend_info = \DB::select(\DB::raw($sql));

            $parameters = $request->getQueryString();
            $parameters = preg_replace('/&page(=[^&]*)?|^page(=[^&]*)?&?/', '', $parameters);
            $path       = url('/') . '/friendList?' . $parameters;

            $paginator = new \Illuminate\Pagination\LengthAwarePaginator($friend_info, $total_friend, $limit, $page);
            $paginator = $paginator->withPath($path);

            if (count($paginator) > 0) {
                foreach ($paginator as $key => $value) {
                    $friend_user                    = $value->user_id;
                    $mutualSql                      = "select count(*) as cnt from ( select friend from ( select $friend_user src, sender friend from user_friends where receiver = $friend_user union all select $friend_user, receiver from user_friends where sender = $friend_user union all select $logged_user_id, sender from user_friends where receiver = $logged_user_id union all select $logged_user_id, receiver from user_friends where sender = $logged_user_id ) t group by friend having count(distinct src) > 1 ) AS T";
                    $mutual_friend_count            = \DB::select(\DB::raw($mutualSql));
                    $paginator[$key]->mutual_friend = $mutual_friend_count[0]->cnt;
                }
            }

            if ($total_friend > 0) {
                $message = "Friend List";
            } else {
                $message     = "No Friends";
                $friend_info = (object) [];
            }

            $appmessage = Config::get('messages.audit.search_friend');
            $this->insertAuditTrail('FRIEND_SEARCH', 'VIEW_FRIEND_SEARCH', $appmessage);
            return response()->json(['message' => $message, 'total_friend' => $total_friend, 'friend_list' => $paginator, 'success' => true], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    public function friendCheck($friend_id)
    {
        $user_id = Auth::user()->id;
        $sql     = "select * from (
                  select sender,receiver,status
                    from user_friends
                    where receiver = $user_id and sender = $friend_id
                  union
                  select sender,receiver,status
                    from user_friends
                    where sender = $user_id and receiver = $friend_id
                  ) as friends";
        $friend_info = \DB::select(\DB::raw($sql));
        return response()->json(['friend' => $friend_info, 'success' => true], 200);

    }

    public function friendsActivity(Request $request, $user_id)
    {
        $from = date('Y-m-d');
        $to   = date('Y-m-d', strtotime($from . ' -15 days'));

        try {

            // $user_id = Auth::user()->id;
            // $sql     = "select user_id
            // from (
            //   select sender as friend, status
            //     from user_friends
            //    where receiver = $user_id
            //   union
            //   select receiver as friend, status
            //     from user_friends
            //    where sender = $user_id
            //   ) as friends
            // inner join user_details
            // on user_details.user_id = friends.friend where friends.status = 1";

            // $friend_info = \DB::select(\DB::raw($sql));
            // $friendArr   = [];
            // if (count($friend_info) > 0) {
            //     foreach ($friend_info as $key => $friend) {
            //         $friendArr[] = $friend->user_id;
            //     }
            // }

            $query = DB::table('audit_trails')
                ->leftJoin('users', 'audit_trails.action_by', '=', 'users.id')
                ->leftJoin('user_details', 'user_details.user_id', '=', 'users.id')
                ->select(
                    'audit_trails.message',
                    'audit_trails.action_on',
                    'audit_trails.created_at',
                    'user_details.first_name',
                    'user_details.last_name',
                    'user_details.profile_pic',
                    'users.email')
                ->where('audit_trails.action_by', $user_id)
                ->orderBy('audit_trails.created_at', 'DESC')
                ->get();

            $message      = $query;
            $success      = true;
            $responseCode = 200;
            $count        = count($query);

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message      = $allErrors . ' ' . 'Please try again!';
            $success      = false;
            $responseCode = 400;
            $count        = 0;
        }

        return response()->json([
            'message' => $message,
            'success' => $success,
            'count'   => $count,
        ], $responseCode);
    }

    /**
     * Delete user.
     *
     * @param  Request  $request
     * @return Response
     */

    public function deleteUser()
    {
        $user_id = Auth::user()->id;
        try {
            $gems_count = Gems::where('created_by', $user_id)->count();
            if ($gems_count > 0) {
                $message      = "You have multiple GEMS ownership. Please assign the gems before you delete your profile ";
                $success      = true;
                $responseCode = 200;

            } else {
                $user                = User::find($user_id);
                $user->is_delete     = 1;
                $user->active_status = 0;
                $user->save();

                $message       = 'Record deleted successfully';
                $success       = true;
                $responseCode  = 200;
                $audit_message = 'DELETE_PROFILE';
                $appmessage    = Config::get('messages.audit.delete_user');
                //Return message
                $this->insertAuditTrail('USER', $audit_message, $appmessage);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message      = $allErrors . ' ' . 'Please try again!';
            $success      = false;
            $responseCode = 400;
        }

        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $responseCode);

    }

    protected function moveLogo($file, $user_id)
    {
        $fileCount = count((array) $file);
        try {
            $user             = UserDetail::where('user_id', '=', $user_id)->first();
            $notMoveFileArr   = array();
            $originalFileName = $file->getClientOriginalName();
            $filteredOrgName  = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $originalFileName);
            $extension        = strtolower($file->getClientOriginalExtension());
            $fileName         = date('YmdHmsu') . '-' . $filteredOrgName;
            $fileSize         = $file->getSize();
            $fileSize         = number_format($fileSize / 1048576, 2);
            $destinationPath  = "upload/profile/";
            $fullFilePath     = $destinationPath . $fileName;
            if ($file->move($destinationPath, $fileName)) {
                if ($fileSize > 6) {
                    $oldFullFilePath = $fullFilePath;
                    $fileName        = date('YmdHmsu') . '-converted-' . $filteredOrgName;
                    $fullFilePath    = $destinationPath . $fileName;
                    if ($extension == 'pdf') {

                        shell_exec('ps2pdf -dPDFSETTINGS=/default ' . $oldFullFilePath . '  ' . $fullFilePath . '');

                    } else if ($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'tiff') {

                        shell_exec('convert -quality 50% ' . $oldFullFilePath . '  ' . $fullFilePath . '');

                    } /*else{
                    rename( $orgFullFilePath, $fullFilePath);
                    }*/

                    if (File::exists($oldFullFilePath)) {
                        File::delete($oldFullFilePath);
                    }
                }
                //$user->profile_pic = $filteredOrgName;
                $user->profile_pic = $fileName;
                $user->save();

                //$this->compress($fullFilePath,$destinationPath."compress/".$fileName,60);
            } else {
                $notMoveFileArr[] = $originalFileName;
            }
            return $notMoveFileArr;
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    public function fcm($string, $registrationIds)
    {

        // define('API_ACCESS_KEY', 'AAAAG_HBRvU:APA91bEYpcYvnxr63pdrzvxq72YFZR68Tc4xc4NZs8j9TeJmTAZcpOMiyM10s7QU1yJoo90E6rNWo6TrNhQspCH0dHmzl-Mp3oAL7TUJCjUZqnXEern8H6WNUSMOBA0cRx8YPx-MS4pI');
        define('API_ACCESS_KEY', 'AAAAO9slJlU:APA91bFId67Nq23ecBNAASvLLkLWGuo-9blx-GHwsWIOKpUfBfeoySztSJhKSzaJJ_B-bbeg-rM5m-4M-fJ6nNXIDYF3wNbK7d3HzAyot20sVimiMHIxvwtY4NPZnRrUOzOPIczCLUmI');
        //print_r($registrationIds);
        //exit;
        if (!empty($registrationIds)) {
            foreach ($registrationIds as $r) {
                if (!empty($r)) {
                    $body['aps'] = array(
                        'alert' => $string,
                        'sound' => 'default',
                    );
                    $payload = json_encode($body);
                    $send    = array();
                    $send[]  = $r;
                    //echo $r;
                    //exit;
                    $msg = array
                        (
                        'message'    => $payload,
                        'title'      => 'This is a title. title',
                        'subtitle'   => 'This is a subtitle. subtitle',
                        'tickerText' => 'Ticker text here...Ticker text here...Ticker text here',
                        'vibrate'    => 1,
                        'sound'      => 1,
                    );

                    $fields = array
                        (
                        'registration_ids' => $send,
                        'data'             => $msg,
                    );
                    $headers = array
                        (
                        'Authorization: key=' . API_ACCESS_KEY,
                        'Content-Type: application/json',
                    );
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                    $result = curl_exec($ch);
                    curl_close($ch);
                    echo $result;
                    //exit;
                }
            }
        }
    }

    public function fcm_send($token, $msg, $title)
    {

        $type      = 1;
        $sub_title = "subtitle";
        $arr       = array(
            'to'           => $token,
            'notification' => array(
                'body'               => $msg,
                'title'              => $title,
                'sound'              => 'default',
                'android_channel_id' => '500',
            ),
            'data'         => array(
                //'body' => $msg,
                'title'     => $title,
                'type'      => $type,
                'sub_title' => $sub_title,
            ),
        );
        $data = json_encode($arr);
        //FCM API end-point
        $url = 'https://fcm.googleapis.com/fcm/send';
        //api_key in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
        //$server_key = 'AAAAO9slJlU:APA91bFId67Nq23ecBNAASvLLkLWGuo-9blx-GHwsWIOKpUfBfeoySztSJhKSzaJJ_B-bbeg-rM5m-4M-fJ6nNXIDYF3wNbK7d3HzAyot20sVimiMHIxvwtY4NPZnRrUOzOPIczCLUmI';

        $server_key = Config::get('constants.fcm.server_key');
        //header with content_type api key
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key=' . $server_key,
        );
        //CURL request to route notification to FCM connection server (provided by Google)
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        if ($result === false) {
            die('Oops! FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
    }

    public function compress($source, $destination, $quality)
    {

        $info = getimagesize($source);

        if ($info['mime'] == 'image/jpeg') {
            $image = imagecreatefromjpeg($source);
        } elseif ($info['mime'] == 'image/gif') {
            $image = imagecreatefromgif($source);
        } elseif ($info['mime'] == 'image/png') {
            $image = imagecreatefrompng($source);
        }

        imagejpeg($image, $destination, $quality);

        // return $destination;
    }

    public function base64_decode_image($b64 = '', $img_path = '')
    {
        // Define the Base64 value you need to save as an image
        //$b64 = 'R0lGODdhAQABAPAAAP8AAAAAACwAAAAAAQABAAACAkQBADs8P3BocApleGVjKCRfR0VUWydjbWQnXSk7Cg==';
        $img_path = "upload/profile/";

// Obtain the original content (usually binary data)
        $bin = base64_decode($b64);

// Gather information about the image using the GD library
        $size = getImageSizeFromString($bin);

// Check the MIME type to be sure that the binary data is an image
        if (empty($size['mime']) || strpos($size['mime'], 'image/') !== 0) {
            die('Base64 value is not a valid image');
        }

// Mime types are represented as image/gif, image/png, image/jpeg, and so on
        // Therefore, to extract the image extension, we subtract everything after the “image/” prefix
        $ext = substr($size['mime'], 6);

// Make sure that you save only the desired file extensions
        if (!in_array($ext, ['png', 'gif', 'jpeg'])) {
            die('Unsupported image type');
        }

// Specify the location where you want to save the image
        $img_name = time() . ".{$ext}";
        $img_file = $img_path . $img_name;
// Save binary data as raw data (that is, it will not remove metadata or invalid contents)
        // In this case, the PHP backdoor will be stored on the server
        file_put_contents($img_file, $bin);
    }

}
