<?php

namespace App\Http\Controllers;

use App\Homepage;
use Illuminate\Http\Request;

class HomepageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Homepage list for frontend.
     *
     * @param  Request  $request
     * @return Response
     */

    public function homepageList(Request $request)
    {
        $type = $request->type;
        $models = Homepage::where('status', '=', '1');
        if($type>0){
           $models->where('type',$type); 
        }
        $models = $models->get();
        return response()->json($models);
    }

    /**
     * Homepage list for admin.
     *
     * @param  Request  $request
     * @return Response
     */

    public function homepageListAdmin(Request $request)
    {
        $pageSize = $request->input('pageSize') ? $request->input('pageSize') : '10';
        $models   = Homepage::paginate($pageSize);
        return response()->json($models);
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'type'  => 'required',
            'title' => 'required',
        ]);

        try {

            $models                    = new Homepage;
            $models->type              = $request->type;
            $models->title             = $request->title;
            $models->slug              = preg_replace("![^a-z0-9]+!i", "-", strtolower($models->title));
            $models->short_description = $request->short_description;
            $models->description       = $request->description;
            $models->status            = $request->status;

            // $models->title = "1111";
            // $models->slug = "111";
            // $models->short_description = "adsd";
            // $models->description       = "hkkk";
            // $models->status = "1";
            $models->save();

            if ($request->hasFile('image')) {

                $file        = $request->file('image');
                $homepage_id = $models->id;

                $fileCount = $this->moveImage($file, $homepage_id);
            }
            return response()->json([
                'message' => 'Record added successfully',
                'success' => true,
            ], 200);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 500);
        }
    }

    public function update(Request $request)
    {
        $id = $request->id;
        try {
            $models = Homepage::find($id);
            if ($request->type != '') {
                $models->type = $request->type;
            }

            if ($request->title != '') {
                $models->title = $request->title;
            }

            if ($request->short_description != '') {
                $models->short_description = $request->short_description;
            }

            if ($request->description != '') {
                $models->description = $request->description;
            }
            if ($request->status != '') {
                $models->status = $request->status;
            }

            if ($request->hasFile('image')) {

                $file        = $request->file('image');
                $homepage_id = $models->id;

                $fileCount = $this->moveImage($file, $homepage_id);
            }

            $models->save();
            return response()->json([
                'message' => 'Record updated successfully',
                'user'    => $models,
                'success' => true,
            ], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'success' => false], 400);
        }
    }

    protected function moveImage($file, $homepage_id)
    {
        $fileCount = count((array) $file);
        try {

            $notMoveFileArr   = array();
            $originalFileName = $file->getClientOriginalName();
            $filteredOrgName  = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $originalFileName);
            $extension        = strtolower($file->getClientOriginalExtension());
            $fileName         = date('YmdHmsu') . '-' . $filteredOrgName;
            $fileSize         = $file->getSize();
            $fileSize         = number_format($fileSize / 1048576, 2);
            $destinationPath  = "upload/homepage/";
            $fullFilePath     = $destinationPath . $fileName;
            if ($file->move($destinationPath, $fileName)) {
                if ($fileSize > 6) {
                    $oldFullFilePath = $fullFilePath;
                    $fileName        = date('YmdHmsu') . '-converted-' . $filteredOrgName;
                    $fullFilePath    = $destinationPath . $fileName;
                    if ($extension == 'pdf') {

                        shell_exec('ps2pdf -dPDFSETTINGS=/default ' . $oldFullFilePath . '  ' . $fullFilePath . '');

                    } else if ($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'tiff') {

                        shell_exec('convert -quality 50% ' . $oldFullFilePath . '  ' . $fullFilePath . '');

                    } /*else{
                    rename( $orgFullFilePath, $fullFilePath);
                    }*/

                    if (File::exists($oldFullFilePath)) {
                        File::delete($oldFullFilePath);
                    }
                }
                $homepage        = Homepage::find($homepage_id);
                $homepage->image = $fileName;
                $homepage->save();

                //$this->compress($fullFilePath,$destinationPath."compress/".$fileName,60);
            } else {
                $notMoveFileArr[] = $originalFileName;
            }
            return $notMoveFileArr;
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    public function statusUpdate(Request $request, $id)
    {

        //return response()->json($request);
        \DB::beginTransaction();
        try {
            $model = Homepage::find($id);
            if (!empty($model)) {

                if ($model->status != (int) $request->input('status')) {
                    $model->status = $request->input('status');
                }

                if ($model->is_approved != (int) $request->input('is_approved')) {
                    $model->is_approved = $request->input('is_approved');
                }

                $model->save();

                \DB::commit();
                $message       = 'Updated successfully!';
                $success       = true;
                $audit_message = 'STATUS_UPDATE_HOMEPAGE_SUCCESS';

            } else {
                $message       = 'record not exists!';
                $success       = false;
                $audit_message = 'STATUS_UPDATE_HOMEPAGE_FAIL';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $success       = false;
            $audit_message = 'STATUS_UPDATE_GEMS_FAIL';
        }

        $this->insertAuditTrail('STATUS_UPDATE_HOMEPAGE', $audit_message);
        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 200);
    }

    public function homepageDetails($id)
    {
        try {
            $models = Homepage::find($id);
            return response()->json([
                'message' => $models,
                'success' => true,
            ], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    /**
     * Delete record.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        \DB::beginTransaction();
        try {
            $model = Homepage::find($id);
            if (!empty($model)) {
                $model->delete();

                \DB::commit();
                $message       = 'Removed successfully!';
                $success       = true;
                $audit_message = 'DELETE_HOMEPAGE_SUCCESS';

            } else {
                $message       = 'Record not exists!';
                $success       = true;
                $audit_message = 'DELETE_HOMEPAGE_FAIL';
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message       = $allErrors . ' ' . 'Please try again!';
            $alerttype     = false;
            $audit_message = 'DELETE_HOMEPAGE_FAIL';
        }

        //Return message
        $this->insertAuditTrail('DELETE_HOMEPAGE', $audit_message);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 200);
    }

    //
}
