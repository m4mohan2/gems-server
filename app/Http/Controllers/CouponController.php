<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\CouponUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class CouponController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //

    /**
     * Coupon List.
     *
     * @param  Request  $request
     * @return Response
     */

    public function couponList(Request $request)
    {
        $coupon   = $request->coupon;
        $pageSize = $request->input('pageSize') ? $request->input('pageSize') : '10';

        $models = Coupon::with(['creator', 'assignuser', 'assignuserEmail']);

        if ($coupon != '') {
            $models->where('coupon', '=', $coupon);
        }

        if ($request->input('creator_id')) {
            $models->where('creator_id', '=', $request->input('creator_id'));
        }
        $models->orderBy('id','DESC');
        $result = $models->paginate($pageSize);

        return response()->json([
            'data'    => $result,
            //'totalCount' => $models->count(),
            'success' => true,
        ], 200);
    }

    /**
     * Coupon list by user.
     *
     * @param  Request  $request
     * @return Response
     */

    public function userCoupons($user_id)
    {
        $models = CouponUser::with([
            'coupon',
        ])->where('user_id', $user_id)->get();

        return response()->json([
            'data'       => $models,
            'totalCount' => $models->count(),
            'success'    => true,
        ], 200);
    }


    /**
     * Active Coupon list by user.
     *
     * @param  Request  $request
     * @return Response
     */

    public function userActiveCoupons($user_id)
    {
        $models =  \DB::table('coupons')
                ->join('coupon_users', 'coupons.id', '=', 'coupon_users.coupon_id')
                ->where('coupon_users.user_id', $user_id)
                ->whereRaw('? between start_date and end_date', [date('Y-m-d H:i:s')])
                ->where('coupons.active_status', '=', '1')->get();

        return response()->json([
            'data'       => $models,
            'totalCount' => $models->count(),
            'success'    => true,
        ], 200);
    }

    /**
     * Coupon details.
     *
     * @param  Request  $request
     * @return Response
     */

    public function couponDetails($code)
    {
        \DB::beginTransaction();
        try {
            $coupon = Coupon::with(['assignInfo', 'creator', 'creatorEmail', 'assignuser', 'assignuserEmail'])->where('coupon', $code)->first();
            \DB::commit();
            $message       = $coupon;
            $success       = true;
            $resCode       = 200;
            $audit_message = 'VIEW_COUPON_DETAILS_SUCCESS';
            $audit_on      = $code;
        } catch (\Exception $e) {
            //return error message
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'VIEW_COUPON_DETAILS_FAIL';
            $audit_on      = $code;
        }
        $appmessage = Config::get('messages.audit.view_coupon');
        $this->insertAuditTrail('COUPON', $audit_message,$appmessage,'', $audit_on);
        return response()->json([
            'details' => $message,
            'success' => $success,
        ], $resCode);

    }

    /**
     * Gemerate coupon.
     *
     * @param  Request  $request
     * @return Response
     */

    public function generateCoupon(Request $request)
    {
        $this->validate($request, [
            'creator_id' => 'required',
            'start_date' => 'required',
            'end_date'   => 'required',
        ]);

        \DB::beginTransaction();
        try {

            $random_chars = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $coupon_code  = substr(str_shuffle(substr(str_shuffle($random_chars), 0, 16) . time()),0,8);

            $models              = new Coupon;
            $models->coupon      = $coupon_code;
            $models->creator_id  = $request->creator_id;
            $models->coupon_type = $request->coupon_type;
            if ($request->coupon_type == 0) {
                $models->amount      = $request->amount;
                $models->amount_type = $request->amount_type;
            }
            $models->start_date    = $request->start_date;
            $models->end_date      = $request->end_date;
            $models->description   = $request->description;
            $models->active_status = $request->active_status;

            //dd($models);
            $models->save();

            \DB::commit();
            $message       = 'Coupon generated successfully';
            $success       = true;
            $resCode       = 200;
            $audit_message = 'GENERATE_COUPON_SUCCESS';
            $audit_on      = $coupon_code;
        } catch (\Exception $e) {
            //return error message
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'GENERATE_COUPON_FAIL';
            $audit_on      = '';
        }

        $appmessage = Config::get('messages.audit.create_coupon');
        $this->insertAuditTrail('COUPON', $audit_message, $audit_on);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);

    }

    /**
     * Update coupon.
     *
     * @param  Request  $request,$id
     * @return Response
     */

    public function updateCoupon(Request $request, $id)
    {
        \DB::beginTransaction();
        try {

            $models = Coupon::find($id);
            if (!empty($models)) {
                if (isset($request->coupon_type)) {
                    $models->coupon_type = $request->coupon_type;
                }
                
                if (isset($request->amount)) {
                    $models->amount = $request->amount;
                    if (isset($request->amount_type)) {
                        $models->amount_type = $request->amount_type;
                    }
                }

                if (isset($request->start_date)) {
                    $models->start_date = $request->start_date;
                }

                if (isset($request->end_date)) {
                    $models->end_date = $request->end_date;
                }

                if (isset($request->description)) {
                    $models->description = $request->description;
                }

                if (isset($request->active_status)) {
                    $models->active_status = $request->active_status;
                }

                $models->save();
                \DB::commit();
                $message       = 'Coupon updated successfully';
                $success       = true;
                $resCode       = 200;
                $audit_message = 'UPDATE_COUPON_SUCCESS';
                $audit_on      = $models->coupon;
            } else {
                \DB::commit();
                $message       = 'Record not exists';
                $success       = false;
                $resCode       = 400;
                $audit_message = 'UPDATE_COUPON_FAIL';
                $audit_on      = '';
            }

        } catch (\Exception $e) {
            //return error message
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'UPDATE_COUPON_FAIL';
            $audit_on      = '';
        }
        $appmessage = Config::get('messages.audit.update_coupon');
        $this->insertAuditTrail('UPDATE_COUPON', $audit_message,$appmessage,'', $audit_on);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    /**
     * Delete coupon.
     *
     * @param  Request  $request
     * @return Response
     */

    public function deleteCoupon($id)
    {
        \DB::beginTransaction();
        try {
            $model = Coupon::find($id);
            if (!empty($model)) {
                $model->delete();
                \DB::commit();
                $message       = 'Coupon deleted successfully';
                $success       = true;
                $resCode       = 200;
                $audit_message = 'DELETE_COUPON_SUCCESS';
                $audit_on      = $model->coupon;
            } else {
                \DB::commit();
                $message       = 'Record not exists';
                $success       = false;
                $resCode       = 400;
                $audit_message = 'DELETE_COUPON_FAIL';
                $audit_on      = '';
            }
        } catch (\Exception $e) {
            //return error message
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'DELETE_COUPON_FAIL';
            $audit_on      = '';
        }
        $appmessage = Config::get('messages.audit.delete_coupon');
        $this->insertAuditTrail('COUPON', $audit_message,$appmessage,'', $audit_on);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    /**
     * Status change coupon.
     *
     * @param  Request  $request
     * @return Response
     */

    public function statusChange(Request $request, $id)
    {

        \DB::beginTransaction();
        try {
            $model = Coupon::find($id);
            if (!empty($model)) {
                if ($model->active_status != (int) $request->input('status')) {
                    $model->active_status = $request->input('status');
                }
                $model->save();

                \DB::commit();
                $message       = 'Status changed successfully';
                $success       = true;
                $resCode       = 200;
                $audit_message = 'COUPON_STATUS_CHANGE_SUCCESS';
                $audit_on      = $model->coupon;

            } else {
                \DB::commit();
                $message       = 'Record not exists';
                $success       = false;
                $resCode       = 400;
                $audit_message = 'COUPON_STATUS_CHANGE_FAIL';
                $audit_on      = '';

            }

        } catch (\Exception $e) {
            //return error message
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'COUPON_STATUS_CHANGE_FAIL';
            $audit_on      = '';

        }
        $appmessage = Config::get('messages.audit.change_coupon_status');
        $this->insertAuditTrail('COUPON', $audit_message,$appmessage,'', $audit_on);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    /**
     * Assign coupon.
     *
     * @param  Request  $request
     * @return Response
     */

    public function assignCoupon(Request $request)
    {
        \DB::beginTransaction();
        try {

            $coupon_id = $request->coupon_id;
            $user_ids  = $request->user_id;
            // if (is_array($user_ids)) {
            //     foreach ($user_ids as $key => $id) {
            //         $model            = new CouponUser;
            //         $model->coupon_id = $coupon_id;
            //         $model->user_id   = $id;
            //         $model->save();
            //     }
            // } else {
            $model            = new CouponUser;
            $model->coupon_id = $coupon_id;
            $model->user_id   = $user_ids;
            $model->save();

            $receiver_device_token = User::where('id', $user_ids)->select('device_token')->first();
            if ($receiver_device_token->device_token != '') {
                $title        = "Reward received";
                $push_message = "You have received a new coupon";
                $this->fcm_send($receiver_device_token->device_token, $push_message, $title);
            }
            //}
            \DB::commit();
            $message       = 'Coupon assigned successfully';
            $success       = true;
            $resCode       = 200;
            $audit_message = 'COUPON_ASSIGN_SUCCESS';
            $audit_on      = '';
            $action_id     = $coupon_id;
            $action_to     = $user_ids;

        } catch (\Exception $e) {
            //return error message
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'COUPON_ASSIGN_FAIL';
            $audit_on      = '';
            $action_id     = null;
            $action_to     = null;
        }
        $appmessage = Config::get('messages.audit.assign_coupon');
        $this->insertAuditTrail('COUPON', $audit_message,$appmessage,'', $audit_on, $action_id, $action_to);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    /**
     * Redeem coupon.
     *
     * @param  Request  $request
     * @return Response
     */

    public function redeemCoupon(Request $request)
    {
        $user_id     = $request->user_id;
        $coupon      = $request->coupon;
        $today       = date('Y-m-d H:i:s');
        $status_code = '200';
        \DB::beginTransaction();
        try {
            $model = \DB::table('coupons')->join('coupon_users', 'coupons.id', '=', 'coupon_users.coupon_id')
                ->where('coupon_users.user_id', $user_id)->where('coupons.coupon', $coupon)->select('coupon_users.id', 'coupons.end_date', 'coupons.start_date')->first();

            if ($model) {
                if ($today > $model->end_date) {

                    \DB::commit();
                    $message       = "Coupon has expired";
                    $success       = false;
                    $resCode       = 400;
                    $audit_message = 'REDEEM_COUPON_FAIL';
                    $audit_on      = $coupon;
                    $action_id     = null;
                    $action_to     = $user_id;
                } else if ($today < $model->start_date) {

                    \DB::commit();
                    $message       = "Coupon will be available on " . date('F j, Y, h:i A', strtotime($model->start_date));
                    $success       = false;
                    $resCode       = 400;
                    $audit_message = 'REDEEM_COUPON_FAIL';
                    $audit_on      = $coupon;
                    $action_id     = null;
                    $action_to     = $user_id;

                } else {
                    $model_coupon              = CouponUser::find($model->id);
                    $model_coupon->is_redeem   = 'Yes';
                    $model_coupon->redeem_date = $today;
                    $model_coupon->save();

                    $receiver_device_token = User::where('id', $user_id)->select('device_token')->first();
                    if ($receiver_device_token->device_token != '') {
                        $title        = "New Coupon";
                        $push_message = "You have successfully redeem your coupon";
                        $this->fcm_send($receiver_device_token->device_token, $push_message, $title);
                    }

                    \DB::commit();
                    $message       = 'Coupon redeem successfully';
                    $success       = true;
                    $resCode       = 200;
                    $audit_message = 'REDEEM_COUPON_SUCCESS';
                    $audit_on      = $coupon;
                    $action_id     = null;
                    $action_to     = $user_id;
                }

            } else {

                \DB::commit();
                $message       = 'Invalid coupon code';
                $success       = false;
                $resCode       = 400;
                $audit_message = 'REDEEM_COUPON_FAIL';
                $audit_on      = $coupon;
                $action_id     = null;
                $action_to     = $user_id;

            }
        } catch (\Exception $e) {
            //return error message
            \DB::rollback();
            $message       = $e->getMessage();
            $success       = false;
            $resCode       = 400;
            $audit_message = 'REDEEM_COUPON_FAIL';
            $audit_on      = '';
            $action_id     = null;
            $action_to     = null;
        }
        $appmessage = Config::get('messages.audit.redeem_coupon');
        $this->insertAuditTrail('COUPON', $audit_message,$appmessage,'', $audit_on, $action_id, $action_to);
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);

    }

    public function fcm_send($token, $msg, $title)
    {

        $type      = 1;
        $sub_title = "subtitle";
        $arr       = array(
            'to'           => $token,
            'notification' => array(
                'body'               => $msg,
                'title'              => $title,
                'sound'              => 'default',
                'android_channel_id' => '500',
            ),
            'data'         => array(
                //'body' => $msg,
                'title'     => $title,
                'type'      => $type,
                'sub_title' => $sub_title,
            ),
        );
        $data = json_encode($arr);
        //FCM API end-point
        $url = 'https://fcm.googleapis.com/fcm/send';

        $server_key = Config::get('constants.fcm.server_key');
        //header with content_type api key
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key=' . $server_key,
        );
        //CURL request to route notification to FCM connection server (provided by Google)
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        if ($result === false) {
            die('Oops! FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
    }

}
