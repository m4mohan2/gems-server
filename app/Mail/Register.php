<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Register extends Mailable
{
    use Queueable, SerializesModels;
    public $name;
    public $email;
    public $password;
    public $link;
    public $logo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$email,$password,$link)
    {
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
        $this->link = $link;
        $this->logo = 'http://54.147.235.207/gems_uat/service/public/img/logo.png';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Activation Mail')->view('emails.register');

        //$data = $this->name;

        $data['name'] = $this->name;
        $data['email'] = $this->email;
        $data['password'] = $this->password;
        $data['link'] = $this->link;
        $data['logo'] = $this->logo;

        $this->withSwiftMessage(function ($data) {
            $data->getHeaders()
                    ->addTextHeader('Custom-Header', 'HeaderValue');
        });
    }
}