<?php 

namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class FaqCategory extends Model
{ 
    public $timestamps = true;

    public function faq()
    {
        return $this->hasmany('App\Faq', 'category_id', 'id')->where('status',1);
    }

}
