<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'state_code',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'phonecode','native','capital','currency','emoji','emojiU','created_at','updated_at','flag','wikiDataId'
    ];

    public function states()
    {
        return $this->hasMany('App\State','country_id', 'id');
    }

    public function gemsLike()
    {
        return $this->hasManyThrough('App\GemsLikeunlike', 'App\UserDetail','country','user_id','id','user_id')->where('action','1');
    }

    public function gemsComment()
    {
        return $this->hasManyThrough('App\GemsComment', 'App\UserDetail','country','user_id','id','user_id');
    }

    public function gemsShare()
    {
        return $this->hasManyThrough('App\ShareGemsGroup', 'App\UserDetail','country','share_by','id','user_id');
    }
}