<?php 

namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class GemsCategory extends Model
{ 
    public $timestamps = true;

    public function gems()
    {
        return $this->hasMany('App\Gems', 'id', 'category_id');
    }
}
