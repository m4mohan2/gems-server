<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    public $timestamps = true;

    public function creator()
    {
        return $this->belongsTo('App\UserDetail', 'creator_id', 'user_id')->select('user_id', 'first_name', 'last_name', 'profile_pic', 'gender', 'address');
    }

    public function creatorEmail()
    {
        return $this->belongsTo('App\User', 'creator_id', 'id')->select('id', 'email');
    }

    public function assignuser()
    {
    	return $this->hasOneThrough('App\UserDetail','App\CouponUser','coupon_id','user_id','id','user_id')->select('coupon_users.user_id', 'first_name', 'last_name', 'profile_pic', 'gender', 'address','is_redeem','redeem_date','ip_address');
    }
    public function assignuserEmail()
    {
    	return $this->hasOneThrough('App\User','App\CouponUser','coupon_id','id','id','user_id')->select('coupon_users.user_id', 'email');
    }

    public function assignInfo()
    {
        return $this->hasMany('App\CouponUser');
    }

}
