<?php namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class Role extends Model
{ 
    protected $fillable = ['roleName','facility','roleDescription','roleKey'];

    protected $appends = ['facility'];

    public function getFacilityAttribute()
    {
        return json_decode($this->attributes['facility']) ; //some logic to return
    }
   
}
