<?php 

namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class UserDetail extends Model
{ 
	protected $fillable = ['user_id'];
	public $timestamps = true;

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    } 

    public function total_view(){
        return $this->hasMany('App\GemsView', 'user_id','user_id')->where('type',1);
    }

    public function total_like(){
        return $this->hasMany('App\GemsLikeunlike', 'user_id','user_id')->where('action','1');
    }

    public function saved_lists(){
        return $this->hasMany('App\GemsSaveList', 'user_id','user_id');
    }

    public function total_share(){
        return $this->hasMany('App\ShareGemsGroup', 'share_by','user_id');
    }

    public function total_comment(){
        return $this->hasMany('App\GemsComment', 'user_id','user_id');
    }
}