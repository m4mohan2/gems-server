<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'country_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'iso2','country_code','fips_code','created_at','updated_at','flag','wikiDataId'
    ];

    public function cities()
    {
        return $this->hasMany('App\City','state_id', 'id');
    }

    public function parrentCountry()
    {
        return $this->belongsTo('App\Country','country_id');
    }
}