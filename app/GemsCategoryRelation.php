<?php 

namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class GemsCategoryRelation extends Model
{ 
    public $timestamps = true;

    public function categoryDetail()
    {
        return $this->hasOne('App\GemsCategory', 'id', 'category_id');

    }

}
