<?php 

namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class GroupMember extends Model
{ 
    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo('App\UserDetail', 'user_id','user_id')->select('user_id','first_name','last_name','profile_pic','gender','address');
    }

}
