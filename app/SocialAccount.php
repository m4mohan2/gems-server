<?php 

namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class SocialAccount extends Model
{ 
	protected $fillable = ['user_id'];
	public $timestamps = true;

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    } 
}