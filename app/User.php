<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;



use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Model implements JWTSubject, AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password','vp'
    ];

    /**
     * The attributes included in the model's JSON form.
     *
     * @var array
     */
    protected $columns = array('id','role_id','email','user_type','is_influencer','active_status','is_verified','token', 'active_token', 'password_reset_token', 'created_at','updated_at');

    /**
     * Return without exclude columns
     *
     * @return array
     */
    public function scopeExclude($query,$value = array()) 
    {
        return $query->select( array_diff( $this->columns,(array) $value) );
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function userDetail()
    {
        return $this->hasOne('App\UserDetail', 'user_id', 'id');
    }

    public function role()
    {
        return $this->hasOne('App\Role','id', 'role_id');
    }

    public function companyDetail()
    {
        return $this->hasOne('App\CompanyDetail', 'user_id', 'id');
    }

    public function companyDocuments()
    {
        return $this->hasMany('App\CompanyDocument', 'user_id', 'id');
    }

    public function privateData()
    {
        return $this->hasMany('App\UserPrivateInfo', 'user_id', 'id')->select('user_id','account','friend_request','save_gems','like_gems','share_gems','comment_post');
    }

    public function gemDetail()
    {
        return $this->hasManyThrough('App\Gems','App\GemsProviderRelation', 'user_id', 'id','id','gems_id')->select('name','address','action');
    }

    public function gemStatus()
    {
        return $this->hasOne('App\GemsProviderRelation','user_id', 'id')->select('action','gems_id','user_id');
    }

    public function userCountry()
    {
        return $this->hasOneThrough('App\Country','App\UserDetail', 'user_id', 'id','id','country')->select('name');
    }

    public function userState()
    {
        return $this->hasOneThrough('App\State','App\UserDetail', 'user_id', 'id','id','state')->select('name');
    }

    public function userCity()
    {
        return $this->hasOneThrough('App\City','App\UserDetail', 'user_id', 'id','id','city')->select('name');
    }

    public function companyCountry()
    {
        return $this->hasOneThrough('App\Country','App\CompanyDetail', 'user_id', 'id','id','country')->select('name');
    }

    public function companyState()
    {
        return $this->hasOneThrough('App\State','App\CompanyDetail', 'user_id', 'id','id','state')->select('name');
    }

    public function companyCity()
    {
        return $this->hasOneThrough('App\City','App\CompanyDetail', 'user_id', 'id','id','city')->select('name');
    }

    
}
