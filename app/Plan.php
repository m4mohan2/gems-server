<?php 

namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class Plan extends Model
{ 
    public $timestamps = true;

    protected $fillable = [
        'plan_name',
        'slug',
        'stripe_plan',
        'plan_price'
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

}
