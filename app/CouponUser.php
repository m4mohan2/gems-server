<?php 

namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class CouponUser extends Model
{ 
    public $timestamps = true;

    public function coupon()
    {
        return $this->belongsTo('App\Coupon', 'coupon_id','id');
    }

}
