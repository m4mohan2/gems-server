<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GemsFeed extends Model
{
    public $timestamps = true;

    public function gemsCategoryDetail()
    {
        return $this->hasManyThrough('App\GemsCategory', 'App\GemsCategoryRelation', 'gems_id', 'id', 'gems_id', 'category_id');
    }
    public function gemsImages()
    {
        return $this->hasMany('App\GemsImage', 'gems_id', 'gems_id')->select('gems_id', 'id', 'image', 'type', 'source');
    }

    public function gemsLike()
    {
        return $this->hasMany('App\GemsLikeunlike', 'gems_id', 'gems_id')->where('action', '1');
    }

    public function gemsDetails()
    {
        return $this->hasMany('App\Gems', 'id', 'gems_id');
    }

    public function gemsCreatedBy()
    {
        return $this->hasOneThrough('App\UserDetail', 'App\Gems', 'id', 'user_id', 'gems_id', 'created_by');
    }

    public function user_like_status()
    {
        return $this->hasMany('App\GemsLikeunlike', 'gems_id', 'gems_id');
    }

}
