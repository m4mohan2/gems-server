<?php 

namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class UserDevice extends Model
{ 
    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo('App\UserDetail', 'creator_id','user_id')->select('user_id','first_name','last_name','profile_pic','gender','address');
    }

}
