<?php 

namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class Gems extends Model
{ 
    public $timestamps = true;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'placeObj'
    ];

    public function categoryDetail()
    {
        //return $this->hasOne('App\GemsCategory', 'id', 'category_id');

        return $this->hasManyThrough('App\Category','App\GemsCategoryRelation', 'gems_id', 'id','id','category_id');

    }

    public function subcategoryDetail()
    {
        //return $this->hasOne('App\GemsCategory', 'id', 'category_id');

        return $this->hasManyThrough('App\Subcategory','App\GemsSubcategoryRelation', 'gems_id', 'id','id','subcategory_id');

    }

    public function createdBy()
    {
        return $this->hasOne('App\User', 'id', 'created_by')->select('id','email');
    }
    public function sender_friend_status()
    {
        return $this->belongsTo('App\UserFriend', 'created_by', 'sender');
    }

    public function receiver_friend_status()
    {
        return $this->belongsTo('App\UserFriend', 'created_by', 'receiver');
    }

    public function createdByUserDetail()
    {
        return $this->hasOne('App\UserDetail', 'user_id', 'created_by');
    }

    public function images(){
        return $this->hasMany('App\GemsImage')->select('gems_id','id','image','type','source');
    }

    public function total_like(){
        return $this->hasMany('App\GemsLikeunlike', 'gems_id','id')->where('action','1');
    }

    public function user_like_status(){
        return $this->hasMany('App\GemsLikeunlike', 'gems_id','id');
    }

    public function likeUnlike(){
        return $this->hasMany('App\GemsLikeunlike', 'gems_id','id');
    }

    public function providers(){
        return $this->hasMany('App\GemsProviderRelation', 'gems_id','id')->where('action','1')->select('action','gems_id','user_id');
    }

    public function total_view(){
        return $this->hasMany('App\GemsView', 'gems_id','id')->where('type',1);
    }
    // public function total_share(){
    //     return $this->hasMany('App\GemsView', 'gems_id','id')->where('type',2);
    // }

    public function total_share(){
        return $this->hasMany('App\ShareGemsGroup', 'gems_id','id');
    }

    public function total_comment(){
        return $this->hasMany('App\GemsComment', 'gems_id','id');
    }

    public function total_favorite(){
        return $this->hasMany('App\GemsFavorite', 'gems_id','id');
    }

    public function saved_lists(){
        return $this->hasMany('App\GemsSaveList', 'gems_id','id');
    }

    public function share_group(){
        return $this->hasMany('App\ShareGemsGroup', 'gems_id','id');
    }

    public function providerName()
    {
        return $this->hasOneThrough('App\UserDetail','App\GemsProviderRelation','gems_id','user_id','id','user_id')->select('gems_id','first_name', 'last_name')->where('action','=','1');
    }
}
