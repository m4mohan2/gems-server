<?php 

namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class GemsFavorite extends Model
{ 
    public $timestamps = true;

    public function gems()
    {
        return $this->hasOne('App\gems', 'id', 'gems_id');
    }

    public function gemsImages(){
        return $this->hasMany('App\GemsImage','gems_id','gems_id')->select('gems_id','id','image','type','source');
    }

}
