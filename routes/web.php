<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
$router->get('/', function () use ($router) {
    return $router->app->version();
});
// API route group
$router->group(['prefix' => 'api'], function () use ($router) {

    $router->post('register', 'AuthController@register');
    $router->post('registerProvider', 'AuthController@registerProvider');
    $router->get('verifyAccount/{token}', 'AuthController@verifyAccount');
    $router->post('login', 'AuthController@login');
    $router->post('socialMediaLogin', 'AuthController@socialMediaLogin');
    $router->post('logout', 'AuthController@logout');
    $router->post('refresh', 'AuthController@refresh');
    $router->post('assignProvider', 'AuthController@assignProvider');

    $router->post('updateUser', 'UserController@updateUser');
    $router->post('updateSelectedUser/{user_id}', 'UserController@updateSelectedUser');
    $router->post('updatePassword', 'UserController@updatePassword');
    $router->put('statusUpdateUser/{id}', 'UserController@statusUpdate');
    $router->put('influenceUpdateUser/{id}', 'UserController@influenceUpdate');
    $router->post('forgotPassword', 'AuthController@forgotPassword');
    $router->get('forgotPasswordVerify/{token}', 'AuthController@forgotPasswordVerify');
    $router->post('resetPassword', 'AuthController@resetPassword');
    $router->get('alluser', 'UserController@index');
    $router->get('userLists', 'UserController@userLists');
    $router->get('auditUserList', 'UserController@auditUserList');
    $router->post('profileDetails', 'UserController@profileDetails');
    $router->post('authActiveStatus', 'UserController@authActiveStatus');
    $router->post('userInfo', 'UserController@userInfo');

    $router->get('providers', 'UserController@providers');
    $router->get('providerInfo', 'UserController@providerInfo');
    $router->post('updateProvider/{id}', 'UserController@updateProvider');
    $router->put('statusUpdateProvider/{id}', 'UserController@statusUpdate');
    $router->delete('deleteUser', 'UserController@deleteUser');



    $router->post('billingWebhook', 'AuthController@billingWebhook');

    $router->post('sendFriendRequest', 'UserController@sendFriendRequest');
    $router->post('pendingFriendRequest', 'UserController@pendingFriendRequest');
    $router->post('sentFriendRequest', 'UserController@sentFriendRequest');
    $router->get('pendingFriendRequestPagination', 'UserController@pendingFriendRequestPagination');
    $router->get('sentFriendRequestPagination', 'UserController@sentFriendRequestPagination');
    $router->post('actionFriendRequest', 'UserController@actionFriendRequest');
    $router->post('cancelSentFriendRequest', 'UserController@cancelSentFriendRequest');
    $router->get('friendList', 'UserController@friendList');
    $router->post('removeFriend', 'UserController@removeFriend');
    

    $router->post('setPrivateInfo', 'UserController@setPrivateInfo');
    $router->post('userSearch', 'UserController@userSearch');
    $router->get('friendSearch', 'UserController@friendSearch');
    $router->get('friendCheck/{friend_id}', 'UserController@friendCheck');
    $router->get('getPrivateInfo/{user_id}', 'UserController@getPrivateInfo');

    $router->get('listCategory', 'GemsCategoryController@index');    
    $router->get('categoryList', 'GemsCategoryController@categoryList');
    $router->post('createCategory', 'GemsCategoryController@create');
    $router->put('editCategory/{id}', 'GemsCategoryController@update');
    $router->delete('deleteCategory/{id}', 'GemsCategoryController@delete');
    $router->put('statusUpdateCategory/{id}', 'GemsCategoryController@statusUpdate');

    // Gems Api
    $router->get('listGem', 'GemsController@index');
    $router->get('gemList', 'GemsController@gemList');
    $router->get('allGemList', 'GemsController@allGemList');
    $router->post('gemsListByDistance', 'GemsController@gemsListByDistance');
    $router->post('gemsSearch', 'GemsController@gemsSearch');
    $router->post('createGem', 'GemsController@create');
    $router->delete('deleteGem/{id}', 'GemsController@delete');
    $router->put('editGem/{id}', 'GemsController@update');
    $router->put('statusUpdateGem/{id}', 'GemsController@statusUpdate');
    $router->get('gemsDetails/{id}', 'GemsController@gemsDetails');
    $router->post('addGemComments', 'GemsController@addGemComments');
    $router->get('gemCommentList/{gems_id}', 'GemsController@gemCommentList');
    $router->post('gemsCommentImage', 'GemsController@gemsCommentImage');
    $router->get('gemsFeed', 'GemsController@gemsFeed');
    $router->post('assignGem', 'GemsController@assignGem'); 
    $router->get('assigngemList/{user_id}', 'GemsController@assigngemList');
       

    $router->get('gemsByPlaceId', 'GemsController@gemsByPlaceId');
    $router->post('searchPlaces', 'GemsController@searchPlaces');
    $router->post('placeDetails', 'GemsController@placeDetails');

    $router->post('shareGemsToUser', 'GemsController@shareGemsToUser');
    $router->post('shareGemsInGroup', 'GemsController@shareGemsInGroup');
    $router->get('shareGemsList/{user_id}', 'GemsController@shareGemsList');
    $router->get('shareGemsGroupList/{user_id}', 'GemsController@shareGemsGroupList'); 
    $router->get('shareGemByUserList/{user_id}', 'GemsController@shareGemByUserList');    
    $router->post('gemsLikeUnlike', 'GemsController@gemsLikeUnlike');
    $router->post('gemsFavorite', 'GemsController@gemsFavorite');
    $router->get('likeUserList/{gems_id}', 'GemsController@likeUserList');
    $router->get('likeGemByUserList/{user_id}', 'GemsController@likeGemByUserList');
    $router->get('createdGemByUserList/{user_id}', 'GemsController@createdGemByUserList');
    $router->get('favouriteGemByUserList/{user_id}', 'GemsController@favouriteGemByUserList');
    $router->get('gemLikeStatus/{user_id}/{gem_id}', 'GemsController@gemLikeStatus');
    $router->post('gemsView', 'GemsController@gemsView');

    $router->post('addNotificationSettings', 'GemsController@addNotificationSettings');
    $router->delete('removeNotificationSettings/{id}', 'GemsController@removeNotificationSettings');

    // Save Gem List
    $router->post('createGemLabel', 'GemsController@createGemLabel');
    $router->put('updateGemLabel/{id}', 'GemsController@updateGemLabel');
    $router->get('gemLabelList', 'GemsController@gemLabelList');    
    $router->delete('deleteGemLabel/{label_id}', 'GemsController@deleteGemLabel');
    $router->post('addGemsSaveList', 'GemsController@addGemsSaveList');
    $router->get('getGemsSaveList/{label_id}', 'GemsController@getGemsSaveList');
    $router->delete('deleteGemsSaveList/{id}', 'GemsController@deleteGemsSaveList');
    
    $router->get('gemsImageList/{gems_id}', 'GemsController@gemsImageList');
    $router->post('gemsImage', 'GemsController@gemsImage');
    $router->delete('deleteGemsImage/{id}', 'GemsController@deleteGemsImage');

    // Group Api
    $router->get('groupList', 'GroupController@groupList');
    $router->get('getGemsByGroupList/{group_id}', 'GroupController@getGemsByGroupList');
    $router->post('createGroup', 'GroupController@createGroup');
    $router->post('updateGroupMember', 'GroupController@updateGroupMember');
    $router->post('joinGroup', 'GroupController@joinGroup');
    $router->post('removeFromGroup', 'GroupController@removeFromGroup');
    $router->post('leftGroup', 'GroupController@leftGroup');
    $router->post('memberList', 'GroupController@memberList');
    $router->post('makeGroupAdmin', 'GroupController@makeGroupAdmin');
    $router->delete('deleteGroup', 'GroupController@deleteGroup');

    // Coupon Api
    $router->get('couponList', 'CouponController@couponList');
    $router->get('userCoupons/{user_id}', 'CouponController@userCoupons');
    $router->get('userActiveCoupons/{user_id}', 'CouponController@userActiveCoupons');
    $router->get('couponDetails/{code}', 'CouponController@couponDetails');
    $router->post('generateCoupon', 'CouponController@generateCoupon');
    $router->delete('deleteCoupon/{id}', 'CouponController@deleteCoupon');
    $router->put('statusChange/{id}', 'CouponController@statusChange');
    $router->post('assignCoupon', 'CouponController@assignCoupon');
    $router->put('updateCoupon/{id}', 'CouponController@updateCoupon');
    $router->post('redeemCoupon', 'CouponController@redeemCoupon');

    // CMS Api
    $router->get('cmsList', 'CmsController@cmsList');
    $router->post('addCms', 'CmsController@add');
    $router->put('updateCms/{id}', 'CmsController@update');
    $router->put('statusUpdateCms/{id}', 'CmsController@statusUpdate');
    $router->get('cmsDetails/{id}', 'CmsController@cmsDetails');

    // News Api
    $router->get('newsList', 'NewsController@newsList');
    $router->get('newsListAdmin', 'NewsController@newsListAdmin');
    $router->post('addNews', 'NewsController@add');
    $router->post('updateNews/', 'NewsController@update');
    $router->put('statusUpdateNews/{id}', 'NewsController@statusUpdate');
    $router->get('newsDetails/{id}', 'NewsController@newsDetails');
    $router->put('newsStatusUpdate/{id}', 'NewsController@statusUpdate');
    $router->delete('deleteNews/{id}', 'NewsController@delete');

    //Testimonial Api
    $router->get('testimonialList', 'TestimonialController@testimonialList');
    $router->get('testimonialListAdmin', 'TestimonialController@testimonialListAdmin');
    $router->post('addTestimonial', 'TestimonialController@add');
    $router->post('updateTestimonial/', 'TestimonialController@update');
    $router->put('statusUpdateTestimonial/{id}', 'TestimonialController@statusUpdate');
    $router->get('testimonialDetails/{id}', 'TestimonialController@testimonialDetails');
    $router->put('testimonialStatusUpdate/{id}', 'TestimonialController@statusUpdate');
    $router->delete('deleteTestimonial/{id}', 'TestimonialController@delete');

    // FAQ Api
    $router->get('faqList', 'FaqController@faqList');
    $router->get('faqCategoryList', 'FaqController@faqCategoryList');    
    $router->get('faqListAdmin', 'FaqController@faqListAdmin');
    $router->post('addFaq', 'FaqController@add');
    $router->put('updateFaq/{id}', 'FaqController@update');
    $router->put('statusUpdateFaq/{id}', 'FaqController@statusUpdate');
    $router->get('faqDetails/{id}', 'FaqController@faqDetails');
    $router->put('faqStatusUpdate/{id}', 'FaqController@statusUpdate');
    $router->delete('deleteFaq/{id}', 'FaqController@delete');

    $router->get('homepageList', 'HomepageController@homepageList');
    $router->get('homepageListAdmin', 'HomepageController@homepageListAdmin');
    $router->post('addHomepage', 'HomepageController@add');
    $router->post('updateHomepage', 'HomepageController@update');
    $router->put('statusUpdateHomepage/{id}', 'HomepageController@statusUpdate');
    $router->get('homepageDetails/{id}', 'HomepageController@homepageDetails');
    $router->put('homepageStatusUpdate/{id}', 'HomepageController@statusUpdate');
    $router->delete('deleteHomepage/{id}', 'HomepageController@delete');

    $router->get('sitesettingList', 'SitesettingController@sitesettingList');
    $router->post('addSitesetting', 'SitesettingController@add');
    $router->put('updateSitesetting/{id}', 'SitesettingController@update');

    $router->get('listService', 'ServicesController@index');
    $router->post('createService', 'ServicesController@create');
    $router->delete('deleteService/{id}', 'ServicesController@delete');
    $router->put('editService/{id}', 'ServicesController@update');
    $router->put('statusUpdateService/{id}', 'ServicesController@statusUpdate');

    $router->get('listPlan', 'PlanController@index');
    $router->post('createPlan', 'PlanController@create');
    $router->delete('deletePlan/{id}', 'PlanController@delete');
    $router->put('editPlan/{id}', 'PlanController@update');
    $router->put('statusUpdatePlan/{id}', 'PlanController@statusUpdate');

    // Category Api  
    $router->get('importSubcat', 'CategoryController@importSubcat');
    $router->get('categoryListAdmin', 'CategoryController@categoryListAdmin');
    $router->post('addCategory', 'CategoryController@addCategory');
    $router->put('updateCategory/{id}', 'CategoryController@updateCategory');
    $router->put('statusUpdateCategory/{id}', 'CategoryController@statusUpdate');
    $router->delete('deleteCategory/{id}', 'CategoryController@delete');
    $router->get('categoryList', 'CategoryController@categoryList');
    $router->get('subcategoryList', 'CategoryController@subcategoryList');
    $router->get('subcategoryAppList', 'CategoryController@subcategoryAppList');
    $router->get('subcategoryListAdmin', 'CategoryController@subcategoryListAdmin');
    $router->post('addSubcategory', 'CategoryController@addSubcategory');
    $router->put('updateSubcategory/{id}', 'CategoryController@updateSubcategory');
    $router->put('statusUpdateSubcategory/{id}', 'CategoryController@statusSubcategoryUpdate');
    $router->delete('deleteSubcategory/{id}', 'CategoryController@deleteSubcategory');

    //Frontend Api
    $router->get('gemsSearchFront', 'FrontendController@gemsSearch');
    $router->get('gemsAnalysis/{id}', 'FrontendController@gemsAnalysis');
    $router->get('gemsAnalysisByDate/{id}', 'FrontendController@gemsAnalysisByDate');
    $router->get('gemsAnalysisByLike/{id}', 'FrontendController@gemsAnalysisByLike');
    $router->get('gemsAnalysisByView/{id}', 'FrontendController@gemsAnalysisByView');
    $router->get('gemsAnalysisByComment/{id}', 'FrontendController@gemsAnalysisByComment');
    $router->get('gemsAnalysisByShare/{id}', 'FrontendController@gemsAnalysisByShare');
    $router->get('gemsAnalysisBySave/{id}', 'FrontendController@gemsAnalysisBySave');
    $router->get('getCmsDetails/{slug}', 'FrontendController@getCmsDetails');
    $router->get('getCmsList', 'FrontendController@getCmsList');
    $router->get('checkCmsSlug', 'FrontendController@checkCmsSlug');
    $router->get('news/{slug}', 'FrontendController@newsDetails');
    $router->get('trendingGems', 'FrontendController@trendingGems');

    //Dashboard
    $router->get('engagementSummery/{id}', 'DashboardController@engagementSummery');
    $router->get('popularGems', 'DashboardController@popularGems');
    $router->get('audienceCountryWise/{id}', 'DashboardController@audienceCountryWise');
    $router->get('engagementGenderWise/{id}', 'DashboardController@engagementGenderWise');
    $router->get('activeUser', 'DashboardController@activeUser');
    $router->get('totalRecords', 'DashboardController@totalRecords');
    $router->get('adminAudienceCountryWise', 'DashboardController@adminAudienceCountryWise');
    $router->get('adminEngagementGenderWise', 'DashboardController@adminEngagementGenderWise');

    //provider
    $router->get('providerGemList/{user_id}', 'FrontendController@providerGemList');
    $router->get('claimList', 'UserController@claimList');
    $router->get('changeClaimStatus/{id}', 'UserController@changeClaimStatus');

    $router->get('subscriptionList', 'SubscriptionController@index');
    $router->get('creditCardLists', 'SubscriptionController@creditCardLists');
    $router->post('createCard', 'SubscriptionController@createCard');
    $router->post('updateCard', 'SubscriptionController@updateCard');
    $router->delete('deleteCard/{card_id}', 'SubscriptionController@deleteCard');
    $router->post('invoice', 'SubscriptionController@invoice');

    //role api
    $router->get('/getRole', 'RoleController@index');
    $router->post('/addRole', 'RoleController@create');
    $router->put('/editRolePermission/{id}', 'RoleController@update');
    $router->get('/role/{id}', 'RoleController@show');
    $router->delete('/deleteRolePermission/{id}', 'RoleController@destroy');
    
    //country api 
    $router->get('/countries', 'CountryController@index');

    //state api 
    $router->post('/getStates', 'StateController@getStates');

    // city api
    $router->post('/getCities', 'CityController@getCities');

    $router->get('getLogList', 'AuthController@getLogList');
    $router->get('friendsActivity/{user_id}', 'UserController@friendsActivity');    

    //$router->get('sendRegisterEmail', 'AuthController@sendRegisterEmail');
    //$router->get('verify/{id}/{token}', 'AuthController@get_active_account');
    //$router->post('forgotpassword', 'AuthController@forgotPassword');
    //$router->get('forgotpasswordverify/{id}/{token}', 'AuthController@forgotPasswordVerify');
});
