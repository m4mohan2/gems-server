<div style="float:left; width:100%; background:#EEEEEE;padding:30px 0;">
     <div style="max-width:100%; width:600px; padding:0px; margin:0 auto;">
        <div style="width:100%; background:#1F2C47; padding:20px 20px; float:left;"><img src="{{ $logo }}" width="80" height="80" style="border-radius:50%">
        </div>
        <div style="font-size:15px;float:left; width:100%; background:#fff; font-family:Tahoma, Geneva, sans-serif; text-align:left; color:#1F2C47; padding:20px;">
            <p><strong>Hi {{ $name }},</strong></p>
            <p>Please reset your account password by clicking <a href="{{ $link }}" target="_blank">here</a><p>

            <p>Thanks and regards,<br />
          	 Admin</p>
        </div>
        <div style="width:100%; background:#1F2C47; padding:30px 20px; float:left; text-align:center;color:#989898">
        <p>© Copyright 2020-21 GEMS - All Rights Reserved</p>
        </div>
     </div>
</div>